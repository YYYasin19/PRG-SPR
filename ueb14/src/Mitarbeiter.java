import java.util.ArrayList;
import java.util.List;

import static Tools.CoreTools.check;
import static Tools.MessageTools.getMsg;

/**
 * An implementation of Mitarbeiter
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-Apr-11
 */
public class Mitarbeiter extends Person {

/* ---------------------------------------- Main ---------------------------------------------------------------- */



/* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private String email;

    private List<Reservierung> reservierungList;

/* ---------------------------------------- Constants ----------------------------------------------------------- */



/* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * constructor for Mitarbeiter (user constructor of PersonQueue.Person)
     * @param vorname vorname of mitarbeiter
     * @param nachname nachname of mitarbeiter
     * @param email e-mail adress of mitarbeiter
     */
    protected Mitarbeiter(String vorname, String nachname, String email) {
        super(vorname, nachname);
        this.setEmail(email);

        this.reservierungList = new ArrayList<>();
    }

/* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * reserves a room for this Mitarbeiter
     * @param raum Raum to be reserved
     * @param beginn start date
     * @param ende end date
     * @param bemerkung information about reservation
     */
    protected void reserviere(Raum raum, Uhrzeit beginn, Uhrzeit ende, String bemerkung){
        reserviere(new Reservierung(this, raum, bemerkung, beginn, ende));
    }

    /**
     * adds a reservation to this Mitarbeiter
     * @param r Reservation
     */
    protected void reserviere(Reservierung r){
        r.getRaum().addReservierung(r); //important for connection
        this.reservierungList.add(r);
    }


    @Override
    public String toString(){
        return String.format("%s %s (%s)",super.getVorname(),super.getNachname(),this.email);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Mitarbeiter that = (Mitarbeiter) o;

        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        return reservierungList != null ? reservierungList.equals(that.reservierungList) : that.reservierungList == null;
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * sets the email adress for a Mitarbeiter object
     * @param email
     */
    private void setEmail(String email) {

        if(email == null) {
            throw new IllegalArgumentException(getMsg("EMPTY_EMAIL"));
        }
        this.email = email;
    }

    /**
     * returns the Reservierung @index
     * @param index
     * @return Reservierung
     */
    protected Reservierung getReservierung(int index){
        return reservierungList.get(index);
    }
}
