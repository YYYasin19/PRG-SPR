package Tools;

/**
 * An implementation of CoreTools
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-10
 */
public class CoreTools {

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */




    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * checks a given condition and throws a RuntimeException if the conditions equals false.
     * @param condition the condition which has to equal true
     * @param message the exception message that will be given
     *
     */
    public static void check(boolean condition, String message) throws RuntimeException {

        if (!condition){
            throw new RuntimeException(message);
        }

    }

    public static void check(boolean condition, Exception t) throws Exception{

        if (!condition){
            throw t;
        }

    }



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
