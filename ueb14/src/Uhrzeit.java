import static Tools.CoreTools.*;
import static Tools.MessageTools.getMsg;

/**
 * An implementation of Uhrzeit
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-10
 */
public class Uhrzeit {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private int hours;
    private int minutes;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */
    /**
     * Default constructor for an "Uhrzeit"-object
     * @param hours hours
     * @param mins minutes
     */
    protected Uhrzeit(int hours, int mins){
        this.setHours(hours);
        this.setMinutes(mins);
    }


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * @return a string version of the current time in 24-hour format
     */
    @Override
    public String toString() {
        return String.format("%d:%d Uhr",hours,minutes);
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * sets the hours and checks if greater than 0 and smaller than 24
     * @param hours hours
     */
    private void setHours(int hours) {
        check(hours <= 24,getMsg("MAX_HOURS_ERROR") + " | Ihre Eingabe: " + hours);
        this.hours = hours;
    }

    /**
     * sets the minutes and checks if greater 0 and smaller than 60
     * @param minutes minutes
     */
    private void setMinutes(int minutes) {
        check((minutes >= 0) && (minutes <= 59),getMsg("MAX_MINUTES_ERROR"));
        this.minutes = minutes;
    }

}
