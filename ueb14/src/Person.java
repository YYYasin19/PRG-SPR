import static Tools.MessageTools.getMsg;

/**
 * An implementation of PersonQueue.Person
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-Apr-10
 */
public class Person {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    private String vorname;
    private String nachname;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    protected Person(String vorname, String nachname){
        if(vorname.trim().length() == 0){

            throw new IllegalArgumentException(getMsg("NAME_EMPTY"));

        }else if (nachname.trim().length() == 0){
            throw new IllegalArgumentException(getMsg("NAME_EMPTY"));

        }

        this.vorname = vorname;
        this.nachname = nachname;
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    @Override
    public String toString(){
        return this.vorname + " " + this.nachname;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }
}

