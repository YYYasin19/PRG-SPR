import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of Raum
 * in Prog2_Codebase
 *
 * @author Yasin Tatar
 * @version 1.0
 * @since 2018-Apr-11
 */
public class Raum {

/* ---------------------------------------- Main ---------------------------------------------------------------- */



/* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private int geb;
    private int etage;
    private int raum;

    private List<Reservierung> reservierungen;


/* ---------------------------------------- Constants ----------------------------------------------------------- */



/* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * constructor for Raum-object
     * @param geb building
     * @param etage stock/floor
     * @param raum room
     */
    protected Raum(int geb, int etage, int raum){
        this.setGeb(geb);
        this.setEtage(etage);
        this.setRaum(raum);

        this.reservierungen = new ArrayList<>();
    }

/* ---------------------------------------- Methods ------------------------------------------------------------- */


    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder(String.format("Raum %d-%d.%d \n",this.geb,this.etage,this.raum));

        for (Reservierung aReservierungen : reservierungen) {
            sb.append(aReservierungen.toString()).append("\n");
        }

        return sb.toString();
    }

    /**
     * adds the Reservierung for this raum
     * @param reservierung to be added
     */
    public void addReservierung(Reservierung reservierung) {
        if (reservierung != null){
            reservierungen.add(reservierung);
        } else {
            throw new IllegalArgumentException(getMsg("RESERVIERUNG_NULL"));
        }
    }

/* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    private void setGeb(int geb) {
        this.geb = geb;
    }

    private void setEtage(int etage) {
        this.etage = etage;
    }

    private void setRaum(int raum) {
        this.raum = raum;
    }

    public List<Reservierung> getReservierungen() {
        return reservierungen;
    }

    /**
     * returns a specific reservation
     * @param index index
     * @return the reservation@index
     */
    public Reservierung getReservierung(int index){

        if (index < reservierungen.size() && index >= 0) {
            return reservierungen.get(index);
        } else {
            throw new IndexOutOfBoundsException(String.format("There are only %d reservations.",reservierungen.size()));
        }
    }
}
