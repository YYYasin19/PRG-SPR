import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of Reservierung
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-Apr-11
 */
public class Reservierung {

/* ---------------------------------------- Main ---------------------------------------------------------------- */



/* ---------------------------------------- Attributes ---------------------------------------------------------- */

    private Uhrzeit beginn;
    private Uhrzeit ende;
    private String bemerkung;

    private Mitarbeiter mitarbeiter;
    private Raum raum;

/* ---------------------------------------- Constants ----------------------------------------------------------- */



/* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Constructor for "Reservierung"-object
     * @param beginn start date
     * @param ende end date
     */
    protected Reservierung(Mitarbeiter m, Raum raum, String bemerkung, Uhrzeit beginn, Uhrzeit ende){
        this.setMitarbeiter(m);
        this.setRaum(raum);
        this.setBemerkung(bemerkung);
        this.setBeginn(beginn);
        this.setEnde(ende);
    }

/* ---------------------------------------- Methods ------------------------------------------------------------- */

    @Override
    public String toString(){
        return String.format("gebucht von: %s - von %s bis %s - für: %s",
                        this.mitarbeiter.toString(),this.beginn.toString(),this.ende.toString(),this.bemerkung);
    }

/* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * set the start date of "Reservierung"
     * @param beginn start date
     */
    private void setBeginn(Uhrzeit beginn) {
        if(beginn == null){
            throw new IllegalArgumentException(getMsg("TIME_EMPTY"));
        }
        this.beginn = beginn;
    }

    /**
     * set the end date of "Reservierung"
     * @param ende end date
     */
    private void setEnde(Uhrzeit ende) {
        if(ende == null) {
            throw new IllegalArgumentException(getMsg("TIME_EMPTY"));
        }
        this.ende = ende;
    }

    /**
     * set the info for "Reservierung"
     * @param bemerkung info
     */
    void setBemerkung(String bemerkung){

        if(bemerkung.trim().length() == 0){
            throw new IllegalArgumentException(getMsg("BEMERKUNG_EMPTY"));
        }

        this.bemerkung = bemerkung;
    }


    /**
     * set the mitarbeiter for "Reservierung"
     * @param mitarbeiter mitarbeiter
     */
    public void setMitarbeiter(Mitarbeiter mitarbeiter){
        if(mitarbeiter == null){
            throw new IllegalArgumentException(getMsg("MITARBEITER_EMPTY"));
        }

        this.mitarbeiter = mitarbeiter;

    }

    /**
     * sets the Raum of the reservation
     * @param raum raum for the reservation (cannot be null)
     */
    public void setRaum(Raum raum){
        if(raum == null){
            throw new IllegalArgumentException(getMsg("RAUM_EMPTY"));
        }

        this.raum = raum;
    }

    public Uhrzeit getBeginn() {
        return beginn;
    }

    public Uhrzeit getEnde() {
        return ende;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public Raum getRaum() {
        return raum;
    }
}
