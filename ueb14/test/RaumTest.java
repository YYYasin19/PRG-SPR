import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class RaumTest {

    private Raum raum;
    private Mitarbeiter m;
    private Reservierung r;

    @Before
    public void setUp() {
        raum = new Raum(18,0,1);
        m = new Mitarbeiter("Markus","Lanz","markus@lanz.de");
        r = new Reservierung(m, raum,"Test",
                new Uhrzeit(12,30), new Uhrzeit(13,30));
    }



    @Test
    public void addReservierung() {

        raum.addReservierung(r);
        assertEquals(r,raum.getReservierung(0));
    }

    @Test
    public void getReservierungen() {
        raum.addReservierung(r);
        Object o = raum.getReservierungen();
        assertNotNull(o);
    }

    @Test
    public void getReservierung() {

        /*
        Idea: We create sample reservations with name from 0 to 99
        We think of a random number between 0 and 100 and run getReservierung(randomNumber)
        Now, if getReservierung() is working right, the returned reservation should contain the randomNumber in its name.
         */

        LinkedList<Reservierung> reservations = createReservations(100);
        int rand;

        //add reservations to room:
        for (Reservierung r : reservations){
            raum.addReservierung(r);
        }

        //Check if the index at the reservation is in its name:
        for (int i = 0; i < 100; i++){
            rand = ThreadLocalRandom.current().nextInt(100);
            Reservierung test = raum.getReservierung(rand);

            if (!(test.getBemerkung().contains(String.valueOf(rand)))) {
                fail();
            }
        }

    }

    /**
     * creates reservations and adds them to this room. the names of the reservations contain
     * incrementing numbers from 0 to 100
     * @return
     */
    private LinkedList<Reservierung> createReservations(int numberOfReservations) {

        LinkedList<Reservierung> reservations = new LinkedList<>();

        String name = "Test-Reservierung";
        int index = 0;
        Uhrzeit start = new Uhrzeit(12,00);
        Uhrzeit end = new Uhrzeit(14,00);

        for (int i = 0; i < numberOfReservations; i++){
            Reservierung r = new Reservierung(m, raum, name + index, start, end);
            index++;
            reservations.add(r);
        }


        return reservations;

    }

    @After
    public void tearDown() {
    }
}