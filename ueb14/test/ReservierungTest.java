
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReservierungTest {

    private Reservierung reservierung;
    private Mitarbeiter mitarbeiter;
    private Raum raum;

    @Before
    public void setUp() throws Exception {
        mitarbeiter = new Mitarbeiter("Test","Test","test@htwsaar.de");
        raum = new Raum(18,0,1);
        reservierung = new Reservierung(
                mitarbeiter,raum,"Test-Bemerkung",
                new Uhrzeit(13,00),
                new Uhrzeit(14,00));
    }

    @Test
    public void setBemerkung() {
        reservierung.setBemerkung("Test-Bemerkung");
        assertEquals("Test-Bemerkung",reservierung.getBemerkung());
    }

    @Test (expected = Exception.class)
    public void setBemerkung2(){
        reservierung.setBemerkung("");
    }

    @Test
    public void setMitarbeiter() {
        Mitarbeiter test = new Mitarbeiter("Test","Test","test@test.de");
        reservierung.setMitarbeiter(test);
        assertEquals(test,reservierung.getMitarbeiter());
    }

    @Test
    public void setRaum() {
        Raum r = new Raum(18,0,1);
        reservierung.setRaum(r);
        assertEquals(r,reservierung.getRaum());
    }
}