

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MitarbeiterTest {

    Mitarbeiter mitarbeiter;

    @Before
    public void setUp() throws Exception {
        mitarbeiter = new Mitarbeiter("Test","Test","test@htwsaar.de");
    }

    @Test
    public void reserviere() {
        Reservierung res = new Reservierung(
                mitarbeiter,
                new Raum(18,0,1),
                "Test-Reservierung",
                new Uhrzeit(12,00),
                new Uhrzeit(14,00));
        mitarbeiter.reserviere(res);


        assertEquals(res, mitarbeiter.getReservierung(0));

    }

    @After
    public void tearDown() throws Exception {
    }


}