# Wirtschaftsinformatik  
  
## Einführung  
  
### Beruflicher Ausblick  
  
### Definition Wirtschaftsinformatik  
  
### Zentrale Ansätze  
  
### Informationssysteme  
  
Ein System,  
– das für die Zwecke eines Teils eines  bestimmten Unternehmens geschaffen bzw. in diesem Betrieb eingesetzt wird.   
  
Ein Informationssystem  
– enthält die dafür notwendige  Anwendungssoftware und Daten  
– und ist in die Organisations-, Personal- und  Technikstrukturen des Unternehmens eingebettet.  
  
* Herausforderungen  
    * Lohnt sich eine Investition?  
    * Was ist für einen effizienten Einsatz erforderlich?  
    * Globalisierung  
          
        Wie können Firmen die Unternehmens- und Systemanforderungen einer globalen Wirtschaft bestimmen?  
  
    * Wie implementieren wir eine zukunftssichere IT-Infrastruktur?  
    * Integration von Systemen  
    * Verantwortung und Kontrolle über Einsatz  
* Motivation  
      
    Informationssysteme unterstützen Unternehmen  
  
    * weit entfernte Standorte zu erreichen  
    * neue Produkte und Dienstleistungen anzubieten  
    * Geschäftsprozesse neu zu organisieren  
* Einsatz von Informationssysteme  
    * Wachsende Bedeutung  
          
        Ansteigende Wechselseitige Abhängigkeit zwischen Unternehmensführung und IT-Infrastruktur (und damit Informationssystemen)  
  
* Strategische Geschäftsziele  
      
    * Neue Produkte  
    * Wettbewerbsvorteile  
    * Kunden- und Lieferantennähe  
    * optimierte Entscheidungsfindung  
    * Unternehmensfortbestand  
* Strategische und Organisatorische Aspekte  
    * Unternehmensorganisation  
        * Flexibilität durch Kommunikationstechnik  
              
            – Massenfertigung kundenindividueller Produkte   
            – Informationsverteilung  
            – Kundenanalyse  
            – Informationsbeschaffung  
  
    * Unternehmensstrategie  
        * Gängige Strategien  
            * Kostenführerschaft  
                * Unterstützung d. Informationssysteme  
                      
                    Informationssysteme  
                    – vereinfachen die Standardisierung von   
                    Dienstleistungen  
                    – vereinfachen Verwaltungsarbeiten  
                    – können Kosten der Produktion senken, z.B.   
                    mit einer automatisierten Produktionsplanung und -steuerung  
  
            * Differenzierung  
                  
                Abhebung des eigenen Produktes gegenüber der Konkurrenz durch besondere Eigenschaften oder Markenverbundenheit etc.  
  
                * Unterstützung d. Informationssyteme  
                      
                    Einsatz von Informationssystemen   
                    – Schaffung einzigartiger neuer Produkte   
                    – Qualität, Service, Differenzierung   
                    – können Konkurrenz daran hindern, Produkte   
                    nachzuahmen  
                    – sollen durch einen Zusatznutzen Kunden   
                    dafür gewinnen, einen Preisaufschlag zu bezahlen   
                      
                    -> Keine Konkurrenz auf Kostenebene mehr  
  
            * Umfang des Wettbewerbs ändern  
                  
                Bspw. Ausweitung auf neue Märkte  
  
            * Fokussierte Differenzierung  
                  
                Neue Marktnischen schaffen, indem spezielle Zielgruppen identifiziert werden   
                – Kleiner Zielmarkt  
                – Für den das Produkt einen besonderen Wert liefert – Spezialisierung  
  
        * Customer-Relationship  
              
            * Feine Abstimmung von Daten auf Kundenwünsche  
            * Informationen als Ressource  
            * Effektivere Marketingstrategien  
        * Wettbewerbskräftemodell nach Porter  
              
            **Branchenstrukturanalyse**  
  
            * Brancheninterner Wettbewerb  
                  
                * Intensität des Wettbewerbs  
                * Umgang der Wettbewerber miteinander  
            * Neue Anbieter  
                  
                * Bedrohung durch Anbieter  
                * Markteintrittsbarriere  
            * Ersatzprodukte/ - dienstleistungen  
                  
                Kunden erhalten von Wettbewerbern, was sie wollen  
  
            * Verhandlungsstärke der Lieferanten  
                  
                Einflussmöglichkeiten der Lieferanten auf den Preis, den sie erhalten  
  
            * Verhandlungsstärke der Abnehmer  
                  
                Einflussmöglichkeiten der Kunden auf den Preis den sie zahlen  
  
### Anwendungssysteme  
  
Ein System,  
– das alle Programme beinhaltet, die für ein   
bestimmtes betriebliches Aufgabengebiet   
entwickelt und eingesetzt werden,  
– inklusive der Technik (IT-Infrastruktur), auf der   
das Anwendungssystem läuft,  
– und der Daten, die vom Anwendungssystem   
genutzt werden  
  
* Klassifikation  
    * Strategische Ebene  
          
        Executive Support System (ESS)  
          
        -> Kombination von Daten und erweiterte Datenanalyse  
  
    * Management Ebene  
          
        * Management Information System (MIS)  
        * Decision Support System (DSS)  
          
        = dienen durch die Bereitstellung von Berichten der Planung, Kontrolle und Entscheidungsfindung  
  
    * Operative Ebene  
          
        Transaction Processing System  
          
        Anwendungssysteme -> täglichen Geschäftsbetrieb  
  
        * CRM | Marketingsysteme  
        * SCM | Beschaffung/Produktion  
        * FI | Finanzbuchhaltung  
        * HR | Personalwesen  
        * Sonstige  
  
### Zusammenhang Anwendungs-/Informationssysteme  
  
Informationssysteme nutzen mehrere Anwendungssysteme und machen diese für Organisationen nutzbar  
  
### Wandel von Unternehmen  
  
•    WenigerHierarchie,flachere Organisationsstrukturen   
  
    •    Dezentralisierung   
  
    •    GrößereFlexibilität   
  
    •    Standortunabhängigkeit   
  
    •    Geringe Transaktions- und Koordinationskosten   
  
    •    ÜbertragungvonVerantwortunganAusführende   
  
    •    UnternehmensübergreifendeKooperationund Teamarbeit  
  
* Trend zum vernetzten Unternehmen  
    * Definition  
          
        Unternehmen in denen alle wesentlichen Geschäftsprozesse (also auch die Außenkommunikation) über Informationstechnik ablaufen  
  
    * Entstehung eines vernetzten Unternehmens  
* E-Business  
      
    E-Business:  
    –  Die Anwendung von Internet und digitalen  Techniken   
    –  zur Ausführung sämtlicher Geschäftsprozesse  eines Unternehmens.   
    –  Umfasst sowohl E-Commerce als auch  Prozesse zur internen Verwaltung des Unternehmens und zur Koordination mit Lieferanten und anderen Geschäftspartnern.  
  
    * E-Commerce  
          
        E-Commerce:  
        Der elektronische Kauf und Verkauf von   
        Waren und Dienstleistungen  
  
* E-Government  
* Carr-Debatte  
  
## Geschäftsprozesse  
  
### Prozess  
  
= inhaltlich abgeschlossene, zeitliche Abfolge von Funktionen mit Input und Output  
  
* Geschäftsprozesse  
      
    = Prozess mit Endwert für den Kunden  
  
    * Organisationsprozesse  
    * Führungsprozesse  
    * Unterstützungsprozesse  
* Prozessorganisation  
    * Ziele  
          
        * Kundenorientierung  
        * Prozesseffizienz  
        * Mitarbeitermotivation  
    * Maßnahmen  
          
        * Minimierung der Aufbauorganisatorischen Schnittstellen  
        * Minimierung der Anzahl der an einem Prozess Beteiligten Personen  
        * Eindeutige Verantwortlichkeiten  
  
### Prozessmodellierung  
  
* Möglichkeiten  
    * Textuelle Form  
    * Tabellarische Form  
    * Grafische Darstellung  
        * Ohne besondere Notation  
        * BPMN / EPK  
            * Verschiedene etablierte Tools  
                  
                * MS Visio  
                * ARIS Toolset  
                * Bonapart  
                * Nebenwirkungen von Tools  
                      
                    * Zu viele Funktionen   
                    –  Insbesondere beim Einsatz zu dokumentarischen Zwecken  genügt oft ein einfacher Funktionsumfang   
                    –  Unnötig hoher Einarbeitungsaufwand   
                      
                    * Starke „Eigendynamik“  
                    – Funktionen werden eingesetzt, weil das Tool sie bietet, nicht  weil es der Prozess erfordert   
                      
                    * Genaue Abbildung wird vorgetäuscht  
                    – Ein Modell ist immer nur eine Vereinfachung der Realität   
                      
                    * Technisierung des Projektes  
                    – Andere Faktoren, wie Kommunikation, die Mitarbeiter oder  strategische Aspekte werden vernachlässig  
  
            * Graph  
                * Event  
                      
                    Kreis  
                    z.Bsp. „Invoice emitted“  
  
                * Task  
                      
                    Rechtecke  
                    z.Bsp. „Approve Order“  
  
                * Flow  
                      
                    Pfeil  
  
                * Gateway  
                      
                    Diamant  
  
                    * AND  
                          
                        Leerer Diamant  
  
                    * XOR  
                          
                        Diamant mit X drin  
  
            * Text  
                  
                < 6 Wörter  
  
## Datenorganisation und Datenmanagement  
  
### Business Intelligence & Analytics  
  
* Big Data  
    * Datenvielfalt  
          
        * Fremddaten (Web etc.)  
        * Firmendaten  
        * strukturierte Daten  
        * Kommunikation zwischen Maschinen  
    * Datenmenge  
    * Geschwindigkeit  
          
        * Echtzeit  
        * Millisekunden  
    * Analytics  
          
        * Erkennen von Zusammenhängen  
        * Data Models | Cluster  
* Business Intelligence  
      
    Entscheidungsunterstützung z.Bsp. durch Reports und Daten aus dem DataWarehouse  
  
* Data Warehouse  
      
    = zentrale Datenbank die verschiedene Informationsquellen zusammen führt  
  
* Data Mart  
  
### Data Mining  
  
– Assoziationen   
– Sequenzen  
– Klassifizierung   
– Clustering   
– Prognose/Forecasting  
  
* Text Mining  
      
    Über 80 % der nützlichen Informationen einer Organisation liegt in unstrukturierten Daten vor, z.B. in Form von:  – Textdateien  
    – E-Mails  
    – Callcenter-Transkripte – Umfrageantworten  
    – Rechtsfälle  
    – Patentbeschreibungen – Serviceberichte  
  
* Web Mining  
      
    = Aufdecken und Analyse nützlicher Daten aus dem Web  
    z.Bsp. Web Crawler etc.  
  
## IT - Infrastruktur  
  
* Technologische Ressourcen, die von allen Mitgliedern eines Unternehmens genutzt werden können, bilden die IT-Infrastruktur  – Hardware  
– Software  
– Daten/Speichertechnik  
– Kommunikationstechnik (inkl. Netzwerke)   
  
* Grundlage für spezifische Informationssysteme   
* Plattform für Dienste, z.B. Beratung, Ausbildung, Schulung  
  
### Wichtige Entwicklungen  
  
* Moore’sches Gesetz  
* Digitale Massenspeicher  
      
    Exponentieller Leistungszuwachs bei fallenden Kosten  
  
* Gesetz von Metcalfe  
      
    Nutzen eines Netzwerkes steigt schneller als Anzahl seiner Mitglieder  
  
* Sinkende Kommunikationskosten  
* Standardisierung  
  
### Infrastrukturkomponenten  
  
1.    Hardwareplattformen   
  
    2.    Betriebssystem-Plattformen   
  
    3.    Unternehmensweite Anwendungssysteme   
  
    4.    Datenverwaltung und -speicherung   
  
    5.    Netzwerke und Telekommunikation   
  
    6.    Internet als Plattform   
  
    7.    Unternehmensberatungen und  Systemintegratoren  
  
### Trends bei Hardwareplattformen  
  
* Mobile digitale IT-Plattformen  
    * Konsumerisierung  
          
        = IT-Technik von Konsumgütermarkt findet Einzug in Unternehmen  
  
* Cloud Computing  
      
    NIST Essential Characteristics of Cloud Computing  
  
* Virtualisierung  
* Quantum Computing  
* Green Computing  
  
### Trends bei Softwareplattformen  
  
* Linux and Open-Source  
    * UNIX  
    * Free Software (GNU)  
* SOA  
      
    Service Oriented Architecture  
  
    * Web Services  
          
        = Software zur interoperablen Maschine-zu-Maschine-Kommunikation über Netzwerk  
  
* Software Outsourcing  
      
    Externe Bezugsquellen  
  
    * Software-Hersteller (z.Bsp. Microsoft, SAP)  
    * Software-Betreiber (z.Bsp. SAP, Data One)  
    * Externe Softwareentwicklung  
  
### Trends der Digitalisierung / Informatisierung  
  
* Gegenstände können über Netzwerk miteinander kommunizieren  
* Internet der Dinge  
* Technik wird in immer mehr Alltagsgegenständen verbaut  
* Technik wird kleiner und leistungsfähiger  
