#!/bin/sh

#Script to set up a cronjob a file upload to sshserver

if [[ $# -ne 4 ]]
then
	echo "Usage: ($0): <source_dir> <ssh_target_dir> <password> [<log_file>]"
	echo "Example: ($0): Test jdoe@exampleserver:exampledir logfile.log"
	exit 1
fi

source_dir=$1
target_dir=$3

if [[ !$source_dir -d ]]
then
	echo "($1) is not a directory"
	exit 1
fi

target_adress=($2 | cut -f1 -d":")
password=$3

exit_code=eval "sshpass  -p $password $target_adress"


if [[ $exit_code -eq 1 ]]
then
	echo "Invalid command line argument to sshpass"
	exit 1

elif [[ $exit_code -eq 2 ]]
then
	echo "Conflicting Arguments given to sshpass"
	exit 2

elif [[ $exit_code -eq 3 ]]
then
	echo "Runtime error at sshpass"
	exit 3

elif [[ $exit_code -eq 4 ]]
then
	echo "Unrecognied response from ssh(parse Error)"
	exit 4

elif [[ $exit_code -eq 5 ]]
then
	echo "Invalid/incorrect password"
	exit 5
fi

logdir=$4

empty=7

#Echoing Line seperator
echo "---------------------------------------------------------" > $logdir


echo "LAST UPLOAD AT" >> $logdir

#Echoing Current Date
date >> $logdir

#See Above
echo "---------------------------------------------------------" >> $logdir
echo "UPDATED FILES" >> $logdir

#RSYNC src_dir target_dir
#flags: -r : recursive; --progress: show progress bar; --exclude: exclude target; --update: only upload chnaged files; --rsh: execute rsh client sshpass
rsync -r --progress --exclude ".git*" --exclude ".idea" --update --rsh="sshpass -p $password ssh" $source_dir $target_dir >> $logdir

#See above
echo "---------------------------------------------------------" >> $logdir

#Count lines int logfile and cut the resulting string into a single integer
count=$(wc -l $logdir | sed 's/[^0-9]*//g')


#If lines equals 7 (magic number, empty)
if [[ $count -eq $empty ]]
then
	echo "No files changed." >> $logdir
else
	echo "$(($count-$empty)) Files changed." >> $logdir
fi 




