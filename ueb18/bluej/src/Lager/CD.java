package src.Lager;

import src.Tools.IOTools;

import src.Lager.NotNull;

import java.util.InputMismatchException;

import static src.Tools.MessageTools.getMsg;

/**
 * An implementation of Buch
 * in ueb18
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-28
 */
public class CD extends Artikel {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private String interpret;
    private String titel;
    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public CD(int artikelNummer, String artikelBezeichnung, int anfangsBestand, double artikelPreis,
              String interpret, String titel) {
        super(artikelNummer, artikelBezeichnung, anfangsBestand, artikelPreis);
        this.setInterpret(interpret);
        this.setTitel(titel);

    }
    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Gibt eine String Repräsentation eines Artikels zurück.
     *
     * @return String
     */
    public String toString() {
        int artikelNummerStellen = String.valueOf(ARTIKELNUMMER_MAX).length();
        return String.format("Artikelnr.: %" + artikelNummerStellen + "d | CD-Name:     %-55s | Bestand: %8d | Preis: %8.2f",
                artikelNummer, artikelBezeichnung, artikelBestand, artikelPreis);
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * sets the interpret of this book
     *
     * @param interpret
     */
    public void setInterpret(@NotNull String interpret) {
        if (IOTools.stringEmpty(interpret)) {
            throw new InputMismatchException(getMsg(""));
        }

        this.interpret = interpret;
    }

    /**
     * sets the title of this book
     *
     * @param titel
     */
    public void setTitel(@NotNull String titel) {
        if (IOTools.stringEmpty(titel)) {
            throw new InputMismatchException(getMsg(""));
        }

        this.titel = titel;
    }

}