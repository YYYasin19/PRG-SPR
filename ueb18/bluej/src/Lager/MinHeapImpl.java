package src.Lager;

import Tools.CoreTools;

import java.util.Arrays;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Iterator;

import static Tools.CoreTools.swap;
import static Tools.MessageTools.getMsg;

/**
 * An implementation of MinHeapImpl
 * in ueb20
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-07
 */
public class MinHeapImpl<Type extends Comparable<Type>> implements MinHeap<Type> {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    public static final int MAX_ELEMENT_COUNT = 100; //this restricts the size of the MinHeap
    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private int elementCount;
    private int maxElements;
    private Type[] elements;

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * The default constructor for this MinHeap
     *
     * @param size int size of the MinHeap -> max amount of elements it can store
     */
    public MinHeapImpl(int size) {
        if (size <= 0) throw new InputMismatchException(getMsg("SIZE_TOO_LOW"));
        if (size > MAX_ELEMENT_COUNT) throw new InputMismatchException(getMsg("SIZE_TOO_HIGH"));
        maxElements = size;
        elements = (Type[]) new Comparable[size];
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    @Override
    public boolean offer(Type type) {
        if (elementCount == maxElements) return false;

        //add to the end
        elements[elementCount] = type;
        elementCount++;

        //check for heap condition
        int parentIndex;
        int index = elementCount - 1; //start at the end

        while (index > 0) {
            //get parent
            parentIndex = (index - 1) / 2;

            //is this one smaller than his parent?
            if (elements[index].compareTo(elements[parentIndex]) < 0) {
                //yes, so swap the values:
                swap(elements, index, parentIndex);
                index = parentIndex;
            } else {
                //no, so everything is fine:
                break;
            }
        }

        return true;
    }

    @Override
    public Type poll() {
        if (elementCount == 0) return null;

        //get the value
        Type returnValue = elements[0];

        //remove first value
        swap(elements, 0, elementCount - 1);
        elementCount--;

        //heap condition
        int index = 0;

        while (index < (elementCount / 2)) {

            //left child
            int leftChild = (2 * index) + 1;

            //right child
            //this seems very complicated but it is not
            int rightChild = (2 * index) + 2 > elementCount - 1 ? (2 * index) + 1 : (2 * index) + 2;

            //get smaller child
            int smaller = leftChild;
            if (elements[leftChild].compareTo(elements[rightChild]) > 0)
                smaller = rightChild;

            //heap condition hurt?
            if (elements[index].compareTo(elements[smaller]) > 0) {
                swap(elements, index, smaller); //swap with child
                index = smaller;
            } else
                break;

        }

        /*

        In Reference to getting children to work with here:
        http://i0.kym-cdn.com/photos/images/original/001/037/655/843.jpg

        */

        return returnValue;
    }

    @Override
    public Type peek() {
        if (elementCount == 0) return null;
        return elements[0];
    }

    /* ---------------------------------------- Other Methods ------------------------------------------------------- */

    /**
     * the option to see the n-th node. The nodes are indexed in their natural order as this underlying structure uses
     * a MinHeap. So naturally with two nodes on the same level, the left index is lower than the right index as the
     * left nodes comes before the right one
     *
     * @param index int the index of the node
     * @return Type
     * @throws IndexOutOfBoundsException when the given index is < 0 or higher than the amout of items
     */
    public Type peekAt(int index) {
        if (index < 0) throw new InputMismatchException(getMsg("INDEX_TOO_LOW"));
        if (index > MAX_ELEMENT_COUNT) throw new InputMismatchException(getMsg("SIZE_TOO_HIGH"));

        return elements[index];
    }

    @Override
    public int size() {
        return elementCount;
    }

    @Override
    public boolean isEmpty() {
        return elementCount == 0;
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public Iterator<Type> iterator() {
        return Arrays.stream(elements, 0, elementCount).iterator();
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public boolean add(Type type) {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public boolean addAll(Collection<? extends Type> c) {
        if (c == null) throw new NullPointerException();
        if (c.size() > (maxElements - this.elementCount))
            throw new IllegalArgumentException(getMsg(""));

        for (Object elem : c) {
            this.offer((Type) elem);
        }

        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }


    @Override
    public Type remove() {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }


    @Override
    public Type element() {
        throw new UnsupportedOperationException(String.format(
                "The method \t'%15s()' is not supported by this MinHeap",
                CoreTools.getStackTraceMethodName(2)));
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}