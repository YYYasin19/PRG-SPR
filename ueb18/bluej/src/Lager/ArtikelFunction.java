package src.Lager;

/**
 * An Interface called ArtikelFunction
 * in ueb17
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-13
 */
@FunctionalInterface
public interface ArtikelFunction {

    void apply(Artikel artikel) throws Exception;

}
