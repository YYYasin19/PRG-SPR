package src.Functions;

import static src.Tools.IOTools.stdoutln;
import static src.Tools.MessageTools.getMsg;

/**
 * An implementation of Methods
 * in ueb17
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-11
 */
public class Methods {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */


    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final int I = 0; //Example value fo I

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private static final int N = 5; //Example Value for N


    public static void main(String[] args) {
        try {


            //a)
            applyAndPrint(I, N, (Factorial::calcFactorial));

            //b)
            applyAndPrint(I, N, (Fac::calculate));

        } catch (MethodsException e) {
            System.err.println(e);
        }
    }



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */


    /**
     * @param i int > 0
     * @param j int > 0
     * @param f MyFunction - the function that is applied to every whole number between i and j
     */
    private static void applyAndPrint(int i, int j, MyFunction f) throws MethodsException {

        if (i < 0 || j < 0) { //check the input
            throw new MethodsException(getMsg("INPUT_MUST_BE_GREATER_THAN_ZERO"));
        }

        for (int index = i + 1; index < j; index++) {


            stdoutln(f.apply(index));


        }
    }

    static class Fac{

        /**
         * Calculates the Factorial with the calculateEndRecursively function and returns it. Returns the last calculated
         * value if Java throws a RuntimeException.
         * @param x
         * @return
         */
        public static int calculate(int x){
            if (x < 0) {
                throw new IllegalArgumentException();
            }

            try {
                return calculateEndRecursively(x, 1);
            }catch(RuntimeException e){
                try{
                    return Integer.parseInt(e.getMessage());
                }catch(NumberFormatException ex){
                    return -1;
                }
            }
        }

        /**
         *Calculates the factorial EndRecursively. Throws an InputTooLargeException if Java throws a RuntimeException or
         * OutOfMemoryError with the last computed Number as a String.
         * b
         * @param x
         * @param current
         * @return
         */
        private static int calculateEndRecursively(int x, int current) {
            if (x < 1) {
                return current;
            } else {
                try {
                    calculateEndRecursively(x - 1, x * current);
                } catch (RuntimeException | OutOfMemoryError e) {
                    throw new Factorial.InputTooLargeException(Integer.toString(current));
                }
            }

            return 0;
        }
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

/**
 * Exception class for Methods
 */
class MethodsException extends Exception {

    public MethodsException(String msg) {
        super(msg);
    }

}