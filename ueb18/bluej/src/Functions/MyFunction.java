package src.Functions;

/**
 * Functional Interface called Functions.MyFunction
 * in ueb17
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-10
 */
@FunctionalInterface
public interface MyFunction {

    public int apply(int i);

}
