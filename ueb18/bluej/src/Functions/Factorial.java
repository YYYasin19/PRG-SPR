package src.Functions;

/**
 * An implementation of Factorial. This class can be used to calculate the factorial of a number.
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-13
 */
public class Factorial {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Calculates the Factorial with the calculateEndRecursively function and returns it. Returns the last calculated
     * value if Java throws a RuntimeException.
     * @param x
     * @return
     */
    public static int calcFactorial(int x) {
        if (x < 0) {
            throw new IllegalArgumentException();
        }

        try {
            return calculateEndRecursively(x, 1);
        }catch(RuntimeException e){
            try{
                return Integer.parseInt(e.getMessage());
            }catch(NumberFormatException ex){
                return -1;
            }
        }

    }

    /**
     *Calculates the factorial EndRecursively. Throws an InputTooLargeException if Java throws a RuntimeException or
     * OutOfMemoryError with the last computed Number as a String.
     * b
     * @param x
     * @param current
     * @return
     */
    private static int calculateEndRecursively(int x, int current) {
        if (x < 1) {
            return current;
        } else {
            try {
                calculateEndRecursively(x - 1, x * current);
            } catch (RuntimeException | OutOfMemoryError e) {
                throw new InputTooLargeException(Integer.toString(current));
            }
        }

        return 0;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * private static nested class for a custom exception
     */
    public static class InputTooLargeException extends RuntimeException{
        private String message;

        public InputTooLargeException(String s){
            this.message = s;
        }

        @Override
        public String getMessage(){
            if(message == null){
                return "";
            }else {
                return this.message;
            }
        }
    }

}
