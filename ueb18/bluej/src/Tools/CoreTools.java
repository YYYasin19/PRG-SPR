package src.Tools;

/**
 * An implementation of CoreTools
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-10
 */
public class CoreTools {

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    public static final float PUBLIC_EPSILON = 0.001F;


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * checks a given condition and throws a RuntimeException if the conditions equals false.
     *
     * @param condition the condition which has to equal true
     * @param message   the exception message that will be given
     * @deprecated Please use the traditional way of if (!condition) throw new Exception instead
     */
    public static void check(boolean condition, String message) throws RuntimeException {

        if (!condition) {
            throw new RuntimeException(message);
        }

    }

    /**
     * checks a given condition and throws a RuntimeException if the conditions equals false.
     *
     * @param condition has to equal true
     * @param t         exception that will be thrown
     * @throws Exception
     * @deprecated Please use the traditional way of if (!condition) throw new Exception instead
     */
    public static void check(boolean condition, Exception t) throws Exception {

        if (!condition) {
            throw t;
        }

    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * prints the given String with System.out.println(s)
     *
     * @param s string to print
     */
    public static void println(String s) {
        System.out.println(s);
    }

    /* -------------------------------------------------------------------------------------------------------------- */


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
