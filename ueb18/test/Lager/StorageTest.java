package Lager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static org.junit.Assert.*;

public class StorageTest {

    private Storage<Artikel> artikelStorage;
    private Artikel[] testArtikel;

    /* ---------------------------------------- Test-Methods -------------------------------------------------------- */

    @Test
    public void testAdd() {
        artikelStorage.add(testArtikel[0]);
        assertTrue(artikelStorage.contains(testArtikel[0],
                (artikel, artikel2) -> artikel.getArtikelNummer() == artikel2.getArtikelNummer()));

    }

    /* ---------------------------------------- Before -------------------------------------------------------------- */
    @Before
    public void setUp() {
        artikelStorage = new Storage<Artikel>(120);
        testArtikel = new Artikel[]{
                new Artikel(1000, "Test-Artikel-1000"),
                new Artikel(1001, "Test-Artikel 1001"),
                new Artikel(1002, "Test-Artikel 1002"),

        };

        //batch adds all the Artikel Objects to the Storage
        artikelStorage.add(testArtikel,
                (artikel, artikel2) -> artikel.getArtikelNummer() == artikel2.getArtikelNummer());
    }

    @Test(expected = StorageException.class)
    public void testAddException() throws StorageException {

        artikelStorage.add(testArtikel[0], Artikel::equals); //adds the Item again and checks with Articles equals method
    }

    /**
     * tests if the given Artikel is in the storage. The Artikel are compared with Artikels equals()-Method
     */
    @Test
    public void testContains() {
        assertTrue(artikelStorage.contains(testArtikel[0], Artikel::equals));
    }

    /**
     * tests if the retrieved Artikel is right
     */
    @Test
    public void testGet() {
        assertEquals(testArtikel[0], artikelStorage.get(artikel -> artikel.getArtikelNummer() == 1000));
    }

    @Test
    public void testAddBatch() {
        int newArtikelCount = 5;
        Artikel[] artikelBatch = new Artikel[newArtikelCount];
        for (int i = 0; i < newArtikelCount; i++) {
            artikelBatch[i] = new Artikel(i + 2000, "Batch Artikel " + i);
        }
        int currItemCount = artikelStorage.getCurrentItemCount();
        artikelStorage.add(artikelBatch);
        assertTrue(artikelStorage.getCurrentItemCount() == currItemCount + newArtikelCount);
    }

    /**
     * checks if a given list is sorted for a given comparator
     *
     * @param list
     * @param comparator
     * @return
     */
    public static boolean isSorted(List<?> list, Comparator comparator) {
        Object prev = null;
        for (Object elem : list) {
            if (prev != null && comparator.compare(prev, elem) > 0) {
                return false;
            }
            prev = elem;
        }
        return true;
    }

    /**
     * changes the Bezeichnung of an Artikel Object and then retrives the object and sees if the Bezeichnung was
     * changed
     */
    @Test
    public void testApplyToItem() {
        String neueBezeichnung = "Neue Bezeichnung 1000";
        Predicate<Artikel> p = artikel -> artikel.getArtikelNummer() == 1000;
        //apply the function
        artikelStorage.applyToSomeItems(
                p,
                artikel -> artikel.setArtikelBezeichnung("Neue Bezeichnung 1000"));

        assertEquals(neueBezeichnung, artikelStorage.get(p).getArtikelBezeichnung());
    }

    @Test
    public void testToString() {
        System.out.println(artikelStorage.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetMaxItemCount() {
        artikelStorage.setMaxItemCount(-10);
    }

    @Test
    public void testSetStorageName() {
        String newName = "New Name of the Storage";
        artikelStorage.setStorageName(newName);
        assertEquals(newName, artikelStorage.getStorageName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetStorageNameException() {
        artikelStorage.setStorageName("");
    }

    //@Test
    public void testAddNull() {
        Storage<Object> testStorage = new Storage<>(1);
        testStorage.add(null); //TODO: Why this? Maybe (excpected = NullPointerException.class) ?
        assertNotNull(testStorage.get(0));
    }

    /**
     * filters the storage for a specification that can only be had once and then checks whether the number of results
     * are also only one.
     */
    @Test
    public void testFilter() {
        ArrayList<Artikel> filterResults =
                artikelStorage.filter(artikel -> artikel.getArtikelNummer() == 1000);
        if (filterResults.size() == 1) { //there is only one Artikel that matches
            assertEquals(testArtikel[0], filterResults.get(0)); //the result should be exactly this one
        } else {
            fail();
        }

    }

    /**
     * checks if a given array is sorted for a given comparator
     *
     * @param array
     * @param greaterThan BiPredicate that tells when a given object is greater than the other
     * @return
     */
    public static boolean isSorted(Artikel[] array, BiPredicate<Artikel, Artikel> greaterThan) {
        Artikel prev = null;
        for (Artikel elem : array) {
            //if prev = null, then the second condition will not be checked
            if (prev != null && !(greaterThan.test(prev, elem))) {
                return false;
            }
            prev = elem;
        }

        return true;
    }

    /* ---------------------------------------- Other Methods ------------------------------------------------------- */

    /**
     * appends a specific-String to every Artikels Bezeichnung and then checks if every Artikel contains that String
     */
    @Test
    public void testApplyToAll() {
        String appendString = "testApplyToAll";

        artikelStorage.applyToAll(
                artikel -> artikel.setArtikelBezeichnung(artikel.getArtikelBezeichnung() + appendString)
        );

        //returns just every Artikel
        ArrayList<Artikel> allArtikel = artikelStorage.filter(artikel -> (true));
        for (Artikel a : allArtikel) {
            if (!(a.getArtikelBezeichnung().contains(appendString))) {
                fail(); //fails this test
            }
        }

        //test finished!
    }

    @Test
    public void testAssignmentTasks() {

        //misceallenaos //TODO: look up how to write miscellaneous please
        String testAutor = "Joko Winterscheid";

        //functions that were required in the assignment
        Storage<Artikel> artikelStorage = new Storage<Artikel>(100);

        //add special Artikel
        artikelStorage.add(new Buch(1001,
                "Klaas Häufer Umlauf - Eine Autobiografie", 2, 19.99,
                testAutor));
        artikelStorage.add(new Buch(1024,
                "Faust und andere Kindermärchen", 100, 9.99,
                "Johann W. Goethe"));
        artikelStorage.add(new CD(1003,
                "5FDP - The Ultimate Album", 2, 24.99,
                "Five Finger Death Punch", "Far From Home"));
        artikelStorage.add(new Artikel(4285,
                "Premium Rasenmäher 3000", 577, 1.99));

        //print current storage
        System.out.println("Anfangszustand: ");
        System.out.println(artikelStorage);

        //do the operations:
        //i) Preis aller CDs um 10% erhöhen
        artikelStorage.applyToSomeItems(artikel -> artikel instanceof CD, artikel ->
                artikel.aenderePreisProzent(10));
        System.out.println("Preis aller CD's um 10% erhöht");
        System.out.println(artikelStorage);

        //ii) Preis von Artikeln mit 2 auf Lager um 5% reduzieren
        artikelStorage.applyToSomeItems(artikel -> artikel.getArtikelBestand() == 2,
                artikel -> artikel.aenderePreisProzent(-5));
        System.out.println("Preis aller Artikel mit einem Bestand von 2 um 5% reduziert");
        System.out.println(artikelStorage);

        //iii) Bücher eines geg. Autors reduzieren
        artikelStorage.applyToSomeItems(artikel -> {
            if (artikel instanceof Buch) {
                Buch buch = (Buch) artikel;
                return buch.getAutor().equals(testAutor);
            }

            return false;
        }, artikel -> artikel.aenderePreisProzent(-5));
        System.out.println(String.format("Bücher des Autors %s um 5 Prozent reduziert", testAutor));
        System.out.println(artikelStorage);

        //iv) Kombination aus 1 und 3
        artikelStorage.applyToSomeItems(artikel -> {

            if (artikel instanceof CD) {
                return true;
            } else if (artikel instanceof Buch) {
                Buch buch = (Buch) artikel;
                return buch.getAutor().equals(testAutor);
            }

            return false;

        }, artikel -> {

            if (artikel instanceof CD) {
                artikel.aenderePreisProzent(10);
            } else if (artikel instanceof Buch) {
                artikel.aenderePreisProzent(-5);
            } //eigentlich muss die zweite Bedingung hier nicht geprüft werden, da nur Bücher und CDs reinkommen

        });
        System.out.println("1 und 3 kombiniert, also:");
        System.out.println("Preis aller Artikel mit einem Bestand von 2 um 5% reduziert");
        System.out.println(String.format("Bücher des Autors %s um 5 Prozent reduziert", testAutor));
        System.out.println(artikelStorage);

        //v) Liste aller Bücher sortiert nach Autor:
        /*
        ArrayList<Artikel> artikels = artikelStorage.filter(artikel -> artikel instanceof Buch);
        artikels.sort((artikel1, artikel2) -> (((Buch) artikel1).getAutor().compareTo(((Buch) artikel2).getAutor())));
        System.out.println("Alle Bücher nach Autor sortiert");
        for (Artikel a : artikels) {
            System.out.println(a);
        }*/

        List<Artikel> artikels = artikelStorage.getItems(
                //Predicate
                artikel -> artikel instanceof Buch,
                //BiPredicate - when is artikel greater than artikel2
                (artikel, artikel2) ->
                        (((Buch) artikel).getAutor().compareTo(((Buch) artikel2).getAutor()) > 0)
        );
        System.out.print("Alle Bücher nach Autor sortiert ausgeben:\n");
        for (Artikel a : artikels) {
            System.out.println(a);
        }


    }

    /* ---------------------------------------- After --------------------------------------------------------------- */
    @After
    public void tearDown() {
        //Mr. Gorbachev, open this gate. Mr. Gorbachev, tear down this method!
    }


}