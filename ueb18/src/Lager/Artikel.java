package Lager;


import Tools.IOTools;
import com.sun.istack.internal.NotNull;

import static Tools.MessageTools.getMsg;
import static Tools.NumberTools.roundNDigits;

/**
 * Eine einfache Klasse "Artikel" zum Erstellen und Bearbeiten von Artikeln.
 *
 * @author Yasin Tatar
 * @version 1.0
 */
public class Artikel {
    /*---------- Konstanten ---------- */
    // Maximale und Minimale Artikelnummer (eingeschlossen)
    public static final int ARTIKELNUMMER_MIN = 1000;
    public static final int ARTIKELNUMMER_MAX = 9999;
    //Mindestbestand = Wie viele Artikel müssen mindestens vorhanden sein?
    protected static final int MINDESTBESTAND = 0;
    /*---------- Attribute ---------- */
    protected int artikelNummer;
    protected String artikelBezeichnung;
    protected int artikelBestand;
    protected double artikelPreis;

    /* ---------------------------------------- Konstruktoren ---------------------------------------- */

    /**
     * Ein Konstruktor für einen Artikel
     *
     * @param artikelNummer:      Die Nummer die der Artikel hat. Muss 4-Stellig sein.
     * @param artikelBezeichnung: Die Bezeichnung des Artikels.
     * @param anfangsBestand:     Bestand den der Artikel zu Beginn hat.
     * @param artikelPreis:       Der Preis des Artikels
     */
    public Artikel(int artikelNummer, String artikelBezeichnung, int anfangsBestand, double artikelPreis) {
        this.setArtikelNummer(artikelNummer);
        this.setArtikelBezeichnung(artikelBezeichnung);
        this.setArtikelBestand(anfangsBestand);
        this.setArtikelPreis(artikelPreis);
    }

    /**
     * Ein Konstruktor für einen Artikel
     *
     * @param artikelNummer:      Die Nummer die der Artikel hat. Muss 4-Stellig sein.
     * @param artikelBezeichnung: Die Bezeichnung des Artikels.
     * @param anfangsBestand:     Bestand den der Artikel zu Beginn hat.
     */
    public Artikel(int artikelNummer, String artikelBezeichnung, int anfangsBestand) {
        this(artikelNummer, artikelBezeichnung, anfangsBestand, 0.0);
    }

    /**
     * Ein kompakter Konstruktor für einen Artikel. Der Bestand ist zu Beginn leer.
     *
     * @param artikelNummer:      Die Nummer die der Artikel hat. Muss 4-Stellig sein.
     * @param artikelBezeichnung: Die Bezeichnung des Artikels.
     */
    public Artikel(int artikelNummer, String artikelBezeichnung) {
        /* Ruft den anderen Konstruktor auf */
        this(artikelNummer, artikelBezeichnung, MINDESTBESTAND, 0.0);
    }


    /* ---------------------------------------- Methoden ---------------------------------------- */

    /**
     * copies a given article and returns a new Artikel object
     * if there was an error whilst creating the new object, a null reference is returned
     *
     * @param artikel Artikel-Object
     * @return Artikel - Object
     */
    public static Artikel kopiereArtikel(Artikel artikel) {
        try {
            return new Artikel(artikel.getArtikelNummer(), artikel.getArtikelBezeichnung(), artikel.getArtikelBestand(), artikel.getArtikelPreis());
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }

        return null;
    }

    /**
     * Erhöht den Artikelbestand um einen bestimmten Wert > 0.
     *
     * @param zugang: Anzahl an Artikeln, die hinzugefügt werden sollen.
     */
    public void zugang(int zugang) {
        if (zugang < 0)
            throw new IllegalArgumentException(getMsg("INPUT_MUST_BE_GREATER_THAN_ZERO"));

        /* Nimmt sich den aktuellen Artikelbestand und setzt dem Artikel
         * diesen Wert + den Zugang als neuen Artikelbestand */
        this.setArtikelBestand(this.getArtikelBestand() + zugang);
    }

    /**
     * Reduziert den Artikelbestand um einen bestimmten Wert > 0.
     *
     * @param abgang Anzahl der Artikel die entnommen werden.
     */
    public void abgang(int abgang) {
        /* Überprüfung: Anzahl der Artikel die
         * entnommen werden, muss größer 0 sein.
         * Anmerkung: Theoretisch ist das möglich und es würde auch stimmen
         * (Wenn ich -3 Artikel entnehme, füge ich ja 3 hinzu, stimmt ja semantisch
         * -> Macht aber praktisch keinen Sinn. )
         */
        if (abgang < 0)
            throw new IllegalArgumentException(getMsg("INPUT_MUST_BE_GREATER_THAN_ZERO"));
        this.setArtikelBestand(this.getArtikelBestand() - abgang);
    }

    /**
     * increases the price of an Artikel by neuerPreis
     *
     * @param neuerPreis
     * @throws Exception when neuerPreis < 0
     */
    public void erhoehePreis(double neuerPreis) {
        if (neuerPreis < 0.0)
            throw new IllegalArgumentException(getMsg("ARTICLE_PRICE_TOO_LOW") + "\nYour input: " + neuerPreis);
        this.setArtikelPreis(this.artikelPreis + neuerPreis);
    }

    /**
     * decreases the price of an Artikel by reduktion
     *
     * @param reduktion
     * @throws Exception when reduktion < 0
     */
    public void reduzierePreis(double reduktion) {
        if (reduktion < 0.0)
            throw new IllegalArgumentException(getMsg("ARTICLE_PRICE_TOO_LOW") + "\nYour input: " + reduktion);
        this.setArtikelPreis(this.artikelPreis - reduktion);
    }

    public void aenderePreisProzent(double prozent) {
        if (prozent < -100.0)
            throw new IllegalArgumentException(getMsg("PERCENT_TOO_LOW"));
        /*
         * Hier muss der Preis auf eine gewisse Nachkommastelle gerundet werden,
         * damit da nicht z.Bsp. 100 + 10% --> 110.000000000001 steht.
         */
        double aktuellerPreis = this.getArtikelPreis();
        double faktor = 1.0 + (prozent / 100);
        double neuerPreis = aktuellerPreis * faktor;
        this.setArtikelPreis(roundNDigits(neuerPreis, 2));
    }

    /**
     * Gibt eine String Repräsentation eines Artikels zurück.
     *
     * @return String
     */
    public String toString() {
        int artikelNummerStellen = String.valueOf(ARTIKELNUMMER_MAX).length();
        return String.format("Artikelnr.: %" + artikelNummerStellen + "d | Art.-Name:   %-55s | Bestand: %8d | Preis: %8.2f",
                artikelNummer, artikelBezeichnung, artikelBestand, artikelPreis);
    }

    /**
     * Gibt den aktuellen Zustand des Artikels mit seinen Attributen über die Konsole aus.
     * Verwendet die toString()-Methode der Klasse.
     */
    public void ausgabeAktuellerZustand() {
        System.out.print("Aktueller Stand des Artikels: \n");
        System.out.println(this.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Artikel) {
            return ((Artikel) obj).getArtikelNummer() == this.getArtikelNummer();
        }

        return false;
    }

    /* ---------- Setter ---------- */
    /* setArtikelNummer wird von außen nicht benötigt, da ein Nutzer eher
     * einen neuen Artikel anlegen soll, als dem aktuellen einfach
     * eine neue Nummer geben zu können.
     * (Welche eventuell die Selbe wie ein anderer Artikel sein könnte)
     */

    /*---------- Getter ---------- */
    public int getArtikelBestand() {
        return this.artikelBestand;
    }

    /**
     * Setzt den Artikelbestand und überprüft dabei,
     * ob dieser dem Mindestbestand entspricht.
     *
     * @param artikelBestand : Der neue Bestand des Artikels
     * @throws IllegalArgumentException
     */
    public void setArtikelBestand(int artikelBestand) {
        if (artikelBestand < MINDESTBESTAND) throw new IllegalArgumentException(getMsg("ARTICLE_AMOUNT_TOO_LOW"));
        this.artikelBestand = artikelBestand;
    }

    public String getArtikelBezeichnung() {
        return this.artikelBezeichnung;
    }

    /**
     * Setzt eine neue Artikelbezeichnung und überprüft dabei,
     * ob diese Nicht-leer ist.
     *
     * @param artikelBezeichnung: Die neue Bezeichnung des Artikels
     * @throws IllegalArgumentException when
     *                                  artikelBezeichnung is empty
     */
    public void setArtikelBezeichnung(@NotNull String artikelBezeichnung) {
        /* Überprüft ob die Artikelbezeichnung NICHT leer ist. */
        if (IOTools.stringEmpty(artikelBezeichnung))
            throw new IllegalArgumentException(getMsg("ARTICLE_NAME_EMPTY"));
        this.artikelBezeichnung = artikelBezeichnung;
    }

    public int getArtikelNummer() {
        return this.artikelNummer;
    }

    /**
     * Setzt die Artikelnummer und überprüft dabei,
     * ob diese im gültigen Bereich liegt (siehe Konstanten)
     *
     * @param artikelNummer: Neue Artikelnummer
     * @throws IllegalArgumentException when
     *                                  artikelNummer < {@link Artikel#ARTIKELNUMMER_MIN} or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public void setArtikelNummer(int artikelNummer) {
        /* Überprüfung: Liegt die Artikelnummer im erlaubten Zahlenbereich? */
        if (artikelNummer < ARTIKELNUMMER_MIN || artikelNummer > ARTIKELNUMMER_MAX)
            throw new IllegalArgumentException("Die Artikelnummer muss im gültigen Zahlenbereich liegen.");

        this.artikelNummer = artikelNummer;
    }

    public double getArtikelPreis() {
        return this.artikelPreis;
    }

    public void setArtikelPreis(double neuerPreis) {
        if (neuerPreis < 0.0) throw new IllegalArgumentException(getMsg("PRICE_MUST_NOT_BE_EMPTY"));
        this.artikelPreis = neuerPreis;
    }

}

class ArtikelException extends Exception {

    public ArtikelException(String msg) {
        super(msg);
    }

}