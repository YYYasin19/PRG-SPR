package Lager;

import Tools.IOTools;
import com.sun.istack.internal.NotNull;

import java.util.InputMismatchException;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of Buch
 * in ueb18
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-28
 */
public class Buch extends Artikel {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private String autor;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public Buch(int artikelNummer, String artikelBezeichnung, int anfangsBestand, double artikelPreis, String autor) {
        super(artikelNummer, artikelBezeichnung, anfangsBestand, artikelPreis);
        this.setAutor(autor);

    }
    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Gibt eine String Repräsentation eines Artikels zurück.
     *
     * @return String
     */
    public String toString() {
        int artikelNummerStellen = String.valueOf(ARTIKELNUMMER_MAX).length();
        return String.format("Artikelnr.: %" + artikelNummerStellen + "d | Buch-Name:   %-55s | Bestand: %8d | Preis: %8.2f",
                artikelNummer, artikelBezeichnung, artikelBestand, artikelPreis);
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public String getAutor() {
        return this.autor;
    }

    /**
     * sets the author of this book
     *
     * @param autor
     */
    public void setAutor(@NotNull String autor) {
        if (IOTools.stringEmpty(autor)) {
            throw new InputMismatchException(getMsg(""));
        }

        this.autor = autor;
    }
}