package Lager;

/**
 * An implementation of StorageException
 * in ueb18
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-28
 */
public class StorageException extends Exception {

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public StorageException(String msg) {
        super(msg);
    }


}