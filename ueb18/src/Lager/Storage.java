package Lager;

import Tools.IOTools;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of Storage
 * in ueb18
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-26
 */
public class Storage<Item> {

    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final int MAX_STORAGE_SIZE = 121;
    private static final String DEFAULT_STORAGE_NAME = "Default Storage";

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private Item[] storage;
    private int maxItemCount;
    private int currentItemCount;
    private String storageName;

    /* ---------------------------------------- Constructors -------------------------------------------------------- */
    /**
     * Constructor for storage
     *
     * @param maxItemCount int Max. amount of items that can be stored
     * @param storageName  String name of the storage
     * @throws IllegalArgumentException when
     *                                  maxItemCount is negative
     */
    private Storage(int maxItemCount, String storageName) {
        //this.storage = (Item[]) Array.newInstance(this.getClass(), maxItemCount);
        //there are no better solutions
        this.storage = (Item[]) new Object[maxItemCount]; //TODO: find better solutions
        this.setStorageName(storageName);
        this.currentItemCount = 0;
        this.setMaxItemCount(maxItemCount);
    }

    /**
     * Constructor for storage
     *
     * @param maxItemCount int Max. amount of items that can be stored
     * @throws IllegalArgumentException when
     *                                  maxItemCount is negative
     */
    public Storage(int maxItemCount) {
        this(maxItemCount,DEFAULT_STORAGE_NAME);
    }

    /**
     * Constructor for storage
     *
     * @throws IllegalArgumentException when
     *                                  maxItemCount is negative
     */
    private Storage() {
        this(MAX_STORAGE_SIZE, DEFAULT_STORAGE_NAME);
    }


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * adds an Item to the storage without checking if it already exists
     *
     * @param item Item to be added
     */
    public void add(@NotNull final Item item) {
        this.storage[currentItemCount] = item;
        currentItemCount++;
    }

    /**
     * adds an item to the storage after checking if it is already stored
     *
     * @param item       Item to be added
     * @param duplicates BiPredicate that defines when two Items are equal. You can pass the equals()-Method of
     *                   the Item-Class here with: Item::equals if you want (Java 8 and Later)
     * @throws StorageException when the Storage is full or the Item is already contained
     */
    public void add(@NotNull final Item item, BiPredicate<Item, Item> duplicates) throws StorageException {

        if (contains(item, duplicates)) {
            throw new StorageException(getMsg("ITEM_ALREADY_FOUND"));
        } else if (this.isFull()) {
            throw new StorageException(getMsg("STORAGE_FULL"));
        }

        //add item
        currentItemCount++;
        this.storage[currentItemCount - 1] = item;
    }

    /**
     * adds an array of items to the storage but only if there is enough space
     * for all items left
     *
     * @param items Item[] array of items that have to be added
     * @throws InputMismatchException when there items.length > storageSize
     */
    public void add(@NotNull final Item[] items) {

        int itemsCount = items.length;
        if ((currentItemCount + itemsCount) > maxItemCount) {
            throw new InputMismatchException("There is not enough place for this amount of Items");
        }

        //please test if this works:
        System.arraycopy(items,0,storage,currentItemCount,itemsCount);
        currentItemCount += itemsCount;
    }

    /**
     * adds the items in the given array to this storage. if there is an error with adding one item
     * (e.g. BiPredicate duplicates states that this storage already contains that item) then
     * this item will be skipped.
     * @param items Item[] items that will be added
     * @param duplicates BiPredicate<Item, Item> tests if two items are equal
     */
    public void add(@NotNull final Item[] items, BiPredicate<Item, Item> duplicates) {

        for (Item item : items) {
            try {
                this.add(item, duplicates);
            } catch (StorageException st) {
                System.err.println(st);
            }

        }

    }

    /**
     * checks whether any item equals the given item in this storage
     * uses the equals()-Method of the the {@link Item} class
     *
     * @param item {@link Item}
     * @return true when this storage contains the given item
     */
    public boolean contains(@NotNull final Item item) {
        return contains(item, Item::equals); //all hail method parameters
    }

    /**
     * checks whether any item equals the given item in this storage
     * uses the equals()-Method of the the {@link Item} class
     *
     * @param item   {@link Item}
     * @param equals {@link BiPredicate} the Predicate for determining when two Instances of {@link Item} are equal
     * @return true when this storage contains the given item
     */
    public boolean contains(@NotNull final Item item, BiPredicate<Item, Item> equals) {
        Iterator<Item> iterator = new StorageIterator<>();
        while (iterator.hasNext()){
            if (equals.test(item,iterator.next()))
                return true;
        }

        return false;
    }

    /**
     * returns the first Item that matches the given Predicate or else null
     *
     * @param p Predicate
     * @return first matching item or null
     */
    @Nullable
    public Item get(Predicate<Item> p) {
        Iterator<Item> itemIterator = new StorageIterator<>();
        Item current;

        while (itemIterator.hasNext()){
            current = itemIterator.next();
            if (p.test(current)) {
                return current;
            }
        }

        return null;
    }

    /**
     * returns the Item at the Index index
     *
     * @param index int starting at 0
     * @return Item
     */
    public Item get(int index) {
        if (index > currentItemCount){
            throw new InputMismatchException(getMsg(""));
        }

        return storage[index];
    }

    /**
     * returns a list of items that fulfill the filter-Predicate and are sorted after the greaterThan
     * BiPredicate
     *
     * @param filter       {@link Predicate}
     * @param greatherThan {@link BiPredicate}
     * @return {@link List}
     */
    public List<Item> getItems(Predicate<Item> filter, BiPredicate<Item, Item> greatherThan) {
        List<Item> list = this.filter(filter);
        list.sort((o1, o2) -> {
            if (greatherThan.test(o1, o2)) {
                return 1;
            } else {
                return -1;
            }
        });

        return list;
    }

    /**
     * returns any Items that match the given Predicate
     *
     * @param p Predicate
     * @return ArrayList of Items that match
     */
    public ArrayList<Item> filter(Predicate<Item> p) {
        ArrayList<Item> matches = new ArrayList<>();
        Iterator<Item> iterator = new StorageIterator<>();
        Item current;
        while (iterator.hasNext()){
            current = iterator.next();

            if (p.test(current)){
                matches.add(current);
            }
        }

        return matches;
    }

    /**
     * applies the given function to all items in-place
     *
     * @param f Function to be applied
     */
    public void applyToAll(Consumer<Item> f) {
        Iterator<Item> itemIterator = new StorageIterator<>();
        while (itemIterator.hasNext()){
            f.accept(itemIterator.next());
        }
    }

    /**
     * applies the given function to every item that fulfills the Predicate
     *
     * @param p Predicate
     * @param f Function
     */
    public void applyToSomeItems(Predicate<Item> p, Consumer<Item> f) {
        Iterator<Item> itemIterator = new StorageIterator<>();
        Item current;
        while (itemIterator.hasNext()){
            current = itemIterator.next();
            if (p.test(current)) {
                f.accept(current);
            }
        }
    }

    /**
     * returns a sorted Array of the items
     *
     * @param greaterThan {@link BiPredicate} tells when an Item is greater than another
     * @return Item[]
     */
    public Object[] getSorted(BiPredicate<Item, Item> greaterThan) {
        Item[] sortedItems = Arrays.copyOf(this.storage,currentItemCount);

        Arrays.sort(sortedItems, (o1, o2) -> {
            if (greaterThan.test(o1, o2)) {
                return 1;
            } else {
                return -1;
            }
        });

        return sortedItems;
    }

    @Override
    public String toString() {
        Iterator<Item> itemIterator = new StorageIterator<>();
        StringBuilder result = new StringBuilder(String.format("--- %s --- \n",this.storageName));

        while (itemIterator.hasNext()){
           result.append(itemIterator.next()).append("\n");
        }

        return result.toString();
    }

    /**
     * checks whether this storage is filled or not
     * @return true if no item can be added to the storage anymore
     */
    public boolean isFull(){
        return this.currentItemCount == this.maxItemCount;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * sets the maximum item count
     *
     * @param maxItemCount
     * @throws IllegalArgumentException when
     *                                  item count < 1
     */
    public void setMaxItemCount(int maxItemCount) {
        if (maxItemCount < 1) throw new IllegalArgumentException(getMsg(""));
        if (maxItemCount > MAX_STORAGE_SIZE) {
            System.err.println(getMsg("MAX_LAGER_SIZE"));
            System.err.println("Storage_Alt size was set to default max value: " + MAX_STORAGE_SIZE);
            this.maxItemCount = MAX_STORAGE_SIZE;
        }
        this.maxItemCount = maxItemCount;
    }

    public int getCurrentItemCount() {
        return this.currentItemCount;
    }

    public String getStorageName() {
        return this.storageName;
    }

    /**
     * sets the non-empty storage name
     *
     * @param storageName name of the storage
     * @throws IllegalArgumentException when
     *                                  storageName is empty
     */
    public void setStorageName(@NotNull String storageName) {
        if (IOTools.stringEmpty(storageName)) throw new IllegalArgumentException(getMsg("EMPTY_STRING"));
        this.storageName = storageName;
    }

    public Iterator<Item> iterator() {
        return new StorageIterator<>();
    }

    class StorageIterator<Type> implements Iterator<Type> {

        int currentIndex = 0; //the index of the article that will be returned with next()

        @Override
        public boolean hasNext() {
            return currentIndex < currentItemCount;
        }

        @Override
        public Type next() {
            return (Type) storage[currentIndex++];
        }
    }
}

