package Functions;

import java.util.function.Predicate;

/**
 * An implementation of ConditionateMyFunction
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-13
 */
@FunctionalInterface
public interface ConditionateMyFunction extends MyFunction {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Returns Lambda that tests and integer for a given Predicate. Returns the applied lambda if the test evaluates
     * to true, returns 0 if it evaluates to false.
     * @param predicate
     * @return
     */
    default MyFunction conditionateInput(Predicate<Integer> predicate) {
        return x -> (predicate.test(x) ? this.apply(x) : 0);
    }

    /**
     * Applies the given lambda to an Integer, then tests it with the predicate argument. If that tests evaluates to true,
     * the applied Integer will be returned, if it evals to false the lambda will return 0.
     * @param predicate
     * @return
     */
    default MyFunction conditionateOutput(Predicate<Integer> predicate) {
        return x -> (predicate.test(this.apply(x)) ? this.apply(x) : 0);
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
