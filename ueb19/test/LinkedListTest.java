import org.junit.Before;
import org.junit.Test;
import org.omg.CORBA.INTERNAL;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static Tools.NumberTools.createNumbersInRange;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class LinkedListTest {

    private LinkedList<Integer> linkedList;


    @Before
    public void setUp() {
        linkedList = new LinkedList<>();
    }

    @Test
    public void testSize() {
        assertEquals(linkedList.size(), 0);
    }

    @Test
    public void testIsEmpty1() {
        assertTrue(linkedList.isEmpty());
    }

    @Test
    public void testContains() {
        linkedList.add(1);
        assertTrue(linkedList.contains(1));

        linkedList.add(2);
        linkedList.add(3);

        assertTrue(linkedList.contains(3));
        assertFalse(linkedList.contains(4));
        assertFalse(linkedList.contains(15));
    }

    @Test (expected = NullPointerException.class)
    public void testContainsNull(){
        linkedList.contains(null);
    }

    @Test
    public void testAdd1(){
        linkedList.add(1);
        assertTrue(linkedList.contains(1));
        assertFalse(linkedList.add(null));
        
        linkedList.add(2);
        assertTrue(linkedList.contains(2));
    }


    @Test
    public void testIterator3() {
        linkedList = new LinkedList<>();
        assertFalse(linkedList.iterator().hasNext());
    }


        
        
    @Test(expected = NoSuchElementException.class)
    public void testIterator4() {
        linkedList = new LinkedList<>();
        linkedList.iterator().next(); //should throw a NoSuchElementException
    }

    @Test
    public void testRemove() {
        int start = 0;
        int end = new Integer(10);
        linkedList.addAll(createNumbersInRange(start, end, a -> a));

        linkedList.remove((Object) start);
        linkedList.remove((Object) end);

        assertFalse(linkedList.contains(start));
    }

    @Test
    public void testAddNull() {
        assertFalse(linkedList.add(null));
    }


    @Test
    public void testRemoveAtIndex2() {
        int start = 0;
        int end = 10;
        int removeIndex = 1;

        //add integers with value = index at every index
        linkedList.addAll(createNumbersInRange(start, end, a -> a));

        //remove the element at index
        linkedList.remove(removeIndex);

        assertFalse(String.format("The element at %d was not removed apparently.", removeIndex),
                linkedList.contains(removeIndex));
    }

    @Test
    public void testAddAll() {
        //generate list
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            list.add(i * i);
        }

        //add all to linked list
        linkedList.addAll(list);

        //check if added
        for (int i = 1; i < 10; i++) {
            if (!(list.contains(i * i))) {
                fail();
            }
        }

        //something like an opposite to fail()
        assertTrue(true);
    }

    public void iterator() {
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Iterator<Integer> iterator = this.linkedList.iterator();

        assertEquals( iterator.next(), (Integer) 1);
        assertEquals(iterator.next(), (Integer) 2);
        assertEquals(iterator.next(), (Integer) 3);
        
    }
        
   
    public void testAdd2() {
        linkedList.add(1);
        assertTrue(linkedList.contains(1));
        assertFalse(linkedList.add(null));
    }
    
    @Test
    public void toArray() {
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Object[] array = new Integer[]{1, 2, 3};

        Object[] actualArray = this.linkedList.toArray();

        assertArrayEquals(actualArray, array);

    }

    @Test
    public void testClear() {
        linkedList.clear();
        assertEquals("The size of the list was not set to 0", 0, linkedList.size());
        assertTrue(linkedList.isEmpty());
    }

    @Test
    public void testGet() {
        int start = 0;
        int end = 10;

        ArrayList<Integer> nums = createNumbersInRange(start, end, a -> a);
        linkedList.addAll(nums);

        //yes, assertEquals() here would sound better but trust me.
        for (int index = start; index < end; index++) {
            assertTrue("The get()-Method returned the false output",
                    linkedList.get(index) == index);
        }

    }


    @Test
    public void testSet() {
        int start = 0;
        int end = 10;
        ArrayList<Integer> nums = createNumbersInRange(start, end, a -> a);
        ArrayList<Integer> numsSquare = createNumbersInRange(start, end, a -> a * a);

        //add the elements
        linkedList.addAll(nums);

        //replace them
        for (int i = start; i < end; i++) {
            linkedList.set(i, numsSquare.get(i));
        }

        //check if they are here
        for (int index = start; index < end; index++) {
            assertTrue("The set()-Method did not replace the value at index" + index,
                    linkedList.get(index) == (index * index));
        }
    }

    @Test
    public void testAddAtIndex() {
        //add the numbers from 1 to 10 (excl.) to the list
        int start = 1;
        int end = 10;
        ArrayList<Integer> nums = createNumbersInRange(start, end, a -> a);
        linkedList.addAll(nums);

        //add something at the beginning
        linkedList.add(0, 0);

        //now add something to the end
        linkedList.add(linkedList.size(), end);

        System.out.println(linkedList);

        for (int index = start; index < end; index++) {

            if (!(linkedList.get(index) == index)) {
                fail();
            }
        }


        //now let us test adding something in the middle
        linkedList = new LinkedList<>();
        linkedList.add(0); //index 0
        linkedList.add(1); //index 1
        linkedList.add(2); //index 2
        linkedList.add(4); //index 3

        linkedList.add(3, 3);

        for (int index = 0; index < 5; index++) {
            if (linkedList.get(index) != index) {
                fail();
            }
        }

        //everything is fine
    }

    @Test
    public void testRemoveAtIndex() {
        linkedList.addAll(createNumbersInRange(0, 10, a -> a));

        //remove first and last element
        linkedList.remove(0);
        linkedList.remove(linkedList.size() - 1);
    }

    @Test
    public void testRemoveIf(){
        int start = 0;
        int end = 10;
        int removeNumber = 3; //has to be start < removeNumber < end
        // creates a list of square numbers
        linkedList.addAll(createNumbersInRange(start,end, a -> a*a));
        //remove all square numbers after 5
        linkedList.removeIf(i -> i > removeNumber*removeNumber);

        System.out.println(linkedList);

        //check
        for (Integer i : linkedList){
            if (i > removeNumber*removeNumber) fail();
        }
    }

    @Test
    public void testIndexOf() {
        int start = 0;
        int end = 10;
        linkedList.addAll(createNumbersInRange(start, end, a -> a));

        for (int index = start; index < end; index++) {
            if (!(linkedList.indexOf(index) == index)) fail();
        }
    }


    @Test
    public void testIterator() {
        linkedList = new LinkedList<>();
        assertFalse(linkedList.iterator().hasNext());
    }

    @Test (expected = NoSuchElementException.class)
    public void testIterator2() {
        linkedList = new LinkedList<>();
        linkedList.iterator().next(); //should throw a NoSuchElementException
    }

    @Test
    public void testIteratorRemove(){
        linkedList.add(1);

        Iterator<Integer> iterator= linkedList.iterator();

        iterator.next();
        iterator.remove();

        assertTrue(linkedList.isEmpty());

    }

    @Test
    public void testIteratorRemove2(){
        linkedList.add(1);
        linkedList.add(2);

        Iterator<Integer> iterator= linkedList.iterator();

        iterator.next();
        iterator.remove();

        assertFalse(linkedList.contains((Integer) 1));

    }

    @Test (expected = IllegalStateException.class)
    public void testIteratorRemove3(){
        linkedList.add(1);
        linkedList.iterator().remove();
    }

    @Test (expected = IllegalStateException.class)
    public void testIteratorRemove4(){

        linkedList.add(1);

        Iterator<Integer> iterator= linkedList.iterator();
        iterator.next();
        iterator.remove();
        iterator.remove();
    }

    @Test (expected = IllegalStateException.class)
    public void testIteratorRemove5(){
        linkedList.iterator().remove();
    }

    @Test
    public void testListIterator0(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        ListIterator<Integer> literator = linkedList.listIterator();

        assertFalse(literator.hasPrevious());
    }

    @Test (expected = NoSuchElementException.class)
    public void testListIterator1(){
        linkedList.add(1);

        linkedList.listIterator().previous();
    }

    @Test
    public void testListIterator2(){
        linkedList.add(1);

        ListIterator<Integer> literator = linkedList.listIterator();

        assertTrue(literator.next().equals(1));

        Integer previous = literator.previous();

        assertTrue(previous.equals(1));
    }



    @Test
    public void testListIterator3() {

        for (int x = 0; x < 20; x++) {
            linkedList.add(x);
        }

        System.out.println(linkedList);

        ListIterator<Integer> literator = linkedList.listIterator();

        while (literator.hasNext()) {
            if (literator.next() % 2 == 0) {
                literator.remove();
            }
        }

        System.out.println("Now all of the even numbers should be removed");

        System.out.println(linkedList);
    }


    @Test
    public void testStream(){
        linkedList.addAll(createNumbersInRange(0,10, a -> a));

        Stream<Integer> stream = linkedList.stream();

        stream.forEach(System.out::println);
    }

    @Test
    public void testSort(){
        int numCount = 5;
        for (int i = 0; i < numCount; i++){
            linkedList.add(ThreadLocalRandom.current().nextInt());
        }

        //now sort the list
        linkedList.sort( (a,b) -> a.equals(b) ? 0 : (Integer.compare(a,b)));

        int lastNumber = linkedList.get(0);

        for (Integer i : linkedList){
            if (i < lastNumber) fail();
            lastNumber = i;
        }

        System.out.println(linkedList);
    }


}