package Tools;


import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * An implementation of NumberTools
 * in ueb16
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-07
 */
public class NumberTools {
    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * returns a randrom float
     *
     * @return float
     */
    public static float getRandomFloat() {
        return ThreadLocalRandom.current().nextFloat();
    }

    public static float getRandomFloat(int min, int max) {
        return min + ThreadLocalRandom.current().nextFloat() * (max - min);
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * returns a random integer
     *
     * @return int
     */
    public static int getRandomInt() {
        return ThreadLocalRandom.current().nextInt();
    }

    /**
     * returns a random integer between 0 and range
     *
     * @param range
     * @return int
     */
    public static int getRandomInt(int range) {
        return ThreadLocalRandom.current().nextInt(range);
    }

    public static int getRandomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    /**
     * compares two floats and returns true if they are equal
     *
     * @param f1      float one
     * @param f2      float two
     * @param epsilon margin of error
     * @return boolean
     */
    public static boolean floatEquals(float f1, float f2, float epsilon) {
        return Math.abs(f1 - f2) < epsilon;
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * rounds a given double to a given scale
     *
     * @param number double
     * @param scale  int
     * @return double - rounded number
     */
    public static double roundNDigits(double number, int scale) {

        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(scale, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    /**
     * creates a arraylist of values between to indices and a function that is applied to each value
     * @param start int starting index (inclusive)
     * @param end int end index (exclusive)
     * @param f {@link Function} that takes a number and returns a number
     * @param <T> extends Number
     * @return {@link ArrayList} with the corresponding values
     */
    public static <T extends Number> ArrayList<T> createNumbersInRange(int start, int end, Function<T,T> f){
        ArrayList<T> results = new ArrayList<>();

        for (int i = start; i < end; i++) {
            results.add(f.apply((T) new Integer(i)));
        }

        return results;
    }


}
