import java.util.Iterator;

/**
 * An implementation of Node
 * in DoubleLinkedList
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-29
 */
public class Node<Type> {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    private Type value;
    private Node<Type> parent;
    private Node<Type> child;


    public Node(){
    }

    public Node(Type t){
        if(t == null){
            throw new IllegalArgumentException();
        }else{
            this.value = t;
        }
    }
    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public boolean hasParent(){
        return this.parent != null;
    }

    public boolean hasChild(){
        return this.child != null;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public Node<Type> getParent() {
        return parent;
    }

    public void setParent(Node<Type> parent) {
        this.parent = parent;
    }

    public Node<Type> getChild() {
        return child;
    }

    public void setChild(Node<Type> child) {
        this.child = child;
    }

    public Type getValue(){
        return this.value;
    }

    public void setValue(Type value) {
        this.value = value;
    }

}
