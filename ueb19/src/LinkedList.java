import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * An implementation of LinkedList
 * in DoubleLinkedList
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-29
 */
public class LinkedList<Type> implements List<Type> {


    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private Node<Type> root;
    //to avoid Over flows
    long currentSize;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    {
        //this should be done in every possible constructor
        this.currentSize = 0;
    }

    public LinkedList() {
    }


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * @inheritDoc
     */
    @Override
    public int size() {
        return (currentSize <= Integer.MAX_VALUE) ? (int) currentSize : Integer.MAX_VALUE;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean contains(Object o) {

        if(o == null) {
            throw new NullPointerException();

        } else if (this.isEmpty()) {
            return false;

        } else {

            NodeIterator<Node<Type>> nodeIterator = this.nodeIterator();

            while (nodeIterator.hasNext()) {

                Node<Type> current = nodeIterator.next();

                if (current.getValue().equals(o)) {
                    return true;
                }
            }

            return false;
        }
    }


    /**
     * NodeIterator interface that appends the method last and getIndex to the Iterator Interface
     * @param <E>
     */
    private interface NodeIterator<E> extends Iterator<E>{

        E last();

        int getIndex();
    }

    /**
     * returns an iterator over the Nodes in this collection.
     *
     * @return An iterator over the Nodes in this collection.
     */
    private NodeIterator<Node<Type>> nodeIterator(){

        return new NodeIterator<Node<Type>>() {

            Node<Type> previous = null;

            boolean removeHasBeenCalledBefore = false;

            Node<Type> current = root;
            private int index = 0;


            /**
             * @inheritDoc
             */
            @Override
            public boolean hasNext() {
                return current != null;
            }

            /**
             * @inheritDoc
             */

            @Override
            public Node<Type> next() {
                if(!this.hasNext()){
                    throw new NoSuchElementException();
                }

                Node<Type> returnedNode = current;
                current = current.getChild();
                index++;

                previous = returnedNode;

                removeHasBeenCalledBefore = false;

                return returnedNode;
            }


            /**
             * @inheritDoc
             */
            @Override
            public void remove(){
                if(previous == null){
                    throw new IllegalStateException();
                }else if(this.removeHasBeenCalledBefore){
                    throw new IllegalStateException();
                } else{

                    Node<Type> previousParent = previous.getParent();
                    Node<Type> previousChild = previous.getChild();


                    if (previous.hasParent()){
                        previousParent.setChild(previousChild);
                    } else {
                        root = previousChild;
                    }

                    if(previous.hasChild()){
                        previousChild.setParent(previousParent);

                    }
                    this.removeHasBeenCalledBefore = true;
                    currentSize--;
                }
            }

            /**
             * Returns the last node of the collection
             */
            @Override
            public Node<Type> last(){

                Node<Type> nextNode = current;

                while(nextNode.hasChild()){
                    nextNode = this.next();
                }

                return nextNode;
            }

            /**
             * returns the current Index of the iterator
             * @return
             */
            @Override
            public int getIndex(){
                return this.index;
            }
        };
    }

    /**
     * @inheritDoc
     */
    @Override
    public Iterator<Type> iterator() {

            return new Iterator<Type>() {


                NodeIterator<Node<Type>> nodeIterator = nodeIterator();

                /**
                 * @inheritDoc
                 */
                @Override
                public boolean hasNext() {
                return nodeIterator.hasNext();
            }

                /**
                 * @inheritDoc
                 */
                @Override
                public Type next() {
                    return nodeIterator.next().getValue();
                }

                /**
                 * @inheritDoc
                 */
                @Override
                public void remove() {
                    nodeIterator.remove();
                }

            };
    }

    /**
     * Returns an array containing all of the elements in this collection. All elements are returned in the order
     * the iterator returns them. The returned array will be "safe" in that no references to it are maintained
     * by this collection. The caller is thus free to modify the returned array.
     *
     * @return an array containing all of the elements in this collection.
     */
    @Override
    public Object[] toArray() {

        if (this.isEmpty()) {
            return null;
        } else {


            Object[] objects = new Object[this.size()];

            int counter = 0;

            Iterator<Node<Type>> nodeIterator = this.nodeIterator();

            while (nodeIterator.hasNext()) {
                objects[counter] = nodeIterator.next().getValue();
                counter++;
            }

            return objects;

        }

    }

    /**
     * Returns an array containing all of the elements in this collection; the runtime type of the returned array is
     * that of the specified array. If the collection fits in the specified array, it is returned therein.
     * Otherwise, a new array is allocated with the runtime type of the specified array and the size of this collection.
     *
     * If this collection fits in the specified array with room to spare
     * (i.e., the array has more elements than this collection), the element in the array immediately
     * following the end of the collection is set to null. (This is useful in determining the length of this collection
     * only if the caller knows that this collection does not contain any null elements.)
     *
     * Like the toArray() method, this method acts as bridge between array-based and collection-based APIs.
     * Further, this method allows precise control over the runtime type of the output array, and may,
     * under certain circumstances, be used to save allocation costs.
     *
     * Suppose x is a collection known to contain only strings.
     * The following code can be used to dump the collection into a newly allocated array of String:
     *
     *      String[] y = x.toArray(new String[0]);
     *
     * Note that toArray(new Object[0]) is identical in function to toArray().
     *
     * @param <T> the runtime type of the array to contain the collection
     *
     * @param a the array into which the elements of this collection are to be stored, if it is big enough; otherwise,
     *         a new array of the same runtime type is allocated for this purpose.
     *
     * @return an array containing all of the elements in this collection
     *
     * @throws ArrayStoreException - if the runtime type of the specified array is not a supertype of the runtime
     *         type of every element in this collection
     *
     * @throws NullPointerException - if the specified array is null
     *
     * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html#iterator--">Oracle Collection Documentation</a>
     */
    @Override
    public <T> T[] toArray(T[] a) {

        if(a == null){
            throw new NullPointerException();
        }

        if (this.isEmpty()) {
            return a;

        } else {


            T[] resultArray;

            //reference the right array in which the results are to be added
            if (a.length < this.size()) {
                resultArray = (T[]) new Object[this.size()];
            } else {
                resultArray = a;
            }

            //traverse list and add elements

            Iterator<Node<Type>> nodeIterator = this.nodeIterator();
            int index = 0;

            while (nodeIterator.hasNext()) {
                try {
                    resultArray[index++] = (T) nodeIterator.next().getValue();
                }catch(ArrayStoreException e){
                    throw e;
                }

            }

            return resultArray;
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean add( Type e) {

        if (e == null) {
            return false;
        }


        if (this.isEmpty()) {
            root = new Node<>(e);
        } else {

            Node<Type> newNode = new Node<>(e);

            NodeIterator<Node<Type>> nodeIterator = this.nodeIterator();

            Node<Type> lastNode = nodeIterator.last();

            lastNode.setChild(newNode);
            newNode.setParent(lastNode);

        }
        currentSize++;
        return true;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean remove(Object o) {
        if (this.isEmpty()) {
            return false;
        } else {

            Node<Type> current = root;
            Node<Type> parent;
            Node<Type> child;

            while (current != null && !current.getValue().equals(o)) {
                current = current.getChild();
            }

            if (current == null) {
                return false;
            } else {
                parent = current.getParent();
                child = current.getChild();

                if (parent != null){
                    parent.setChild(child);
                }else{
                    this.root = child; //If the node to be removed is null
                }
                if (child != null) child.setParent(parent);


                currentSize--;
                return true;
            }
        }
    }


    /**
     * @inheritDoc
     */
    @Override
    public boolean containsAll(Collection<?> c){
        if(c == null || c.stream().anyMatch(Objects::isNull)) {
            throw new NullPointerException();

        }else if (c.isEmpty()){
            throw new NullPointerException();

        }else{

            return this.stream().allMatch(c::contains);

        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean addAll(Collection<? extends Type> c) {



        if (c == null) {
            throw new IllegalArgumentException("The given collection must not be empty");
        }

        if(c.stream().anyMatch(Objects::isNull)){
            throw new NullPointerException();
        }

        for (Type elem : c) {
            //add() increases the counter here
            this.add(elem);
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean addAll(int index, Collection<? extends Type> c) {
        if(index < 0 || index > currentSize){
            throw new IllegalArgumentException();
        }

        if (c == null) {
            throw new IllegalArgumentException("The given collection must not be empty");
        }

        c.stream().map(this::add);

        return true;

    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        if (c == null) {
            throw new IllegalArgumentException("The given collection must not be empty");
        }

        if(c.stream().anyMatch(Objects::isNull)){
            throw new NullPointerException();
        }

        this.stream().filter(c::contains).map(this::remove);

        return true;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        if (c == null) {
            throw new IllegalArgumentException("The given collection must not be empty");
        }

        if(c.stream().anyMatch(Objects::isNull)){
            throw new NullPointerException();
        }

        this.stream().filter(t -> !c.contains(t)).map(this::remove);
        return true;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void clear() {
        this.root = null;
        currentSize = 0;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Type get(int index) {

        if (index < 0) {
            throw new IllegalArgumentException();
        }

        if (this.isEmpty()) {
            return null;
        } else {

            Node<Type> current = root;
            int counter = index;

            while (current != null) {
                if (counter == 0) {
                    return current.getValue();
                } else {
                    counter--;
                    current = current.getChild();

                }
            }

            return null;
        }
    }


    /**
     * @inheritDoc
     */
    public Type set(int index, Type element){

        if(element == null){
            throw new NullPointerException();
        }

        if(index < 0 || index > currentSize){
            throw new IndexOutOfBoundsException();
        }


        NodeIterator<Node<Type>> nodeIterator = this.nodeIterator();

        Node<Type> current = root;

        while(nodeIterator.getIndex() != index && nodeIterator.hasNext()){
            current = nodeIterator.next();
        }

        Type removedElement = current.getValue();


        current.setValue(element);

        return removedElement;


    }



    /**
     * @inheritDoc
     */
    //TODO check if this is solvable with NodeIterator
    @Override
    public void add(int index, Type element) {

        //check the input
        if (index < 0 || index > currentSize) {
            throw new IndexOutOfBoundsException(
                    String.format("The given index %d is not supported because this list has only %d elements",
                            index, currentSize));
        } else if (element == null) {
            throw new NullPointerException
                    ("The given element could not be added because this list does not accept elements that are null");
        } else if (index == currentSize) {
            //add at the end
            this.add(element);
        }

        int currentPos = 0;
        Node<Type> currentNode = root; //this is the place where the new node should be
        Node<Type> newNode = new Node(element);

        while (currentPos < index) {
            currentNode = currentNode.getChild();
            currentPos++;
        }

        //references
        Node<Type> parent = currentNode.getParent();

        //existing nodes
        if (parent != null) parent.setChild(newNode);
        else root = newNode;
        currentNode.setParent(newNode);

        //Set the currentNode bwow newNode inside the newNode object.
        newNode.setChild(currentNode);

        //increase size
        currentSize++;
    }


    /**
     * @inheritDoc
     */
    //Todo check if this is solvable with NodeIterator
    @Override
    public Type remove(int index) {
        //check the input
        if (index < 0 || index >= currentSize) {
            throw new IndexOutOfBoundsException(
                    String.format("Index %d out of Bounds,  List has only %d elements",
                            index, currentSize));
        }

        Node<Type> currentNode = this.getNode(index);

        Node<Type> parent = currentNode.getParent();
        Node<Type> child = currentNode.getChild();
        Type removedValue = currentNode.getValue();


        //references
        if (parent != null){
            parent.setChild(child);
        }else{ //If the removed node was the root, set its child to the new root
            this.root = child;
        }

        if (child != null) child.setParent(parent);


        //other stuff
        currentSize--;
        return removedValue;
    }

    /**
     * @inheritDoc
     */
    //TODO solve this with NodeIterator
    @Override
    public int indexOf(Object o) {

        //check the input
        if (o == null) throw new NullPointerException("The specified element is null.");

        Node currentNode = this.root;
        int index = 0;

        //does this object equal?
        while (currentNode != null) {
            if (currentNode.getValue().equals(o)) {
                return index;
            }
            index++;
            currentNode = currentNode.getChild();
        }

        return -1;
    }

    /**
     * @inheritDoc
     */
    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();


    }




    /**
     * @inheritDoc
     */
    @Override
    public ListIterator<Type> listIterator() {

        if (this.isEmpty()) return null;

        return new ListIterator<Type>() {

            /* Attributes */
            private Node<Type> currentNode = root;
            private int currentIndex = 0;
            private Node<Type> lastAccessed = null; //This is the node that was last accessed by next() or previous()

            /**
             * @inheritDoc
             */
            @Override
            public boolean hasNext() {
                return currentNode != null;
            }

            /**
             * @inheritDoc
             */
            @Override
            public Type next() {
                if (!hasNext()) throw new NoSuchElementException("There is no element in this list left");
                Type value = currentNode.getValue();
                lastAccessed = currentNode;
                currentNode = currentNode.getChild();
                currentIndex++;

                return value;
            }

            /**
             * @inheritDoc
             */
            @Override
            public boolean hasPrevious() {
                return lastAccessed != null;
            }

            /**
             * @inheritDoc
             */
            @Override
            public Type previous() {
                if (!hasPrevious()) throw new NoSuchElementException("There is no previous element.");
                currentNode = lastAccessed;
                currentIndex--;

                return currentNode.getValue();
            }

            /**
             * @inheritDoc
             */
            @Override
            public int nextIndex() {
                return currentIndex;
            }

            /**
             * @inheritDoc
             */
            @Override
            public int previousIndex() {
                return currentIndex - 1;
            }

            /**
             * @inheritDoc
             */
            @Override
            public void remove() {
                if (lastAccessed == null) throw new IllegalStateException("");
                Node<Type> parent = lastAccessed.getParent();   //x
                Node<Type> child = lastAccessed.getChild();     //y

                if (parent != null) parent.setChild(child);
                if (child != null) child.setParent(parent);

                if (lastAccessed == currentNode)
                    currentNode = child;
                else
                    currentIndex--;

                currentSize--;
                lastAccessed = null;
            }

            /**
             * @inheritDoc
             */
            @Override
            public void set(Type type) {
                Objects.requireNonNull(type, "The parameters to the set()-Method must not be null.");
                lastAccessed.setValue(type);
            }

            /**
             * @inheritDoc
             */
            @Override
            public void add(Type type) {
                throw new UnsupportedOperationException
                        ("This iterator does not support the add()-Method. Please use the LinkedLists method.");

                /*

                RIP code and time

                Node<Type> newNode = new Node(type);

                if (isEmpty()) {
                    root = newNode;
                    return;
                }

                if (lastAccessed == null)
                    throw new IllegalStateException("");

                lastAccessed.setChild(newNode);
                if ( currentNode!= null ) currentNode.setParent(newNode);

                newNode.setParent(lastAccessed);
                newNode.setChild(currentNode);

                currentNode = newNode;

                currentSize++;

                lastAccessed = null;
                */
            }
        };
    }

    /**
     * @inheritDoc
     */
    @Override
    public ListIterator<Type> listIterator(int index) {
        //check the input
        if (index < 0 || index > currentSize) {
            throw new IndexOutOfBoundsException(
                    String.format("The given index %d is not supported because this list has only %d elements",
                            index, currentSize));
        }

        ListIterator<Type> literator = this.listIterator();

        for (int i = 0; i < index; i++) {
            literator.next();
        }

        return literator;
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Type> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    /* ---------------------------------------- Other --------------------------------------------------------------- */

    /**
     * Returns the node at the specified index. If the node is not present in this list then this method will return
     * null as result
     *
     * @param index index of the specified node to be returned
     * @return {@link Node}
     * @throws IndexOutOfBoundsException when the given index < 0 or greater than the number of elements in this list
     */
    private Node<Type> getNode(int index) {

        if (index < 0 || index >= currentSize) {
            throw new IndexOutOfBoundsException(
                    String.format("The given index %d is not supported because this list has only %d elements",
                            index, currentSize));
        }

        Node<Type> current = this.root;

        int currentPos = 0;
        while (currentPos < index) {
            current = current.getChild();
            currentPos++;
        }

        return current;
    }

    /**
     * @inheritDoc
     */
    @Override
    public String toString() {
        Iterator<Type> iterator = this.iterator();
        StringBuilder res = new StringBuilder("--- Contents of this Linked List ---\n");

        while (iterator.hasNext()) {
            res.append(iterator.next()).append(",");
        }

        String result = res.toString();
        return result.substring(0, result.length() - 1);
    }


    /**
     * @inheritDoc
     */
    @Override
    public Stream<Type> stream(){

        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(this.iterator(), Spliterator.NONNULL), false);

    }

    /**
     * @inheritDoc
     */
    @Override
    public void sort(Comparator<? super Type> c) {
        Object[] a = this.toArray();
        Arrays.sort(a, (Comparator) c);
        ListIterator<Type> i = this.listIterator();
        for (Object e : a) {
            i.next();
            i.set((Type) e);
        }
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
