cmake_minimum_required(VERSION 3.10)
project(ueb23 C)

set(CMAKE_C_STANDARD 11)

set(SOURCE_FILES symbolPrinter.c)

#not sure if necessary
add_subdirectory(callByValue)
add_subdirectory(physicalApplications)
add_subdirectory(typeLimitTestings)

add_executable(ueb23 ${SOURCE_FILES})
