#include <stdio.h>
#define DEBUG 1
int main(void){
	
	//change these values in order to produce other images
	const char star[] = { "X" };
	const char line[] = { "_" };
	
	int rows, columns;
	printf("Please input the number of rows (int) and columns (int) like this: <ROWS>x<COLUMNS>\n");
	int check = scanf("%ix%i", &rows, &columns);
	
	if (check != 2) {
		printf("You have not entered two numbers in the given format. The output will be set to: 11 x 11. Thanks. \n");
		rows = columns = 11;
	}
	
	
	#if DEBUG == 1
	printf("Input: %i rows and %i columns \n", rows, columns);
	#endif
	
	for (int rowIndex = 0; rowIndex < rows; rowIndex++){
		for (int colIndex = 0; colIndex < columns; colIndex++){
			
			if ((colIndex == rowIndex) || ((colIndex + rowIndex) == columns-1)) {
				printf(star);
			} else {
				printf(line);
			}
			
			//new line at the end of each line
			if (colIndex == (columns - 1)) {
				printf("\n");
			}
		}
	}
	
}