#include <stdio.h>

void square(int x){
	x = x*x;
}

int main(void){
	
	int val = 5;
	int currentState = val;
	
	square(val);
	
	printf("The value is %d before the function call and %d after\n", currentState, val);
	
}