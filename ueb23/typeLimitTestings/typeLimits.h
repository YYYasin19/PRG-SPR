#ifndef TYPELIMITS_H
#define TYPELIMITS_H

//Char
long int * getTypeLimits_Char(long int *pointer);
long int * getTypeLimits_UnsignedChar(long int *pointer);

//Short
long int * getTypeLimits_Short(long int *pointer);
long int * getTypeLimits_UnsignedShort(long int *pointer);

//Int
long int * getTypeLimits_Int(long int *pointer);
long int * getTypeLimits_UnsignedInt(long int *pointer);


#endif