#include "typeLimits.h"
#include <stdio.h>

int main(void){
	
	printf(" --------------- Char ---------------- \n");
	
	//char
	long int charTypeLimits[2];
	getTypeLimits_Char(charTypeLimits);
	printf("The Type Limits for char are:\t\t%12li [LOWER] and %12li [UPPER]\n", 
	charTypeLimits[0], charTypeLimits[1]);
	
	long int unsignedCharTypeLimits[2];
	getTypeLimits_UnsignedChar(unsignedCharTypeLimits);
	printf("The Type Limits for unsigned char are:\t%12li [LOWER] and %12li [UPPER]\n", 
	unsignedCharTypeLimits[0], unsignedCharTypeLimits[1]);
	
	printf(" --------------- Short ---------------- \n");
	
	//short
	long int shortTypeLimits[2];
	getTypeLimits_Short(shortTypeLimits);
	printf("The Type Limits for short are:\t\t%12li [LOWER] and %12li [UPPER]\n", 
	shortTypeLimits[0], shortTypeLimits[1]);
	
	long int unsignedShortTypeLimits[2];
	getTypeLimits_UnsignedShort(unsignedShortTypeLimits);
	printf("The Type Limits for unsigned short are:\t%12li [LOWER] and %12li [UPPER]\n", 
	unsignedShortTypeLimits[0], unsignedShortTypeLimits[1]);
	
	printf(" --------------- Int ---------------- \n");
	
	//int
	long int intTypeLimits[2];
	getTypeLimits_Int(intTypeLimits);
	printf("The Type Limits for int are:\t\t%12li [LOWER] and %12li [UPPER]\n", 
	intTypeLimits[0], intTypeLimits[1]);
	
	long int unsignedIntTypeLimits[2];
	getTypeLimits_UnsignedInt(unsignedIntTypeLimits);
	printf("The Type Limits for unsigned int are:\t%12li [LOWER] and %12li [UPPER]\n", 
	unsignedIntTypeLimits[0], unsignedIntTypeLimits[1]);
	
}