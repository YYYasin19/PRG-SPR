#include <stdio.h>
#include "typeLimits.h"

long int * getTypeLimits_Char(long int *arr){
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	char oldtest = 0;
	char test = 1;
	
	do {
		oldtest = test;
		test = test + 1;
	} while (test > oldtest);
	
	arr[0] = test;
	arr[1] = oldtest;
	
	return arr;
}

long int * getTypeLimits_UnsignedChar(long int *arr){
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	unsigned char oldtest = 0;
	unsigned char test = 1;
	
	do {
		oldtest = test;
		test = test + 1;
	} while (test > oldtest);
	
	arr[0] = test;
	arr[1] = oldtest;
	
	return arr;
}

long int * getTypeLimits_Short(long int *arr){
	
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	short oldtest = 0;
	short test = 1;
	
	do {
		oldtest = test;
		test = test + 1;
	} while (test > oldtest);
	
	arr[0] = test;
	arr[1] = oldtest;
	
	return arr;
}

long int * getTypeLimits_UnsignedShort(long int *arr){
	
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	unsigned short oldtest = 0;
	unsigned short test = 1;
	
	do {
		oldtest = test;
		test = test + 1;
	} while (test > oldtest);
	
	arr[0] = test;
	arr[1] = oldtest;
	
	return arr;
}

long int * getTypeLimits_Int(long int *arr){
	
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	int oldtest = 0;
	int test = 1;
	
	do {
		oldtest = test;
		test = test + 1;
	} while (test > oldtest);
	
	arr[0] = test;
	arr[1] = oldtest;
	
	return arr;
}

long int * getTypeLimits_UnsignedInt(long int *arr){
	
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	unsigned int oldtest = 0;
	unsigned int test = 1;
	
	do {
		oldtest = test;
		test = test + 1;
	} while (test > oldtest);
	
	arr[0] = test;
	arr[1] = oldtest;
	
	return arr;
}