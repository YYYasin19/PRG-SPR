#include <stdio.h>
#include "typeLimits_2.h"

long int * getTypeLimits_Char(long int *arr){
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	char test = 0;
	test = ~test;
	char oldtest = test -1;
	
	arr[0] = test; 				//LOWER BOUND
	arr[1] = oldtest;			//UPPER BOUND
	
	return arr;
}

long int * getTypeLimits_UnsignedChar(long int *arr){
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	unsigned char oldtest = 0;
	unsigned char test = 1;
	
	do {
		oldtest = test;
		test = test << 1;
	} while (test > oldtest);
	
	arr[0] = test;
	arr[1] = oldtest;
	
	return arr;
}

long int * getTypeLimits_Short(long int *arr){
	
	//Calculate type limits and write in array at index 0 for lower and 1 for upper bound
	short oldtest = 0;
	short test = 1;
	
	do {
		oldtest = test;
		test = test << 1;
		
	} while (test > oldtest);
	
	
	arr[0] = test;
	arr[1] = (oldtest<<1)-1;
	
	return arr;
}

long int * getTypeLimits_UnsignedShort(long int *arr){
	
	unsigned short test = 1;
	//unsigned short oldtest = 0;
	
	//invert all bits
	test = ~test;
	
	//add one again
	test++;
	
	//i do not understand this
	//overflow
	arr[0] = test;
	arr[1] = test+2;
	
	printf("Min %u Max %u\n", test, test+1);
	
	return arr;
}

long int * getTypeLimits_Int(long int *arr){
	
	int upper = 1;
	int lower = 0;
	
	do {
		lower = upper;
		upper = upper << 1;
	} while ((upper +1) > lower);
	
	arr[0] = upper +1;
	arr[1] = lower << 1;
	
	return arr;
}

long int * getTypeLimits_UnsignedInt(long int *arr){
	
	unsigned int test = 1;
	unsigned int oldtest = 0;
	
	do {
		
		oldtest = test;
		test = test << 1;
		
	} while (test > oldtest);
	
	//overflow
	arr[0] = test;
	arr[1] = test -1;
	
	return arr;
}