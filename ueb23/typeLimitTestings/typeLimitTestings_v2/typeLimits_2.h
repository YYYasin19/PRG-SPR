#ifndef TYPELIMITS_H
#define TYPELIMITS_H

//Char
long int * getTypeLimits_Char();
long int * getTypeLimits_UnsignedChar();

//Short
long int * getTypeLimits_Short();
long int * getTypeLimits_UnsignedShort();

//Int
long int * getTypeLimits_Int();
long int * getTypeLimits_UnsignedInt();


#endif