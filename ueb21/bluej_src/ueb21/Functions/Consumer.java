package Functions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * An implementation of Consumer
 * in ueb21
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-14
 */
public class Consumer {

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private static HashMap<Integer, ArrayList<Long>> timeStamps;

    static {
        timeStamps = new HashMap<>();
    }

    private Consumer() {
        //construct here
    }
    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Diese Methode nimmt einen Integer entgegen und berechnet die Quersumme.
     * This crosssum is added to the underyling data structure
     *
     * @param input int
     */
    public static void consume(int input) {
        int crossSum = crossSum(input);
        long time = getTimestamp();

        //now we have to add this sum to a map, that maps this sum to all the time
        if (timeStamps.containsKey(crossSum)) {
            timeStamps.get(crossSum).add(time);
        } else {
            ArrayList<Long> list = new ArrayList<>();
            timeStamps.put(crossSum, new ArrayList<Long>() {{
                add(time);
            }});
        }
    }

    /**
     * this counts the number of timestamps that were saved for a particular cross sum computation
     *
     * @param crossSum int
     * @return int number of timestamps that are saved currently - or 0 if there is none.
     */
    public static int countTimeStamps(int crossSum) {
        if (timeStamps.containsKey(crossSum)) {
            return timeStamps.get(crossSum).size();
        }

        return 0;
    }

    /**
     * returns the number of different cross sums that were calculated up to now
     *
     * @return int
     */
    public static int numberOfDifferentResults() {
        return timeStamps.size();
    }

    /**
     * returns the number of occurences for this particular solution
     *
     * @param crossSum int
     * @return int
     */
    public static int numberOfOccurences(int crossSum) {
        return timeStamps.get(crossSum).size();
    }

    /**
     * caluculates the cross sum of a given integer
     *
     * @param number int
     * @return int
     */
    private static int crossSum(int number) {
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number = number / 10;
        }

        return sum;
    }

    /**
     * returns the current system time as long. this is queried by System.currentTimeMillis()
     *
     * @return long current system time
     */
    private static long getTimestamp() {
        return System.currentTimeMillis();
    }

    /**
     * gibt eine Collection zurück, welche die berechneten Quer- summen in aufsteigender Reihenfolge enthält
     *
     * @return Collection
     */
    public static Collection<Integer> getCrossTotalsAscending() {
        Set<Integer> keySet = timeStamps.keySet();
        ArrayList<Integer> keyList = new ArrayList<Integer>() {{
            addAll(keySet);
        }};
        keyList.sort(Integer::compareTo);
        return keyList;
    }

    /**
     * gibt eine Collection zurück, welche die berechneten Quer- summen in absteigender Reihenfolge enthält.
     *
     * @return Collection
     */
    public static Collection<Integer> getCrossTotalsDescending() {
        Set<Integer> keySet = timeStamps.keySet();
        ArrayList<Integer> keyList = new ArrayList<Integer>() {{
            addAll(keySet);
        }};
        keyList.sort((o1, o2) -> {
            if (o1 > o2) {
                return -1;
            } else if (o1.equals(o2)) {
                return 0;
            } else {
                return 1;
            }
        });
        return keyList;
    }

    /**
     * nimmt einen Integer entgegen und gibt eine Collection zurück, welche alle zugehörigen Zeitstempel enthält.
     * D.h. die Zeitstempel der Berechnungen, die zu dem gegebenen Ergebnis geführt haben.
     *
     * @param crossSum int
     * @return Collection
     */
    private static Collection<Long> getTimestampsForResult(int crossSum) {
        return timeStamps.get(crossSum);
    }

    @Override
    public String toString() {
        return "Consumer with following cross sums already calculated" + getCrossTotalsAscending();
    }
    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}