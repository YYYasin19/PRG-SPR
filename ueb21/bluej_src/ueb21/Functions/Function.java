package Functions;

import java.util.*;

/**
 * An implementation of Function
 * in ueb21
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-15
 */
public class Function {

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private static Collection<Integer> integers;
    private static QUEUE_SORTING sorting = QUEUE_SORTING.DEFAULT;

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    private Function() {
    } //closed constructor

    public static void main(String[] args) {

        //get the input
        for (String argument : args) {
            if (argument.contains("-natural")) {
                sorting = QUEUE_SORTING.NATURAL;
            } else if (argument.contains("-fifo")) {
                sorting = QUEUE_SORTING.FIFO;
            }
        }

        //initialize the underlying data structure - default is a LinkedList
        switch (sorting) {

            case FIFO:
                integers = new LinkedList<>();
                break;
            case NATURAL:
                //integers = new ArrayList<>();
                integers = new PriorityQueue<>();
                break;
            default:
                integers = new LinkedList<>();
                break;
        }

        Random ran = new Random();

        for (int i = 0; i < 50000; i++) {

            try {

                if (ran.nextInt(2) > 0) {
                    addToCollection(Producer.produce());
                } else {
                    Consumer.consume(removeFromCollection());
                }

            } catch (NoSuchElementException nse) {
                //System.err.println(nse);
            }
        }

    }

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    private static void addToCollection(int value) {
        integers.add(value);
    }

    /**
     * retrieves and removes the first element of this collection
     *
     * @return
     */
    private static int removeFromCollection() {

        switch (sorting) {

            case NATURAL:
                //sort collection
                return ((PriorityQueue<Integer>) integers).remove();

            case FIFO:
                return ((LinkedList<Integer>) integers).removeFirst();

            default:
                return ((LinkedList<Integer>) integers).removeFirst();


        }
    }

    @Override
    public String toString() {
        switch (sorting) {
            case FIFO:
                return "Function with the underlying data structure being FIFI sorted";
            default:
                return "Function with the underlying data structure being naturally sorted";
        }
    }
    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    enum QUEUE_SORTING {
        FIFO,
        NATURAL,
        DEFAULT
    }
}