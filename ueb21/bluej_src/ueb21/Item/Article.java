package Item;

import java.util.InputMismatchException;

import static Tools.IOTools.stringEmpty;

/**
 * An implementation of Article
 * in ueb21
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-14
 */
public class Article implements Item {

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private int articleNumber;
    private String name;

    /* ---------------------------------------- Main ---------------------------------------------------------------- */
    private int amount;
    private double price;
    /**
     * constructs an article
     *
     * @param articleNumber
     * @param name
     * @param amount
     * @param price
     */
    public Article(int articleNumber, String name, int amount, double price) {
        if (articleNumber < 0) throw new InputMismatchException();
        if (price < 0.0) throw new InputMismatchException();

        this.articleNumber = articleNumber;
        this.setName(name);
        this.setAmount(amount);
        this.price = price;
    }
    /**
     * constructs an article
     *
     * @param articleNumber
     * @param name
     */
    public Article(int articleNumber, String name) {
        this(articleNumber, name, 0, 0.0);
    }

    /* ---------------------------------------- Constants ----------------------------------------------------------- */


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    @Override
    public int compareTo(Object o) {
        if (o instanceof Article) {
            //compare the two articles by their identifier
            return Integer.compare(this.getIdentifier(), ((Article) o).getIdentifier());
        }

        return 0;
    }

    @Override
    public String toString() {
        return String.format("#%-4d | %-20s | %-10d | %.2f", this.articleNumber, this.name, this.amount, this.price);
    }
    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public void setAmount(int amount) {
        if (amount < 0) throw new InputMismatchException();
        this.amount = amount;
    }

    @Override
    public int getIdentifier() {
        return this.articleNumber;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (stringEmpty(name)) throw new InputMismatchException();
        this.name = name;
    }
}