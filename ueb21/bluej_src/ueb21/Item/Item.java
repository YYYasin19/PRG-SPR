package Item;

/**
 * An Interface called Item
 * in ueb21
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-14
 */
public interface Item extends Comparable {

    /**
     * this method returns the identifier that identifies this unique item
     *
     * @return int identifier
     */
    int getIdentifier();

}
