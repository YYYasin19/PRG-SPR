package Storage;

import Item.Article;
import org.junit.Before;
import org.junit.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class StorageTest {

    private Storage<Article> articleStorage;


    /* ---------------------------------------- Before -------------------------------------------------------------- */
    @Before
    public void setUp() {
        articleStorage = new StorageImpl<Article>();
    }

    /* ---------------------------------------- Storage - Test-Methods ---------------------------------------------- */

    @Test
    public void testContainsObject() {
        Article article = new Article(4711, "Test");
        articleStorage.add(article);
        assertTrue(articleStorage.contains(article));
    }

    @Test
    public void testContains() {
        final int ARTICLE_NUMBER = 4711;
        articleStorage.add(new Article(ARTICLE_NUMBER, "Test-Name 4711"));
        assertTrue("The Article could not be found by contains()", articleStorage.contains(ARTICLE_NUMBER));
    }

    @Test
    public void testGet() {
        final int ARTICLE_NUMBER = 4711;
        Article article = new Article(ARTICLE_NUMBER, "Test Article 4711");
        articleStorage.add(article);
        assertEquals(article, articleStorage.get(ARTICLE_NUMBER));
    }

    @Test
    public void testGetItems() {
        int difference = 1005;
        articleStorage.addAll(articleGenerator(10));
        List<Article> articles = articleStorage.getItems(article -> article.getIdentifier() < difference);
        assertEquals(difference - 1000, articles.size());

        for (Article article : articles) {
            assertTrue(article.getIdentifier() < difference);
        }
    }

    @Test (expected = NotImplementedException.class)
    public void testGetSorted() {
        throw new NotImplementedException();
    }

    @Test (expected = NotImplementedException.class)
    public void testApplyToAll() {
        throw new NotImplementedException();
    }

    @Test (expected = NotImplementedException.class)
    public void testApplyToSome() {
        throw new NotImplementedException();
    }

    @Test
    public void testAdd() {
        Article article = new Article(1000, "Test Article 1000");
        articleStorage.add(article);
        assertEquals("The Article was not added correctly somehow.",
                article, articleStorage.get(article.getIdentifier()));
    }

    @Test
    public void testAddAll() {
        ArrayList<Article> articles = articleGenerator(10);
        articleStorage.addAll(articles);
        for (int i = 0; i < 10; i++) {
            assertTrue("The article at " + i + " was not added or could not be properly found by contains()."
                    , articleStorage.contains(articles.get(0)));
        }
    }

    @Test (expected = NotImplementedException.class)
    public void testCurrentWorkload() {
        throw new NotImplementedException();
    }

    /* ---------------------------------------- Other Methods ------------------------------------------------------- */

    /**
     * @param number
     * @return
     */
    public ArrayList<Article> articleGenerator(int number) {
        ArrayList<Article> articles = new ArrayList<>();
        int startNumber = 1000;
        String mainName = "Test-Article ";
        //generate
        for (int index = 0; index < number; index++) {
            Article tmpArticle = new Article(startNumber + index, mainName + index);
            articles.add(tmpArticle);
        }

        return articles;
    }

}