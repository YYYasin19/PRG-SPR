package Tools;

import java.util.Scanner;

/**
 * An implementation of IOTools
 * in ueb16
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-07
 */
public class IOTools {

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * prints the given type to the standart output
     *
     * @param t output
     */
    public static <T> void stdout(T t) {
        System.out.print(t);
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * prints the given type to the standart output with a new line
     *
     * @param t output
     */


    public static <T> void stdoutln(T t) {
        System.out.println(t.toString());
    }


    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * reads user input from the standard console
     *
     * @return int
     */
    public static int stdinInt() {
        int result = -1;
        Scanner input = new Scanner(System.in);

        try {
            while (!input.hasNextInt()) {
                input.next();
            }
            return input.nextInt();
        } catch (Exception e) {
            System.err.println(e);
        }

        return result;
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * prints a message before reading user input from the standard console
     *
     * @param msg String message
     * @return int
     */
    public static int stdinInt(String msg) {
        stdoutln(msg);
        return stdinInt();
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * outputs a dividor to the console
     */
    public static void stdoutDividor() {
        System.out.println
                ("--------------------------------------------------" +
                        "--------------------------------------------------");
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * checks whether a given String is empty or not
     *
     * @param s
     * @return
     */
    public static boolean stringEmpty(String s) {
        try {
            if (s == null || s.isEmpty() || s.trim().length() == 0) {
                return true;
            }

        } catch (Throwable t) {
            return true;
        }

        return false;
    }


    /**
     * Parses string to integer when possible
     *
     * @param s
     * @return Integer when successful, 0 on failure.
     */
    public static int parseInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

}
