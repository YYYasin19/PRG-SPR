package Functions;

import java.util.Random;

/**
 * An implementation of Producer
 * in ueb21
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-15
 */
public class Producer {

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private static Random random;

    static {
        random = new Random();
    }

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    private Producer() {
        //construct here
    }

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public static int produce() {
        return random.nextInt(1001);
    }

    @Override
    public String toString() {
        return "Producer with random " + Integer.toString(random.hashCode());
    }
    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}