package Storage;

import Item.Item;

import java.util.Collection;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * An Interface called Storage
 * in ueb21
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-14
 */
public interface Storage<Type extends Item> extends Collection<Type> {

    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    static final String DEFAULT_STORAGE_NAME = "Default Storage Name";
    /**
     * this attribute determines how much this storage will be filled in the average case
     */
    static final int DEFAULT_LOAD_PERCENT = 75;
    static final int DEFAULT_INITIAL_CAPACITY = 20;

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * checks whether the given element is contained by this storage
     *
     * @param element Type
     * @return boolean
     */
    boolean contains(Type element);

    /**
     * checks whether the given element is contained by this storage by comparing the identifiers
     *
     * @param identifier int
     * @return boolean
     */
    boolean contains(int identifier);

    /**
     * returns the element with the given identifier
     *
     * @param identifier int
     * @return Type element
     */
    Type get(int identifier);

    /**
     * returns a list with all the items that match the given predicate
     *
     * @param filter {@link Predicate}
     * @return {@link List}
     */
    List<Type> getItems(Predicate<Type> filter);

    /**
     * applies the given function to every element
     *
     * @param function {@link Consumer}
     */
    void applyToAll(Consumer<Type> function);

    /**
     * applies the given function to all elements that match the given filter
     *
     * @param filter   {@link Predicate}
     * @param consumer {@link Consumer}
     */
    void applyToSome(Predicate<Type> filter, Consumer<Type> consumer);

    /**
     * returns a sorted Array containing the items of this storage. the elements are sorted by their given comparator.
     *
     * @return List
     */
    List<Type> getSorted();

    /**
     * returns a sorted Array containing the items of this storage.
     *
     * @param greaterThan {@link BiPredicate} this predicate determines, when an Item object is greater than an other
     * @return List
     */
    List<Type> getSorted(BiPredicate<Type, Type> greaterThan);

    /**
     * returns an double that represents the percentage that this storage is filled
     *
     * @return double
     */
    double currentWorkload();

}
