package Storage;

import Item.Item;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static Tools.IOTools.stringEmpty;
import static Tools.MessageTools.getMsg;

/**
 * An implementation of Storage
 * in ueb21
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-14
 */
public class StorageImpl<Type extends Item> implements Storage<Type> {

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private LinkedHashMap<Integer, Type> storage;
    private String storageName;

    /**
     * This constructor creates a new storage object
     *
     * @param initialCapacity int (> 0)the capacity that this storage can store initially. can be described as "how many elements
     *                        are planned to be stored?"
     * @param loadFactor      int (> 0) how much will the storage be filled in average in per cent
     * @param storageName     String (non-empty) the name of the storage
     */
    public StorageImpl(int initialCapacity, int loadFactor, String storageName) {
        //check the input
        if (initialCapacity < 0) throw new InputMismatchException(getMsg("INITIAL_CAPACITY_TOO_LOW"));
        if (loadFactor < 0) throw new InputMismatchException(getMsg("LOAD_FACTOR_TOO_LOW"));
        if (stringEmpty(storageName)) throw new InputMismatchException(getMsg("STORAGE_NAME_EMPTY"));

        //initialize storage
        float lf = (float) (loadFactor * 1.1);
        this.storage = new LinkedHashMap<>(initialCapacity, lf / 100);

        //and the attributes
        this.storageName = storageName;
    }

    /**
     * This constructor creates a new storage object
     *
     * @param loadFactor int (> 0) how much will the storage be filled in average in per cent
     */
    public StorageImpl(int loadFactor) {
        this(DEFAULT_INITIAL_CAPACITY, loadFactor, DEFAULT_STORAGE_NAME);
    }
    /**
     * This constructor creates a new storage object
     */
    public StorageImpl() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_PERCENT, DEFAULT_STORAGE_NAME);
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    @Override
    public boolean contains(Type element) {
        return this.storage.containsValue(element);
    }

    @Override
    public boolean contains(int identifier) {
        for (Type element : storage.values()) {
            if (element.getIdentifier() == identifier) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Type get(int identifier) {
        for (Type element : storage.values()) {
            if (element.getIdentifier() == identifier) {
                return element;
            }
        }

        return null;
    }

    @Override
    public List<Type> getItems(Predicate<Type> filter) {
        return this.storage
                .values()
                .stream()
                .filter(filter)
                .collect(Collectors.toList());
    }

    @Override
    public List<Type> getSorted() {
        return storage
                .values()
                .stream()
                .sorted((Type o1, Type o2) -> o1.compareTo(o2))
                .collect(Collectors.toList());
    }

    @Override
    public List<Type> getSorted(BiPredicate<Type, Type> greaterThan) {
        return
                storage.values().stream().sorted((o1, o2) -> {
                    if (greaterThan.test(o1, o2))
                        return 1;
                    else
                        return -1;
                }).collect(Collectors.toList());
    }

    @Override
    public void applyToAll(Consumer<Type> function) {
        this.storage
                .values()
                .forEach(function);
    }

    @Override
    public void applyToSome(Predicate<Type> filter, Consumer<Type> consumer) {
        this.storage
                .values()
                .stream()
                .filter(filter)
                .forEach(consumer);
    }

    @Override
    public double currentWorkload() {
        throw new UnsupportedOperationException();
    }

    /* ---------------------------------------- Collection-Methods -------------------------------------------------- */

    @Override
    public boolean add(Type element) {
        if (element == null) throw new InputMismatchException(getMsg("ELEMENT_MUST_NOT_NULL"));
        storage.put(element.getIdentifier(), element);

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Type> c) {
        for (Object o : c) {
            this.add((Type) o);
        }

        return true;
    }

    @Override
    public int size() {
        return this.storage.size();
    }

    @Override
    public boolean isEmpty() {
        return this.storage.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return this.storage.containsValue(o);
    }

    @Override
    public Iterator<Type> iterator() {
        return this.storage.values().iterator();
    }

    @Override
    public Object[] toArray() {
        return this.storage.values().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return this.storage.values().toArray(a);
    }

    @Override
    public boolean remove(Object o) {
        this.storage.remove(o);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return this.storage.values().containsAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return this.storage.values().removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return this.storage.values().retainAll(c);
    }

    @Override
    public void clear() {
        this.storage.clear();
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}