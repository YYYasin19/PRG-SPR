package PersonQueue;

import java.util.Iterator;
import java.util.LinkedList;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of PersonQueue.PersonQueue
 * in ueb16
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-26
 */
public class PersonQueue implements Queue, Iterable<Person> {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private LinkedList<Person> queue;
    private int currentSize;
    private int maxSize;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * default contructor of PersonQueue.PersonQueue
     * @param size size of the queue | must be greater than 0
     * @throws PersonException if size <= 0
     */
    public PersonQueue(int size) throws PersonException {

        this.setMaxSize(size);
        //create queue:
        this.currentSize = 0;
        this.queue = new LinkedList<Person>();
        this.currentSize = queue.size();

    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    @Override
    public void addLast(Object o) {

        try {

            if (queue.size() == maxSize) { //is the queue full?
                throw new PersonException(getMsg("QUEUE_FULL"));
            }

            if ( o instanceof Person) { //is o a PersonQueue.Person?
                queue.addLast((Person) o);
            } else {
                throw new PersonException(getMsg("OBJECT_NOT_PERSON" + o.toString()));
            }

        } catch (PersonException pe) {
            System.out.println(pe);
        }

    }

    @Override
    public Object removeFirst() {
        return queue.removeFirst();
    }

    @Override
    public Object get(int i) {
        return queue.get(i);
    }

    @Override
    public boolean empty() {
        return queue.isEmpty();
    }

    @Override
    public boolean full() {
        return queue.size() == maxSize;
    }

    @Override
    public int size() {
        return queue.size();
    }

    /**
     * finds the lexicographically smallest person in the queue
     * @return PersonQueue.Person object reference
     */
    public Person smallest(){

        //Hi ich habe deine Methode umgeschrieben ich hoffe es stört dich nicht
        //mfg Yasinovic

        if (this.empty()) {
            //TODO: Throw Exception (?)
        }

        //Start the search:

        Person p = (Person) this.get(0); //returns the first person

        for (Person currentPerson : this) { //goes through the queue

            //compare two persons lexicographically
            if ((p.getName().compareTo(currentPerson.getName())) < 0) {
                p = currentPerson;
            }
        }

        return p;
    }

    public void print(){
        StringBuilder result = new StringBuilder("PersonQueue.Person - PersonQueue.Queue: -------------------- \n");
        int counter = 1;
        //iterate through the queue
        for (Person p : this){
            result.append(String.format("%d: %s\n",counter,p.toString()));
        }

        //print result:
        System.out.print(result.toString());
    }

    @Override
    public Iterator<Person> iterator() {

        //Anonymous class:
        PersonIterator it = new PersonIterator() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return (currentIndex < currentSize) && (queue.get(currentIndex) != null);
            }

            @Override
            public Person next() {
                return queue.get(currentIndex++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();

            }
        };

        return it;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    private void setMaxSize(int size) throws PersonException {

        if (size <= 0) {
            throw new PersonException(getMsg("QUEUE_SIZE_TOO_LOW"));
        }

        this.maxSize = size;

    }

    /* ---------------------------------------- Inner Classes ------------------------------------------------------- */

    //Inner class:
    interface PersonIterator extends Iterator {
        //Just a marker interface like Serializable
    }

}

