package PersonQueue;

import java.util.Iterator;

/**
 * An Interface called PersonQueue.Queue
 * in ueb16
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-26
 */
public interface Queue {

    /**
     * adds an Object to the end of the queue
     * the Object is only added when the queue is not full
     * @param o Object to be added
     */
    void addLast(Object o); // Objekt hinten anfuegen

    /**
     * removes the first Object in PersonQueue.Queue
     * @return removed Object
     */
    Object removeFirst(); // entferne das erste Element und gebe eine // Referenz darauf zurueck

    /**
     * returns the i-th object in the queue
     * @param i index of object
     * @return Object at the index i
     */
    Object get(int i); // Das i-te Element zurueckgeben

    /**
     * checks if the queue is empty
     * @return true if and if only queue is empty
     */
    boolean empty(); // Testen, ob schon Elemente eingefuegt wurden

    /**
     * checks if the queue is full
     * @return true if the queue is full
     */
    boolean full(); // Testen, ob noch Elemente einfuegbar sind,d.h. ob das letzte Element schon einen Wert != null hat

    /**
     * returns the number of elements in the queue
     * @return number of elements
     */
    int size(); // Anzahl eingefuegter Elemente


}
