package PersonQueue;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of PersonQueue.Person
 * in ueb16
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-26
 */
public class Person {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private String name;
    private int age;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * default constructor for Person
     * @param name name of the person
     * @param age age of the person >= 0
     */
    public Person(String name, int age) {

        try {
            this.setName(name);
            this.setAge(age);
        } catch (PersonException e) {
            System.err.println(e);
        }

    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    @Override
    public String toString() {
        return String.format("Name: %-40s | Age: %d",this.getName(),this.getAge());
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * sets the age of this Person object
     * @param age int >= 0
     * @throws PersonException when age < 0
     */
    private void setAge(int age) throws PersonException {
        if (age < 0) {
            throw new PersonException(getMsg("AGE_TOO_LOW"));
        }

        this.age = age;
    }

    /**
     * sets the name of this Person object
     * @param name non-empty String
     * @throws PersonException when name is empty
     */
    private void setName(String name) throws PersonException {
        if (name == null || name.trim().length() == 0) {
            throw new PersonException("EMPTY_NAME");
        }

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

}

