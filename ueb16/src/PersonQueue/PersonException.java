package PersonQueue;

/**
 * An implementation of PersonQueue.PersonException
 * in PRG-SPR
 *
 * @author Julian
 * @version 1.0
 * @since 2018-Mai-06
 */
public class PersonException extends RuntimeException {

    public PersonException(String msg){
        super(msg);
    }

}




