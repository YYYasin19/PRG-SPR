package NumberCruncher;

import Tools.CoreTools;
import Tools.NumberTools;
import Tools.ToolLocationMarker;

import java.util.Arrays;
import java.util.Scanner;

import static Tools.IOTools.*;
import static Tools.MessageTools.getMsg;

/**
 * An implementation of NumberCruncherDialog
 * in ueb16
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-07
 */
public class NumberCruncherDialog {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    public static void main(String[] args) {

        try {
            int arraySize = -1;

            stdoutln(WELCOME_MSG);

            while(arraySize < 1){
                arraySize = stdinInt("Please input the size of the NumberCruncher-Array");

                if(arraySize < 1){
                    stdoutln("Invalid input: " + arraySize);
                }
            }


            NumberCruncherDialog cruncherDialog =
                    new NumberCruncherDialog(arraySize);
            cruncherDialog.start();


        } catch (NumberCruncherException e){
            System.err.println(e);
        }

    }

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private NumberCruncher numberCruncher;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final boolean VERBOSE = true; //Executes in verbose mode

    private static final int START = -1;
    private static final int ENDE = 0;

    private static final int SUM = 1;
    private static final int SWIRL = 2;
    private static final int DIVIDE = 3;
    private static final int SUBSTRACT = 4;
    private static final int AVERAGE = 5;

    private static final int ANLEGEN = 9;
    private static final int ARRAY_AUSGABE = 10;

    private static final String FUNCTION_LIST =
            ENDE            + "\t\t for ending the application.\n" +
            SUM             + "\t\t for using the SUM function.\n" +
            SWIRL           + "\t\t for using the SWIRL function.\n" +
            DIVIDE          + "\t\t for using the DIVIDE function.\n" +
            SUBSTRACT       + "\t\t for using the SUBTRACT function.\n" +
            AVERAGE         + "\t\t for using the AVERAGE function.\n" +
            ANLEGEN         + "\t\t for creating a new array.\n" +
            ARRAY_AUSGABE   + "\t\t to view the array.\n"
    ;

    //TODO: Make the following green
    private static final String WELCOME_MSG =
           "Welcome " +  System.getProperty("user.name") + ". Please follow the given instructions.";

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    private NumberCruncherDialog(int arrayLength) throws NumberCruncherException {
        if (arrayLength <= 0)
            throw new NumberCruncherException(getMsg("ARRAY_SIZE_TOO_LOW") + "Your input:" + arrayLength);
        numberCruncher = new NumberCruncher(arrayLength);
    }


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * starts the dialog
     */
    private void start(){
        int function = START;

        if (VERBOSE) {
            stdoutDividor();
            stdoutln("\033[93m" + "Started Verbose mode." + "\033[0m");
            if(ToolLocationMarker.getMessagePropertiesLocation("de") != null){
                stdoutln("\033[92m" + "message_de.properties found at: "
                        + ToolLocationMarker.getMessagePropertiesLocation("de")
                        + "\033[0m");
            }
            stdoutDividor();
            stdout("\033[92m Current Array Status: \033[0m");
            executeFunction(ARRAY_AUSGABE);
            stdoutDividor();
        }

        try (Scanner input = new Scanner(System.in)) {
            //main loop of the dialog
            while (function != ENDE) {


                //if there is no NumberCruncher object yet
                if (numberCruncher == null) {
                    stdoutln(getMsg("NO_NUMBER_CRUNCHER_FOUND"));
                    numberCruncher =
                            new NumberCruncher(stdinInt("Please input the size of the NumberCruncher-Array"));
                }

                //ask for next input:
                stdout(FUNCTION_LIST);
                stdoutDividor();
                function = stdinInt(getMsg("CHOOSE_OPTION_PLEASE"));
                float[] currentFloats = Arrays.copyOf(numberCruncher.getFloats(), numberCruncher.getFloats().length);
                //execute function
                executeFunction(function);
                stdoutDividor();

                if (VERBOSE) {
                    stdout(String.format("\033[92m%-25s \033[0m","Current Array Status"));
                    executeFunction(ARRAY_AUSGABE);
                    stdoutln(String.format("\033[92m%-25s\033[0m %s","Changes",
                            changeCheck(currentFloats, numberCruncher.getFloats())));
                    stdoutDividor();
                }

            }

        } catch (Exception e) {
            System.err.println(e);
        }

    }


    /**
     * executes a given function
     * @param function
     */
    private void executeFunction(final int function) {

        switch (function) {

            case SUM:
                numberCruncher.crunch("sum");
                break;

            case SWIRL:
                numberCruncher.crunch("swirl");
                break;

            case DIVIDE:
                numberCruncher.crunch("divide");
                break;

            case SUBSTRACT:
                numberCruncher.crunch("substract");
                break;

            case AVERAGE:
                numberCruncher.crunch("average");
                break;

            case ARRAY_AUSGABE:
                stdoutln(Arrays.toString(numberCruncher.getFloats()));
                break;

            case ANLEGEN:
                int arraySize = stdinInt("How big should the new Array be?");
                while(arraySize < 1){
                    stdoutln("Invalid input: " + arraySize);
                    arraySize = stdinInt("How big should the new Array be?");
                }
                numberCruncher = new NumberCruncher(stdinInt("How big should the array be?"));
                break;

            default:
                stdoutln(getMsg("ENTER_VALID_OPTION"));

        }

    }


    private static String changeCheck(float[] oldArray, float[] newArray){
        if(oldArray.length != newArray.length){
            throw new IllegalArgumentException();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("[");

        for(int i = 0; i < oldArray.length; i++){
            if(!NumberTools.floatEquals(oldArray[i] , newArray[i], CoreTools.PUBLIC_EPSILON)){
                sb.append(oldArray[i] + " -> " + newArray[i]);
            }else {
                sb.append(oldArray[i]);
            }

            if(i != oldArray.length - 1){
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
