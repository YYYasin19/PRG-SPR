package NumberCruncher;

import Tools.CoreTools;


import java.util.*;

import static Tools.NumberTools.floatEquals;
import static Tools.NumberTools.getRandomFloat;

/**
 * An implementation of NumberCruncher.NumberCruncher
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-Apr-29
 */
public class NumberCruncher {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    private float[] floats;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * default constructor of NumberCruncher
     * @param size int
     */
    public NumberCruncher(int size){
        if(size < 0){
            throw new IllegalArgumentException();
        }

        floats = new float[size];
        Random r = new Random();

        for(int i = 0; i < size; i++){
            floats[i] = getRandomFloat(1,10);
        }
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * crunch method takes an array of operations that are then executed on {@link NumberCruncher#floats}
     * if none of the possibilities is given, the method does not operate.
     * @param operations String[] - possible: sum, swirl, divide, subtract, average
     */
    public void crunch(String... operations){

        for(String s : operations){

            switch (s.toLowerCase()){
                case "sum" : sum(); break;

                case "swirl" : swirl(); break;

                case "divide" : divide(); break;

                case "subtract" : subtract(); break;

                case "average" : average(); break;

                default:
                    System.err.println(s + "not supported as crunch-Method.\n");
                    break;

            }
        }
    }

    /**
     * executes the sum-function on {@link NumberCruncher#floats}
     */
    private void sum(){
        for(int i = 0; i < floats.length - 1; i++){
            floats[i+1] += floats[i];
        }
    }

    /**
     * executes the swirl-function on {@link NumberCruncher#floats}
     */
    private void swirl(){

        Random r = new Random();

        int firstIndex;
        int secondIndex;

        for(int i = 0; i < floats.length; i++){

            try {
                firstIndex = r.nextInt(floats.length);
                secondIndex = r.nextInt(floats.length);

                float current = floats[firstIndex];

                floats[firstIndex] = floats[secondIndex];
                floats[secondIndex] = current;
            }catch (IndexOutOfBoundsException e){
                System.err.println(e.getStackTrace());
            }

        }
    }

    /**
     * executes the divide-function on {@link NumberCruncher#floats}
     */
    private void divide(){
        //This should work but as they say: Trust Nobody, not even yourshelf.
        //Reference: http://i0.kym-cdn.com/photos/images/original/000/886/611/cb5.jpg

        float[] sortedArray = Arrays.copyOf(floats, floats.length);
        Arrays.sort(sortedArray);
        int sortedLength  = sortedArray.length;

        for (int i = sortedLength - 1; i > sortedLength/2; i--){


            //Find index of the corresponding number in the original Array.
            for(int nIndex = 0; nIndex < floats.length; nIndex++){
                if (floatEquals(floats[nIndex], sortedArray[i], CoreTools.PUBLIC_EPSILON)) {
                    floats[nIndex] = sortedArray[i] / sortedArray[sortedLength - i - 1];
                }
            }


        }
    }

    /**
     * executes the subtract-function on {@link NumberCruncher#floats}
     */
    private void subtract(){
        for(int i = 0; i < floats.length - 1; i++){
            floats[i+1] -= floats[i];
        }
    }

    /**
     * executes the average-function on {@link NumberCruncher#floats}
     */
    private void average(){
        float[] sortedArray = Arrays.copyOf(floats, floats.length);
        Arrays.sort(sortedArray);

        float largestNumber = sortedArray[sortedArray.length - 1];

        int largestNumberIndex = 0;

        float average = 0F;

        for(int i = 0; i < floats.length; i++){
            if(floatEquals(floats[i], largestNumber, CoreTools.PUBLIC_EPSILON)){
                largestNumberIndex = i;
            }

            average += floats[i];
        }

        average /= floats.length;
        floats[largestNumberIndex] = average;
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * returns {@link NumberCruncher#floats}
     * @return float[]
     */
    public float[] getFloats() {
        return floats;
    }

}

/**
 * An exception class for the NumberCruncher Package
 */
class NumberCruncherException extends Exception {

    /**
     * default constructor for NumberCruncherException
     * @param msg String exception message
     */
    public NumberCruncherException(String msg){
        super(msg);
    }

}
