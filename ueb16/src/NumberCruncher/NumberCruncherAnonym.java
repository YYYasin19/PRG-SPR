package NumberCruncher;

import Tools.CoreTools;
import Tools.NumberTools;

import java.util.*;

/**
 * An implementation of NumberCruncher.NumberCruncherAnonym
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-03
 */
public class NumberCruncherAnonym {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    float[] floats;
    List<Operator> operatorList;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public NumberCruncherAnonym(int size){
        if(size < 0){
            throw new IllegalArgumentException();
        }

        floats = new float[size];
        Random r = new Random();

        for(int i = 0; i < size; i++){
            floats[i] = r.nextFloat();
        }

        this.operatorList = new ArrayList<>(
                Arrays.asList(
                        sum(),
                        swirl(),
                        divide(),
                        average(),
                        subtract()));
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public void crunch(String... operations){

        for(String s : operations){
            for(Operator operator : this.operatorList){
                if(operator.getName().equals(s.toLowerCase())){
                    operator.function();
                }
            }
        }

    }

    private Operator sum(){
        return new Operator() {
            @Override
            public String getName() {
                return "sum";
            }

            @Override
            public void function() {
                for(int i = 0; i < floats.length - 1; i++){
                    floats[i+1] += floats[i];
                }
            }
        };

    }

    private Operator swirl(){
        return new Operator() {
            @Override
            public String getName() {
                return "swirl";
            }

            @Override
            public void function() {
                Random r = new Random();
                int firstIndex;
                int secondIndex;

                for(int i = 0; i < floats.length; i++){


                    firstIndex = r.nextInt(floats.length - 1);
                    secondIndex = r.nextInt(floats.length - 1);

                    floats[firstIndex] = floats[secondIndex];
                }
            }
        };
    }

    //TODO Fix this Hot Trash.
    private Operator divide(){
        return new Operator() {
            @Override
            public String getName() {
                return "divide";
            }

            @Override
            public void function() {
                //This should work but as they say: Trust Nobody, not even yourshelf.
                //Reference: http://i0.kym-cdn.com/photos/images/original/000/886/611/cb5.jpg

                float[] sortedArray = Arrays.copyOf(floats, floats.length);
                Arrays.sort(sortedArray);
                int sortedLength  = sortedArray.length;

                for (int i = sortedLength - 1; i > sortedLength/2; i--){


                    //Find index of the corresponding number in the original Array.
                    for(int nIndex = 0; nIndex < floats.length; nIndex++){
                        if(NumberTools.floatEquals(floats[nIndex], sortedArray[i], Tools.CoreTools.PUBLIC_EPSILON)){
                            floats[nIndex] = sortedArray[i] / sortedArray[sortedLength - i - 1];
                        }
                    }


                }
            }
        };

    }

    private Operator subtract(){
        return new Operator() {
            @Override
            public String getName() {
                return "subtract";
            }

            @Override
            public void function() {
                for(int i = 0; i < floats.length - 1; i++){
                    floats[i+1] -= floats[i];
                }
            }
        };
    }

    private Operator average(){
        return new Operator() {
            @Override
            public String getName() {
                return "average";
            }

            @Override
            public void function() {
                float[] sortedArray = Arrays.copyOf(floats, floats.length);
                Arrays.sort(sortedArray);

                float largestNumber = sortedArray[sortedArray.length - 1];

                int largestNumberIndex = 0;

                float average = 0F;

                for(int i = 0; i < floats.length; i++){
                    if(NumberTools.floatEquals(floats[i], largestNumber, CoreTools.PUBLIC_EPSILON)){
                        largestNumberIndex = i;
                    }

                    average += floats[i];
                }

                average /= floats.length;
                floats[largestNumberIndex] = average;
            }
        };
    }



    public interface Operator{
        String getName();

        /**
         * executes the the implemented function on the underlying array of floats
         */
        void function();
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
