package NumberCruncher.NumberCruncherTopLevel;

import NumberCruncher.NumberCruncherAnonym;

/**
 * An implementation of subtract
 * in PRG-SPR
 *
 * @author Julian
 * @version 1.0
 * @since 2018-Mai-06
 */
public abstract class Subtract implements NumberCruncherAnonym.Operator {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public static void function(float[] floats) {

        for (int i = 0; i < floats.length - 1; i++) {
            floats[i + 1] -= floats[i];
        }

    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

