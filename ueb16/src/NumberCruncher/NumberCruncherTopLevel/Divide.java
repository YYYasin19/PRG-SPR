package NumberCruncher.NumberCruncherTopLevel;

import NumberCruncher.NumberCruncherAnonym;
import Tools.CoreTools;
import Tools.NumberTools;

import java.util.Arrays;

/**
 * An implementation of divide
 * in PRG-SPR
 *
 * @author Julian
 * @version 1.0
 * @since 2018-Mai-06
 */
public abstract class Divide implements NumberCruncherAnonym.Operator {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public static void function(float[] floats) {


        float[] sortedArray = Arrays.copyOf(floats, floats.length);
        Arrays.sort(sortedArray);
        int sortedLength = sortedArray.length;

        for (int i = sortedLength - 1; i > sortedLength / 2; i--) {


            //Find index of the corresponding number in the original Array.
            for (int nIndex = 0; nIndex < floats.length; nIndex++) {
                if (NumberTools.floatEquals(floats[nIndex], sortedArray[i], CoreTools.PUBLIC_EPSILON)) {
                    floats[nIndex] = sortedArray[i] / sortedArray[sortedLength - i - 1];
                }
            }


        }

    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

