package NumberCruncher.NumberCruncherTopLevel;

import NumberCruncher.NumberCruncherAnonym;

import java.util.Random;

/**
 * An implementation of swirl
 * in PRG-SPR
 *
 * @author Julian
 * @version 1.0
 * @since 2018-Mai-06
 */
public abstract class Swirl implements NumberCruncherAnonym.Operator {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public static void function(float[] floats) {

        Random r = new Random();
        int firstIndex;
        int secondIndex;

        for (int i = 0; i < floats.length; i++) {


            firstIndex = r.nextInt(floats.length - 1);
            secondIndex = r.nextInt(floats.length - 1);

            floats[firstIndex] = floats[secondIndex];
        }

    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

