package NumberCruncher.NumberCruncherTopLevel;

import NumberCruncher.NumberCruncherAnonym;
import Tools.CoreTools;
import Tools.NumberTools;

import java.util.Arrays;

/**
 * An implementation of average
 * in PRG-SPR
 *
 * @author Julian
 * @version 1.0
 * @since 2018-Mai-06
 */
public abstract class Average implements NumberCruncherAnonym.Operator {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public static void function(float[] floats) {
        float[] sortedArray = Arrays.copyOf(floats, floats.length);
        Arrays.sort(sortedArray);

        float largestNumber = sortedArray[sortedArray.length - 1];

        int largestNumberIndex = 0;

        float average = 0F;

        for (int i = 0; i < floats.length; i++) {
            if (NumberTools.floatEquals(floats[i], largestNumber, CoreTools.PUBLIC_EPSILON)) {
                largestNumberIndex = i;
            }

            average += floats[i];
        }

        average /= floats.length;
        floats[largestNumberIndex] = average;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

