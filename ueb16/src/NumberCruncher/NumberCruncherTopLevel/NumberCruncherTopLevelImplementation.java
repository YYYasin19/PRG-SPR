package NumberCruncher.NumberCruncherTopLevel;

import java.util.Random;

/**
 * An implementation of NumberCruncherTopLevelImplementation
 * in PRG-SPR
 *
 * @author Julian
 * @version 1.0
 * @since 2018-Mai-06
 */
public class NumberCruncherTopLevelImplementation {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */


    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    float[] floats;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public NumberCruncherTopLevelImplementation(int size) {
        if (size < 0) {
            throw new IllegalArgumentException();
        }

        floats = new float[size];
        Random r = new Random();

        for (int i = 0; i < size; i++) {
            floats[i] = r.nextFloat();
        }
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * executes the given operations in the string array on the underlying float-array
     * no operation will be done when the given name is not in the realm of options
     *
     * @param operations - possible: sum, swirl, divide, subtract, average
     */
    public void crunch(String... operations) {

        for (String s : operations) {

            switch (s.toLowerCase()) {
                case "sum":
                    Sum.function(this.floats);
                    break;

                case "swirl":
                    Swirl.function(this.floats);
                    break;

                case "divide":
                    Divide.function(this.floats);
                    break;

                case "subtract":
                    Subtract.function(this.floats);
                    break;

                case "average":
                    Average.function(this.floats);
                    break;

                default:
                    break;

            }
        }
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

