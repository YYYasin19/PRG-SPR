import PersonQueue.Person;
import PersonQueue.PersonException;
import PersonQueue.PersonQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class PersonQueueTest {

    /* ---------------------------------------- Test-Constants & Attributes ----------------------------------------- */

    private static final int QUEUE_SIZE = 10;

    private PersonQueue pq;

    @Before
    public void testSetUp() throws Exception {
        //initialize object
        pq = new PersonQueue(QUEUE_SIZE);
        pq.addLast(new Person("Julian", 20));
        pq.addLast(new Person("Yasin", 20));
    }

    @Test
    public void testAddLast() {
        assertEquals(((Person) pq.get(1)).getName(), "Yasin");
    }

    @Test
    public void testRemoveFirst() {
        Person first = (Person) pq.removeFirst();
        assertEquals(first.getName(), "Julian");
    }

    @Test
    public void testGet() {
        assertEquals("Yasin", ((Person) pq.get(1)).getName());
    }

    @Test
    public void testEmpty() {
        assertFalse(pq.empty());
    }

    @Test
    public void testFull() {
        assertFalse(pq.full());
    }

    @Test
    public void testSize() {
        assertEquals(2, pq.size());
    }

    @Test(expected = Exception.class)
    public void testIterator_Exception() throws PersonException {
        pq = new PersonQueue(10); //create empty collection
        Iterator<Person> it = pq.iterator();
        it.next();
    }

    @Test
    public void testIterator() throws PersonException {
        //TODO: please someone solve the issue with this one
        pq = new PersonQueue(10);
        Iterator<Person> it = pq.iterator();
        assertFalse(it.hasNext());
    }
}