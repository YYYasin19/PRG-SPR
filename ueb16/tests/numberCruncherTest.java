import NumberCruncher.NumberCruncher;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

import static junit.framework.TestCase.fail;

public class numberCruncherTest {

    NumberCruncher numberCruncher;

    @Before
    public void setUp() {
        numberCruncher = new NumberCruncher(10);
    }


    /**
     * tests the method {@link NumberCruncher#crunch(String... operations)} executed with the "sum"-Method
     * explanation of method:
     * if everything with "sum" was successful, then every float in the old array should be less than or (at most) equal
     * to every float in the new array. The method fail() is called when this assumption fails.
     */
    @Test
    public void testSum() {
        int length = numberCruncher.getFloats().length;
        float[] oldArray = Arrays.copyOf(numberCruncher.getFloats(),length);
        numberCruncher.crunch("sum");
        float[] newArray = numberCruncher.getFloats();
        //check if everyone is greater/equal than the one before:
        for (int i = 0; i < length; i++){
            if (newArray[i] < oldArray[i]){
                fail();
            }
        }
        //test has passed
    }

    /*

    @Test
    public void testSubtract() {
        int length = numberCruncher.getFloats().length;
        float[] oldArray = Arrays.copyOf(numberCruncher.getFloats(),length);
        numberCruncher.crunch("subtract");
        float[] newArray = numberCruncher.getFloats();

        //check if everyone is less/equal than the one before:
        for (int i = 0; i < length; i++){
            if (Float.compare(newArray[i],oldArray[i]) < 0){
                System.err.println("-->"+i + "\n\n" + newArray[i] + " and " + oldArray[i]);
                fail();
            }
        }
        //test has passed
    }

    */


}