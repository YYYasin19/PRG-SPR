# Checklist for every assignment
This is a checklist that should be filled so that every assignment gets all of the points
- [ ] BlueJ
    - [ ] Jar - Dateien für Libraries wie JUnit sind enthalten?
    - [ ] Alles wurde compiled?
- [ ] Setter und Getter
    - [ ] Es gibt für jedes Attribut einen Setter
    - [ ] Jeder Setter überprüft alle Parameter
    - [ ] Es gibt für jedes Attribut einen Getter
    - [ ] Zugriffsrechte der Setter/Getter sind richtig
        - alle Setter/Getter sollten private sein, außer die auf die zugefriffen wird, die
            werden dann quasi manuell geöffnet (in IntelliJ einstellen?)

- [ ] JavaDoc
    - [x] Jede Methode (bis auf triviale Setter/Getter) ist dokumentiert
    - [x] Kontrollrahmen (Bsp. Uhrzeit kleiner 24) für Parameter
    - [x] geworfene Exceptions

- [ ] JUnit
    - [ ] Konstruktoren werden getestet
    - [ ] Methoden werden getestet
        - [ ] Positiv-Test
        - [ ] Negativ-Test

- [ ] Design und Zusatz
    - [ ] Jede Klasse hat ihre eigene Exception-Klasse 
        - Das kann man sicherlich ins FileTemplate einbauen
    - [ ] Dialog zum Testen
    - [ ] Imports optimieren (IntelliJ Funktion)
