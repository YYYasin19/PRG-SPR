import java.util.Queue;

/**
 * An Interface called MinHeap
 * in ueb20
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Jun-07
 */
public interface MinHeap<Type extends Comparable<Type>> extends Queue<Type> {

    @Override
    boolean offer(Type type);

    @Override
    Type poll();

    @Override
    Type peek();

}
