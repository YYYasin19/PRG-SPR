import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static Tools.NumberTools.minIntArray;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class MinHeapTest {

    private static final int MIN_HEAP_SIZE = 10;
    private MinHeapImpl<Integer> integerMinHeap;

    /* ---------------------------------------- Before -------------------------------------------------------------- */
    @Before
    public void setUp() {
        integerMinHeap = new MinHeapImpl<>(MIN_HEAP_SIZE);
    }

    /* ---------------------------------------- Main Test-Methods --------------------------------------------------- */

    /**
     * this test offers the MinHeap Values and then peeks at the specified index to found out if the values
     * have been inserted correctly
     */
    @Test
    public void testOffer() {
        //add some values
        integerMinHeap.offer(0);
        integerMinHeap.offer(1);
        integerMinHeap.offer(2);
        integerMinHeap.offer(3);

        //test if those values are there
        if (integerMinHeap.peekAt(0) != 0) fail();
        if (integerMinHeap.peekAt(1) != 1) fail();
        if (integerMinHeap.peekAt(2) != 2) fail();
        if (integerMinHeap.peekAt(3) != 3) fail();

    }

    /**
     * {@link MinHeapTest#testOffer} - this one does it backwards to check if the heap condition was reconstructed
     */
    @Test
    public void testOfferBackwards() {
        //add some values
        integerMinHeap.offer(3);
        integerMinHeap.offer(2);
        integerMinHeap.offer(1);
        integerMinHeap.offer(0);

        //test if those values are there
        if (integerMinHeap.peekAt(0) != 0) fail();
        if (integerMinHeap.peekAt(1) != 1) fail();
        if (integerMinHeap.peekAt(2) != 2) fail();
        if (integerMinHeap.peekAt(3) != 3) fail();
    }

    /**
     * first adds the elements to the MinHeap
     * then polls them one after another and compares them with an ordered list of the elements that were passed at the
     * beginning
     */
    @Test
    public void testPoll() {
        //create test elements
        Integer[] test = {6, 2, 4, 7, 3, 29, 2};
        List<Integer> list = Arrays.asList(test);
        //add to heap
        integerMinHeap.addAll(list);

        //get a sorted list with these values. the minHeap should return those as well when polled sequentially
        list.sort(Integer::compareTo);
        int size = integerMinHeap.size();

        for (int i = 0; i < size; i++) {
            if (Integer.compare(list.get(i), integerMinHeap.poll()) != 0) fail();
        }

    }

    @Test
    public void testPeek() {
        //create test elements
        Integer[] test = {6, 2, 4, 7, 3, 29, 2};
        List<Integer> list = Arrays.asList(test);
        //add to heap
        integerMinHeap.addAll(list);

        //check if smallest element on top
        assertTrue("The smallest value in that MinHeap was not the smallest value, that was passed to it.",
                Integer.compare(minIntArray(test), integerMinHeap.peek()) == 0);
    }

    /**
     * this test simply creates a list of methods that this integerMinHeap has to implement but does not support
     * these methods should return a {@link UnsupportedOperationException}, otherwise this test fails
     */
    @Test
    public void testUnsupportedOperations() {
        StringBuilder res = new StringBuilder("Unsupported Operations: \n");
        ArrayList<MinHeapAction> actions = new ArrayList<>();
        actions.add(() -> integerMinHeap.toArray());
        actions.add(() -> integerMinHeap.toArray(new Integer[10]));
        actions.add(() -> integerMinHeap.clear());
        actions.add(() -> integerMinHeap.forEach(System.out::println));
        actions.add(() -> integerMinHeap.element());
        actions.add(() -> integerMinHeap.contains(0));
        actions.add(() -> integerMinHeap.retainAll(new ArrayList<>()));
        actions.add(() -> integerMinHeap.remove());
        actions.add(() -> integerMinHeap.remove(0));
        actions.add(() -> integerMinHeap.containsAll(new ArrayList<>()));

        for (MinHeapAction m : actions) {
            try {
                m.action();
            } catch (UnsupportedOperationException uoe) {
                res.append(uoe.getLocalizedMessage()).append("\n");
            } catch (Exception t) {
                //it should always be UnsupportedOperationException
                System.err.println(t.getLocalizedMessage());
                fail();
            }
        }

        System.out.println(res.toString());
    }

    /* ---------------------------------------- After --------------------------------------------------------------- */
    @After
    public void tearDown() {

    }

    /* ---------------------------------------- Other Test-Methods -------------------------------------------------- */


    /**
     * this interface is used for the test {@link #testUnsupportedOperations()}
     */
    interface MinHeapAction {
        void action() throws UnsupportedOperationException;
    }

    /* ---------------------------------------- Other Methods ------------------------------------------------------- */

}