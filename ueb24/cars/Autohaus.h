//
// Created by takezo on 7/9/18.
//

#ifndef UEB24_AUTOHAUS_H
#define UEB24_AUTOHAUS_H

typedef struct Autohaus{

    Car *cararray;
    size_t currentsize;
    size_t maxsize;

} Autohaus;

#endif //UEB24_AUTOHAUS_H
