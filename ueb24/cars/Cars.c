//
// Created by takezo on 7/9/18.
//

#include <memory.h>
#include "Cars.h"

Car makeCar(char *marke, int hoechstgeschwindigkeit, int tuerenanzahl, bool hat_abs, char (*sonderaustattung)[100],
            Engine engine) {
    Car car;
    memcpy(car.marke, marke, sizeof(char) * 20);
    car.hoechstgeschwindigkeit = hoechstgeschwindigkeit;
    car.tuerenanzahl = tuerenanzahl;
    car.hat_abs = hat_abs;
    memcpy(car.sonderaustattung, sonderaustattung, sizeof(char) * 10 * 100);

    return car;
}

int getValue(Car car) {
    int val = 0;

    val *= car.hoechstgeschwindigkeit * car.tuerenanzahl + (car.hat_abs ? 5000 : 0);
    for(int i = 0; i < 10; i++){
        if(car.sonderaustattung[i]){
            val += 3000;
        }
    }

    if(car.marke == "Porsche"){
        val *= 2;
    }

    return val;
}
//TODO
void printCar(Car car) {

}
