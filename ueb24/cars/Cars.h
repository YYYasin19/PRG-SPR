//
// Created by takezo on 7/9/18.
//

#ifndef UEB24_CARS_H
#define UEB24_CARS_H

#include <stdbool.h>

typedef struct engine_t{
    int PS;
    int zylinder;
    int hubraum;
} Engine;

typedef struct car_t{
    char marke[20];
    int hoechstgeschwindigkeit;
    int tuerenanzahl;
    bool hat_abs;
    char sonderaustattung[10][100];
    Engine engine;
} Car;

Car makeCar(char marke[20],
            int hoechstgeschwindigkeit,
            int tuerenanzahl,
            bool hat_abs,
            char sonderaustattung[10][100],
            Engine engine);

int getValue(Car car);

void printCar(Car car);



#endif //UEB24_CARS_H
