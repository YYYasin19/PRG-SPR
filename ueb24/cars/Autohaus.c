//
// Created by takezo on 7/9/18.
//

#include <wchar.h>
#include <stdlib.h>
#include "Cars.h"
#include "Autohaus.h"

Autohaus *createAutohaus(size_t size){
    Autohaus *autohaus = malloc(sizeof(autohaus));
    autohaus->maxsize = size;
    autohaus->currentsize = 0;
    autohaus->cararray = calloc(sizeof(Car), size);
    return autohaus;
}

bool addCar(Autohaus *autohaus, Car car) {

    if(!autohaus{
        return false;
    }else if(autohaus->currentsize == autohaus->maxsize){
        return false;
    }else{
        autohaus->cararray[autohaus->currentsize] = car;
        autohaus->currentsize++;
        return true;
    }
}

Car *removeCar(Autohaus *autohaus, int index){

    if(!autohaus){
        return NULL;
    }

    if(index > autohaus->maxsize || index > autohaus->currentsize){
        return NULL;
    }

    Car *car = malloc(sizeof(Car));

    *car = autohaus->cararray[index];
    autohaus->cararray[index] = NULL;

    return car;

}
