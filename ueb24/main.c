#include <stdio.h>
#include <stdlib.h>
#include "libmatrx/matrixfunc.h"

int** initMatrix(int rows, int columns);

int main() {


    int rows = 3;
    int columns = 3;

    int** matrix = initMatrix(rows, columns);

    print_matrix(matrix, rows, columns);

    printf("Max of column %i: %i\n", 1, find_max_in_col(matrix, rows, columns, 1));

    printf("Max of row %i: %i\n",1, find_max_in_row(matrix, rows, columns, 1));

    swap_rows(matrix, 3, 3, 0, 2);

    printf("\n Matrix after row %i and %i were swapped:\n", 0, 2);
    print_matrix(matrix, rows, columns);

    return 0;
}

int** initMatrix(int rows, int columns){
    int **matrix = (int **)malloc(rows * sizeof(int *));;

    for (int i=0; i<rows; i++)
        matrix[i] = (int *)malloc(columns * sizeof(int));

    int x = 1;

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < columns; j++){

            //*(*(matrix+i)+j) = x;
            matrix[i][j] = x;
            x++;
        }
    }

    return matrix;
}