//
// Created by takezo on 7/5/18.
//

#ifndef UEB24_MATRIXFUNC_H
#define UEB24_MATRIXFUNC_H

void print_matrix(int **matrix, int columns, int rows);

int find_max_in_col(int **matrix, int rows, int columns, int row_index);

int find_max_in_row(int **matrix, int rows, int columns, int row_index);

void swap_rows(int** matrix, int rows, int columns, int index1, int index2);



#endif //UEB24_MATRIXFUNC_H
