//
// Created by takezo on 7/5/18.
//

#include "matrixfunc.h"
#include <stdio.h>
#include <zconf.h>

void print_matrix(int **matrix, int columns, int rows){

    printf("\n");

    for(int i = 0; i < columns; i++){

        printf("|");

        for(int j = 0; j < rows; j++){
            printf(" %i ", matrix[i][j]);
        }

        printf("|\n");
    }

    printf("\n");
}


int find_max_in_col(int **matrix, int rows, int columns, int col_index){

    if(col_index - 1> columns){
        return -1;
    }

    int maxnum;

    for(int i = 0; i < rows; i++){
        if(!maxnum){
            maxnum = matrix[i][col_index];
        }else{
            if(matrix[i][col_index] > maxnum){
                maxnum = matrix[i][col_index];
            }
        }
    }

    return maxnum;
}

int find_max_in_row(int **matrix, int rows, int columns, int row_index){

    if(row_index - 1> rows){
        return -1;
    }

    int maxnum = INT_MIN;

    for(int j = 0; j < columns; j++){
        if(!maxnum){
            maxnum = matrix[row_index][j];
        }else{
            if(matrix[row_index][j] > maxnum){
                maxnum = matrix[row_index][j];
            }
        }
    }

    return maxnum;
}

//Bäh.
void swap_rows(int** matrix, int rows, int columns, int index1, int index2){
    if(index1 -1 > rows || index2 - 1 > rows){
        return;
    }else if(index1 == index2){
        return;
    }

    int *row1 = *(matrix + index1);
    int *row2 = *(matrix + index2);;

    *(matrix + index1) = row2;
    *(matrix + index2) = row1;

}
