#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "task1.h"
#include "fibonacci.h"



int main(int argc, char* argv[]) {
    if(argc < 2){
        printf("No arguments given. Usage:%s [-invoice] [-fib]\n", argv[0]);

        return 1;
    }else{
        if(strstr(argv[1], "-invoice") != NULL){
            return abs(runprce());
        }else if(strstr(argv[1], "-fib") != NULL){
            return abs(runfib());
        }else{
            printf("Wrong arguments. Usage:%s [-invoice] [-fib]\n", argv[0]);
            return 1;
        }
    }
}

