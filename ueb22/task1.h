//
// Created by takezo on 6/24/18.
//

#ifndef UEB22_TASK1_H
#define UEB22_TASK1_H

int runprce(void);

void calcbrtt(float *price_ptr);

float calcmwst(const float *price_ptr);

float calcsknt(const float *price_ptr);

void calcbtrg(float *price_ptr);

#endif //UEB22_TASK1_H
