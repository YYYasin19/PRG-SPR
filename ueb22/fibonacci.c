//
// Created by takezo on 6/24/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "fibonacci.h"


///
/// \return -1 on error, 0 when execution is successful
int runfib(void){
    printf("Fibonacci Calculator. Enter integer: ");

    char *str = malloc(sizeof(char));

    scanf("%s", str);

    //convert str to int, endptr is NULL, base 10.
    long long num =  strtol(str, NULL, 10);

    if(num == 0) {

        if(errno == ERANGE){
            perror("Number out of range of LONG_MAX or LONG_MIN");
            free(str);
            return -1;
        }else{
            printf("strtol returned 0L. Conversion error or input was 0. Continuing...\n");
        }
    }

    calcfib(num);
    free(str);
    return 0;
}


void calcfib(long long num){

    long long fib = 1;
    long long prevFib = 1;

    printf("Fibonacci No.1: %lli\n", fib);

    for(int i = 2; i < num; i++){

        printf("Fibonacci No.%i: %lli\n", i, fib);

        long long temp = fib;
        fib+= prevFib;
        prevFib = temp;


    }


}