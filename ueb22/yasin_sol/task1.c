#include <stdio.h>
#include "functions.h"

int main(){
	
	//variables
	double price;
	double taxes;
	double priceWithTaxes;
	double skonto;
	double priceSkonto;
	
	//data input
	printf("Please enter the sum\n");
	scanf("%lf",&price);
	
	//calculation
	taxes = price * 0.2;
	priceWithTaxes = price + taxes;
	skonto = priceWithTaxes * 0.02;
	priceSkonto = priceWithTaxes - skonto;
	
	//output
	printf("Nettopreis               EURO %04.2f\n",price);
	printf("+20%% MwSt.               EURO %04.2f\n",taxes);
	printLineSep();
	printf("Bruttopreis              EURO %04.2f\n",priceWithTaxes);
	printf("-2%% Skonto               EURO %04.2f\n",skonto);
	printLineSep();
	printf("Rechnungsbetrag          EURO %04.2f\n",priceSkonto);
	
}