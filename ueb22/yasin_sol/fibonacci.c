#include <stdio.h>
#include "functions.h"

/* Constants ------------------------------------------------------------ */
static const int MAX_FIB_SIZE = 100;

/* Functions ------------------------------------------------------------ */
int calcFib(int);

int main(void){
	
	char charUserInput[3];
	int userInput;
	
	//prompt user
	printf("Welcome to the Fibonacci-Programm. Please input a valid number between 0 and %i. \nWhereas you should not use something larger than 100 in order to minimize the time needed for computation.\n", MAX_FIB_SIZE);
	
	//get the input
	scanf("%i", &userInput);
	
	//do the calculation
	int result = calcFib(userInput);
	
	printf("The Fibonacci number at %d is %d\n", userInput, result);
	
	//end the system
	return 0;
}

int calcFib(int fib){
	
	//init
	static int fibValues[100];
	fibValues[0] = 1;
	fibValues[1] = 1;
	
	if (fib == 0){
		return 1;
	} else if (fib == 1){
		return 1;
	} else {
		//loop for calculation
		printf("-- Calculation Details -- \n");
		printf("> Fib of %3d is %d\n", 0, fibValues[0]);
		printf("> Fib of %3d is %d\n", 1, fibValues[1]);
		for (int i = 2; i <= fib; i++){
			fibValues[i] = fibValues[i-1] + fibValues[i-2];
			printf("> Fib of %3d is %d\n", i, fibValues[i]);
		}
		printf("------------------------- \n");
	}
	return fibValues[fib];
} 
