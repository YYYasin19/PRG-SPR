//
// Created by takezo on 6/24/18.
//

#include "task1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

int runprce(void){

    //TODO give better names.
    printf("Calculating Invoice.\n");
    printf("Please insert a price above 0: ");


    //Allocating space for string


    char *str = (char*) malloc(sizeof(float));


    //Allocating space for price
    float *price_ptr = (float*) malloc(sizeof(float));

    //if scanf returns an error return user Input, print the error and free allocated space. Also return -1
    if(scanf("%s", str) < 1){
        fprintf(stderr ,"Error reading input. %s\n", strerror(errno));
        free(str);
        free(price_ptr);
        return -1;
    }



    //fix for strtof, which converts to a 1.X float if the given string literal does not contain a dot.
    if(strstr(str, ".") == NULL){
        //concat .00 to string
        strcat(str, ".00");
    }

    //If pointer to first occurence of "." char is not pointer of last occurence of "." char
    if(strpbrk(str, ".") != strrchr(str, '.')){
        fprintf(stderr, "Err: Multiple \".\" detected in number.\n");
        free(str);
        free(price_ptr);
        return -1;
    }

    //entptr NULL
    *price_ptr = strtof(str, NULL);

    if(*price_ptr == 0){

        if(errno == ERANGE){
            fprintf(stderr, "Err: %s\n", strerror(errno));
        }else{
            fprintf(stderr, "Err:  User Input was 0 or String to float conversion failed when strtof returned 0.0F.\n");

        }

        free(str);
        free(price_ptr);

        return -1;
    }

    if(*price_ptr == INFINITY){
        fprintf(stderr, "Err: User Input too large");
        //str = NULL;
        *price_ptr = 0;
        return -1;
    }

    if(*price_ptr < 0 ){
        fprintf(stderr, "\nErr:Price must not be below zero.\n");
        free(str);
        free(price_ptr);
        return -1;
    }

    printf("\n\nNettopreis\tEuro %f\n", *price_ptr);


    printf("+ 20%% MwSt\tEuro %f\n", calcmwst(price_ptr));

    printf("====================\n");

    calcbrtt(price_ptr);

    printf("Bruttopreis\tEuro %f\n", *price_ptr);



    printf("- 2%% Skonto\tEuro %f\n", calcsknt(price_ptr));
    printf("====================\n");

    calcbtrg(price_ptr);

    printf("Rechnungsbetrag\tEuro %f\n", *price_ptr);

    free(str);
    free(price_ptr);

    return 0;
}




void calcbrtt(float *price_ptr){
    *price_ptr *= 1.2;
}

float calcmwst(const float *price_ptr){
    return (float) (*price_ptr * 0.2);
}

void calcbtrg(float *price_ptr){
    *price_ptr *= 0.98;
}

float calcsknt(const float *price_ptr){
    return (float) (*price_ptr * 0.02);
}