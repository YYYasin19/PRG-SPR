import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PalindromeRecursiveCalculatorTest {

    public static final String TEST_STRING = "BaCaB";
    public static final String TEST_STRING1 = "baab";
    public static final String TEST_STRING2 = "bACAb";

    PalindromeRecursiveCalculator recursiveCalculator;

    @Before
    public void setUp() throws Exception {
        recursiveCalculator = new PalindromeRecursiveCalculator();
    }

    @org.junit.Test
    public void checkPalindrome() {
        assertTrue(recursiveCalculator.checkPalindrome(TEST_STRING));
        assertTrue(recursiveCalculator.checkPalindrome(TEST_STRING1));
        assertTrue(recursiveCalculator.checkPalindrome(TEST_STRING2));
    }

    @Test
    public void checkPalindromeNegativeTest(){
        assertFalse(recursiveCalculator.checkPalindrome(""));
        assertFalse(recursiveCalculator.checkPalindrome("abc"));
    }
}