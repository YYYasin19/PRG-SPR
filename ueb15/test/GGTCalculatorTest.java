import org.junit.Test;

import static org.junit.Assert.*;

public class GGTCalculatorTest {

    @Test
    public void negativeTest(){
        assertEquals(0, GGTCalculator.calcGGT(1, 0));
        assertEquals(0, GGTCalculator.calcGGT(0, 1));
    }
}