import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class PalindromeIterativeCalculatorTest {

    public static final String TEST_STRING = "BAAAaaaaaaAAAB";
    PalindromeIterativeCalculator pic = new PalindromeIterativeCalculator();

    @Test
    public void checkPalindromeNegativeTest(){

        assert(pic.checkPalindrome("") == false);

        File f = null;
        assert(pic.checkPalindrome(f) == false);

        assertFalse(pic.checkPalindrome("abc"));
    }

    @Test
    public void checkPalindromePositiveTest(){
        assertTrue(pic.checkPalindrome(TEST_STRING));
    }
}