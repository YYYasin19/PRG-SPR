/**
 * An implementation of GgtCalculator
 * in Prog2_Ueb15
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-Apr-17
 */
public class GGTCalculator {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    public static void main(String[] args) {
       if(args.length != 2){
           System.out.println("Usage: GGTCalculator <int a> <int b>");
       }else{

           int a= 0, b = 0;
           boolean correctInput = true;
           try{
                a = Integer.parseInt(args[0]);
           }catch(NumberFormatException e){
               System.out.println("first argument is not a valid integer");
               correctInput = false;
           }

           try{
               b = Integer.parseInt(args[1]);

           }catch(NumberFormatException e){
               System.out.println("second argument is not a valid integer");
               correctInput = false;
           }

           if(correctInput) {
               System.out.println("GGT of " + a + " and " + b + ": " + calcGGT(a, b));
           }
       }
    }


    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public static int calcGGT(int a, int b){
        if(b == 0){
            return a;
        }

        else{
            return calcGGT(b, a % b);
        }
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
