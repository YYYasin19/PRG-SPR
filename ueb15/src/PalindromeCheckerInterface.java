/**
 * An Interface of PalindromeIterativeCalculator
 * in Prog2_Ueb15
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-21
 */
public interface PalindromeCheckerInterface {


    boolean checkPalindrome(String s);

    String getAlgorithmName();

}


