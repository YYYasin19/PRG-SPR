import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.*;

/**
 * An implementation of PalindromeSpeedComparisonBarChart
 * in ueb15
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-22
 */
public class PalindromeSpeedComparisonBarChart extends ApplicationFrame {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    double[][] timesReference;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    //you can use this to adjust the contents of this class:
    public static final String Y_AXIS_LABEL = "Seconds";
    public static final String X_AXIS_LABEL = "Problem Size";
    public static final PlotOrientation ORIENTATION = PlotOrientation.VERTICAL;

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * constructor for PalindromeSpeedComparisonBarChart
     * you have to call the draw()-function on an object of this class in order to show the chart to the user
     * @param title title of the chart
     * @param times 2d array of times that should be displayed
     * @param testClasses the classes that were tested
     * @param palindromeSizes the problem sizes
     */
    public PalindromeSpeedComparisonBarChart(String title, double[][] times, PalindromeCheckerInterface[] testClasses, int[] palindromeSizes) {
        super(title);
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        timesReference = times;
        int testedAlgorithms = testClasses.length;
        int testedProblemSizes = palindromeSizes.length;

        for (int i = 0; i < testedAlgorithms; i++) {
            String algorithmName = testClasses[i].getAlgorithmName();

            for (int j = 0; j < testedProblemSizes; j++) {

                dataset.addValue(times[i][j],algorithmName,String.valueOf(palindromeSizes[j]));

            }
        }

        JFreeChart chart = ChartFactory.createBarChart(
                title,
                X_AXIS_LABEL,
                Y_AXIS_LABEL,
                dataset,
                ORIENTATION,
                true,
                true,
                false
        );

        ChartPanel chartPanel = new ChartPanel(chart,false);
        chartPanel.setPreferredSize(new Dimension(1024,600));
        setContentPane(chartPanel);

    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * draws the graph for the user to see
     */
    public void draw(){
        this.configure(); //configures the current window
        this.pack();
        RefineryUtilities.centerFrameOnScreen(this);
        this.setVisible(true);
    }

    /**
     * configures the current window
     */
    private void configure() {

        //creating a menu for the user to interact
        JMenuBar bar = new JMenuBar();
        JMenu functionMenu = new JMenu("Functions");
        JMenuItem saveItem = new JMenuItem("Save");
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PalindromeSpeedComparison.createCSVFile(timesReference);
            }
        });

        //add everything:
        this.add(bar);
        bar.add(functionMenu);
        functionMenu.add(saveItem);
        this.setJMenuBar(bar);

    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
