/**
 * An implementation of PalindromeRecursiveCalculator
 * in Prog2_Ueb15
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-17
 */
public class PalindromeRecursiveCalculator implements PalindromeCheckerInterface {



    /* ---------------------------------------- Main ---------------------------------------------------------------- */


    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final String NAME_OF_CLASS = "Recursive Palindrome Checker";


    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * checks if a given string is a palindrome
     * @param s String that has to be checked
     * @return true if and if only given string is a palindrome
     */
    @Override
    public boolean checkPalindrome(String s) {
        if(s == null || s.length() == 0){
            return false;
        }
        int length = s.length();

        if (length == 0){
            return true;
        }

        return checkPalindromeRec(s,0,length-1);
    }

    /**
     * checks if a string is a palindrome recursively (help function)
     * @param s String
     * @param start starting point for the check
     * @param end end point for the check
     * @return true - if and if only - the given string is a palindrome
     */
    private boolean checkPalindromeRec(String s, int start, int end){
        //validate input:
        if(s == null ||s.length() == 0 || start < 0 || end < 0){
            return false;
        }

        //only one char:
        if (start == end) {
            return true;
        }

        //check if start and end match:
        if (s.charAt(start) != s.charAt(end)){
            return false;
        }

        //check middle recursive:
        if (start < (end + 1)) {
            return checkPalindromeRec(s,start+1,end-1);
        }

        return true;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * returns the name of the used algorithm in this class
     * @return String name of algorithm
     */
    @Override
    public String getAlgorithmName() {
        return NAME_OF_CLASS;
    }
}
