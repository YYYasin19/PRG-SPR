import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of PalindromeSpeedComparison
 * in ueb15
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-21
 */
public class PalindromeSpeedComparison {

    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    //Here you can adjust the settings of this programm
    private static int[] palindromeSizes = {
            100,
            200,
            500,
            1000,
            2000,
            5000,
            10000
    };

    private static PalindromeCheckerInterface[] classes = {
            new PalindromeIterativeCalculator(),
            new PalindromeRecursiveCalculator()
    };

    public static final String CSV_EXTENSION = ".csv";
    public static final String CSV_EXTENSION_NAME = "csv";

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /**
     * the main method of this class creating a chart for the solutions
     * @param args String[] is not considered
     */
    public static void main(String[] args) {

        /*

        long[] palindromSizes = {10,100,1000,10000,1000000,100000000,1000000000}
        -- > createPalindromes() --> createString() --> createString()
        String[] palindroms = {,,,,}
        -- > test () || Class[] classes = {Palindrome.class} --> Palindrome.class.newInstance().checkString(s);
        double[classes.length][palindromSizes.length]

        [][][][][][]...
        [][][][]..
         . . . .
         .

        --> createGraph()
         */

        try {
            //the main code goes here:
            PalindromeSpeedComparison speedComparison = new PalindromeSpeedComparison();
            speedComparison.start();
        } catch (PalindromeSpeedComparisonException e) {
            e.printStackTrace();
        }


    }

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private String[] palindromes;
    private double[][] times;

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * default constructor for the speed comparison
     * sets up the environment and can take a little while if started with big numbers
     * @throws PalindromeSpeedComparisonException
     */
    private PalindromeSpeedComparison() throws PalindromeSpeedComparisonException {

        this.setPalindromes(createPalindromes(palindromeSizes));
        this.setTimes(testPalindromeFunctions(classes));

    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */


    /**
     * something like the second main()-method for this class
     * all the other methods are called from here and at the end the graph is created and given out to the user
     */
    private void start(){

        this.createGraph(this.times);

    }

    /**
     * takes an array of sizes and creates palindroms at each place for the given
     * size in the input array
     * @param palindromeSizes array with sizes for the palindromes
     * @return String[] of palindromes
     */
    private String[] createPalindromes(int[] palindromeSizes) throws PalindromeSpeedComparisonException {
        String[] palindromes = new String[palindromeSizes.length];
        for (int i = 0; i < palindromes.length; i++){

            palindromes[i] = createPalindrome(palindromeSizes[i],"a");

        }

        return palindromes;
    }

    /**
     * returns a palindrome with the length of
     * @param palindromeSize size of the palindrome
     * @param letter the letter the palindrome should consist of
     * @return a palindrome
     */
    private String createPalindrome(int palindromeSize, String letter) throws PalindromeSpeedComparisonException {
        StringBuffer result = new StringBuffer("");
        for (int i = 0; i < palindromeSize; i++) {
            result.append(letter);
        }

        return result.toString();
    }

    /**
     * creates a 2d times array with [classes.lentgh][palindromSizes.length] and fills it with the
     * result times the test of each function have returned for their corresponding problem sizes
     * @return double[][] of times
     */
    private double[][] testPalindromeFunctions(PalindromeCheckerInterface[] classes){

        /*
        Information about the array:
                        10      100     1000    10000   ... (look at palindromeSizes[])
        FirstClass:     []      []      []      []      []
        SecondClass:    []      []      []      []      []
        .               .       .       .       .       .
        .               .       .       .       .       .
        .               .       .       .       .       .
        (look at classes[])

        so times[0][1] gives me the comparison result of a 100 char-palindrome with the first test class (PalindromeIterativeCalculator)
         */

        double[][] times = new double[classes.length][palindromeSizes.length];

        for (int classIndex = 0; classIndex < times.length; classIndex++){
            PalindromeCheckerInterface checker = classes[classIndex];

            for (int sizeIndex = 0; sizeIndex < times[classIndex].length; sizeIndex++){
                //set everything up:
                String pal = palindromes[sizeIndex];


                //start timer:
                long start = System.nanoTime();

                //do calculation:
                checker.checkPalindrome(pal);

                //end timer:
                long end = System.nanoTime();

                //calculate difference:
                long time = (end - start) / 100; //TODO: multiplicate here, because the numbers get to small

                //write calculation to array:
                times[classIndex][sizeIndex] = time;

            }

        }

        return times;
    }

    /**
     * creates a comma seperated text file with the values of a 2d array
     * @param times content for the csv file
     */
    public static void createCSVFile(double[][] times){

        StringBuilder headerRow = new StringBuilder("Problem Sizes");
        StringBuilder actualTable = new StringBuilder();

        for (int sizes : palindromeSizes) {
            headerRow.append(",").append(sizes);
        }
        headerRow.append("\n");

        // ---------------------

        //now the actual values:
        for (int classIndex = 0; classIndex < classes.length; classIndex ++) {
            actualTable.append(classes[classIndex].getAlgorithmName());
            int valueCount = times[classIndex].length;

            //the value row:
            for (int valueIndex = 0; valueIndex < valueCount; valueIndex++) {

                actualTable.append(",").append(times[classIndex][valueIndex]);

            }

            //new line
            actualTable.append("\n");
        }

        String result = headerRow.toString() + actualTable.toString();

        //save the result in a file:
        saveTextInFile(result,CSV_EXTENSION);
    }

    private static void saveTextInFile(String text, String extension) {
        JFileChooser jfc = new JFileChooser();
        jfc.setCurrentDirectory(new File("./"));

        switch (jfc.showSaveDialog(new JButton())) {

            case JFileChooser.APPROVE_OPTION:
                File outputFile = new File(jfc.getSelectedFile() + extension);

                //you could collapse those if statements but i think this shows the logic better
                if (outputFile.exists()) {
                    if ((JOptionPane.showConfirmDialog(new JPanel(),"Do you want to override?")
                            == JOptionPane.NO_OPTION)) {
                        return;
                    }
                }

                try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile))){
                    out.write(text);
                } catch (IOException e) {
                    System.err.println(e);
                }

                break;
            case JFileChooser.CANCEL_OPTION:
                System.out.println("INFO: File Saving Was Cancelled.");
                return;
        }

    }

    /**
     * creates a graph for the given 2d times[][] array
     */
    private void createGraph(double[][] times){

        PalindromeSpeedComparisonBarChart comparisonBarChart = new PalindromeSpeedComparisonBarChart(
                "Algorithm Speed Comparison",
                times,
                classes,
                palindromeSizes
        );

        comparisonBarChart.draw();
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * sets the non-null table of result times after testing every algorithm
     * @param doubles 2d array of result times
     * @throws PalindromeSpeedComparisonException thrown when doubles is null
     */
    private void setTimes(double[][] doubles) throws PalindromeSpeedComparisonException {
        if (doubles == null) {
            throw new PalindromeSpeedComparisonException(getMsg("WRONG_INPUT"));
        }

        this.times = doubles;
    }

    /**
     * sets the non-null table of palindromes for testing purposes
     * @param palindromes 2d array of palindromes
     * @throws PalindromeSpeedComparisonException thrown when palindromes is null
     */
    private void setPalindromes(String[] palindromes) throws PalindromeSpeedComparisonException {
        if (palindromes == null) {
            throw new PalindromeSpeedComparisonException(getMsg("WRONG_INPUT"));
        }
        this.palindromes = palindromes;
    }

}

class PalindromeSpeedComparisonException extends Exception {

    public PalindromeSpeedComparisonException(String msg){
        super(msg);
    }

}
