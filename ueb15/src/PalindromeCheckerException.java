/**
 * An implementation of PalindromeCheckerException
 * in ueb15
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-21
 */
public class PalindromeCheckerException extends Exception {

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    protected PalindromeCheckerException(String msg) {
        super(msg);
    }

}
