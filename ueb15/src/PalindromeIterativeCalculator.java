import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * An implementation of PalindromeIterativeCalculator
 * in Prog2_Ueb15
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Apr-17
 */
public class PalindromeIterativeCalculator implements PalindromeCheckerInterface {




    /* ---------------------------------------- Main ---------------------------------------------------------------- */


    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final String NAME_OF_CLASS = "Iterative Palindrome Checker";


    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * checks if a given string is a palindrome
     * @param s String that has to be checked
     * @return true if and if only given string is a palindrome
     */
    @Override
    public boolean checkPalindrome(String s) {

        if(s == null){
            return false;
        }else if(s.length() == 0){
            return false;
        }

        int i;
        char[] chars = s.toCharArray();
        int charsLength = chars.length;

        for (i = 0; i < charsLength / 2; i++){

            if (chars[i] != chars[charsLength - i - 1]) {
                return false;
            }

        }

        return true;
    }

    /**
     * iterates over strings of a file and checks if those are a palindrome as well
     * @param f file that should be checked
     * @return true if and if only file consists of palindromes
     */
    public boolean checkPalindrome(File f){
        if(f == null){
            return false;
        }

        if(f.isFile() && f.canRead()) {
            try (Scanner in = new Scanner(f)) {

                while (in.hasNext()) {
                    if (!checkPalindrome(in.next())) {
                        return false;
                    }
                }

            } catch (IOException io) {
                System.err.println(io);
            }
        }

        return true;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * returns the name of the used algorithm in this class
     * @return String name of algorithm
     */
    @Override
    public String getAlgorithmName() {
        return NAME_OF_CLASS;
    }
}
