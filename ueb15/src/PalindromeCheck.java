import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import static Tools.CoreTools.check;
import static Tools.CoreTools.println;
import static Tools.MessageTools.getMsg;

/**
 * An implementation of PalindromeCheck
 * in Prog2_Ueb15
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-Apr-17
 */
public class PalindromeCheck {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    public static void main(String[] args) {
        File mainFile;
        String string;

        /*
        The principle:
        if the user has entered something, that is the string that has to be checked
        otherwise the user can choose a file
        then the start() method is called which analizes the string and outputs the answer
         */

        try {
            if (args.length == 0) {
                mainFile = chooseFile(); //lets the user choose a file
                string = fileToString(mainFile);
            } else {
                string = args[0];
            }

            //start the operation:
            start(string);

        } catch (PalindromeCheckerException e) {
            System.err.println(e);
            System.exit(1); //TODO: find a better solution
        }

    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * the start method
     * @param input Input that will be checked for palindromes
     * @throws PalindromeCheckerException thrown if input is empty
     */
    private static void start(String input) throws PalindromeCheckerException {
        StringBuilder res = new StringBuilder();
        res.append("PalindromeCheck\n");
        res.append("---------------\n");
        res.append(String.format("Input:\n%s\n---------------\n",input));

        if (checkStringIterative(input)) {
            res.append("--> IS a palindrome.\n");
        } else {
            res.append("--> IS NOT a palindrome.\n");
        }
        res.append("---------------\n");

        println(res.toString());
    }

    /**
     * lets the user choose a file from the underlying file system
     * @return the chosen file
     */
    private static File chooseFile() throws PalindromeCheckerException {

        JFileChooser jfc = new JFileChooser(".");
        JButton jButton = new JButton();

        //configuration of File Chooser:
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfc.setDialogTitle(getMsg("FILECHOOSER_TITLE"));

        if (jfc.showOpenDialog(jButton) == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        }

        throw new PalindromeCheckerException(getMsg("CANCELLED"));
    }

    /**
     * checks if a given String is a palindrome or not
     * @param s String
     * @return true if String is palindrome
     */
    private static boolean checkStringIterative(String s) throws PalindromeCheckerException {

        //validate input
        if (s == null){
            throw new PalindromeCheckerException(getMsg("WRONG_INPUT"));
        }

        PalindromeIterativeCalculator iterativeCalculator = new PalindromeIterativeCalculator();
        return iterativeCalculator.checkPalindrome(s);
    }

    /**
     * checks if a given String is a palindrome or not
     * @param s String
     * @return true if String is palindrome
     */
    private static boolean checkStringRecursive(String s) throws PalindromeCheckerException {

        //validate input
        if (s == null){
            throw new PalindromeCheckerException(getMsg("WRONG_INPUT"));
        }

        PalindromeRecursiveCalculator iterativeCalculator = new PalindromeRecursiveCalculator();
        return iterativeCalculator.checkPalindrome(s);
    }

    /**
     * reads a file and returns its contents as a string
     * @param file input file
     * @return string content of file
     */
    private static String fileToString(File file) {

        StringBuilder result = new StringBuilder();

        if (file.canRead()){

            //using try with resources statement to automatically close streams
            try (Scanner in = new Scanner(new FileReader(file))) {

                while (in.hasNext()){
                    result.append(in.next());
                }


            } catch (IOException io) {
                System.err.println(io.toString());
            }
        }

        return result.toString();
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
