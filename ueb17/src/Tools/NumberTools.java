package Tools;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

/**
 * An implementation of NumberTools
 * in ueb16
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-07
 */
public class NumberTools {
    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * returns a randrom float
     *
     * @return float
     */
    public static float getRandomFloat() {
        return ThreadLocalRandom.current().nextFloat();
    }

    public static float getRandomFloat(int min, int max) {
        return min + ThreadLocalRandom.current().nextFloat() * (max - min);
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * returns a random integer
     *
     * @return int
     */
    public static int getRandomInt() {
        return ThreadLocalRandom.current().nextInt();
    }

    /**
     * returns a random integer between 0 and range
     *
     * @param range
     * @return int
     */
    public static int getRandomInt(int range) {
        return ThreadLocalRandom.current().nextInt(range);
    }

    public static int getRandomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    /**
     * compares two floats and returns true if they are equal
     *
     * @param f1      float one
     * @param f2      float two
     * @param epsilon margin of error
     * @return boolean
     */
    public static boolean floatEquals(float f1, float f2, float epsilon) {
        return Math.abs(f1 - f2) < epsilon;
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * rounds a given double to a given scale
     *
     * @param number double
     * @param scale  int
     * @return double - rounded number
     */
    public static double roundNDigits(double number, int scale) {

        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(scale);
        return bd.doubleValue();
    }


}
