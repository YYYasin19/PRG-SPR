package Lager;

import Tools.IOTools;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static Tools.MessageTools.getMsg;

/**
 * An implementation of Storage
 * in ueb17
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-14
 */
public class Storage<Item> {
    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final int MAX_STORAGE_SIZE = 100;
    private static final String DEFAULT_STORAGE_NAME = "Default Storage";
    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    private int maxItemCount;
    private String storageName;
    private ArrayList<Item> storage;

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Constructor for storage
     *
     * @param maxItemCount int Max. amount of items that can be stored
     * @param storageName  String name of the storage
     * @throws IllegalArgumentException when
     *                                  maxItemCount is negative
     */
    private Storage(int maxItemCount, String storageName) {
        this.setMaxItemCount(maxItemCount);
        this.setStorageName(storageName);
        this.storage = new ArrayList<>(maxItemCount);
    }

    /**
     * Constructor for storage
     *
     * @param maxItemCount int Max. amount of items that can be stored
     * @throws IllegalArgumentException when
     *                                  maxItemCount is negative
     */
    public Storage(int maxItemCount) {
        this.setMaxItemCount(maxItemCount);             // item count
        this.storageName = DEFAULT_STORAGE_NAME;        // storage name
        this.storage = new ArrayList<>(maxItemCount);   // storage initialization
    }

    /**
     * Constructor for storage
     *
     * @throws IllegalArgumentException when
     *                                  maxItemCount is negative
     */
    private Storage() {
        this.maxItemCount = MAX_STORAGE_SIZE;           // item count
        this.storageName = DEFAULT_STORAGE_NAME;        // storage name
        this.storage = new ArrayList<>(maxItemCount);   // storage initialization
    }


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * adds an Item to the storage without checking if it already exists
     *
     * @param item Item to be added
     */
    public void add(@NotNull final Item item) {
        this.storage.add(item);
    }

    /**
     * adds an item to the storage after checking if it is already stored
     *
     * @param item       Item to be added
     * @param duplicates BiPredicate that defines when two Items are equal. You can pass the equals()-Method of
     *                   the Item-Class here with: Item::equals if you want (Java 8 and Later)
     * @throws StorageException when the item already exists according to duplicates
     */
    public void add(@NotNull final Item item, BiPredicate<Item, Item> duplicates) throws StorageException {

        if (contains(item, duplicates)) {
            throw new StorageException(getMsg("ITEM_ALREADY_FOUND"));
        }

        this.storage.add(item);
    }

    /**
     * adds an Item to the storage without checking if it already exists
     *
     * @param items Item[] array of items that have to be added
     */
    public void add(@NotNull final Item[] items) {
        //Alternative: Arrays.stream(items).forEach(this::add)
        for (Item item : items) {
            this.add(item);
        }
    }

    /**
     * adds an item to the storage after checking if it is already stored
     *
     * @param items      Item[] array of items that have to be added
     * @param duplicates BiPredicate that defines when two Items are equal. You can pass the equals()-Method of
     *                   the Item-Class here with: Item::equals if you want (Java 8 and Later)
     * @throws StorageException when the item already exists according to duplicates
     */
    public void add(@NotNull final Item[] items, BiPredicate<Item, Item> duplicates) throws StorageException {

        for (Item item : items) {
            this.add(item, duplicates);
        }

    }

    /**
     * checks whether any item equals the given item in this storage
     * uses the equals()-Method of the the {@link Item} class
     *
     * @param item {@link Item}
     * @return true when this storage contains the given item
     */
    public boolean contains(@NotNull final Item item) {
        return this.storage.stream().anyMatch(item::equals);
    }

    /**
     * checks whether any item equals the given item in this storage
     * uses the equals()-Method of the the {@link Item} class
     *
     * @param item   {@link Item}
     * @param equals {@link BiPredicate} the Predicate for determining when two Instances of {@link Item} are equal
     * @return true when this storage contains the given item
     */
    public boolean contains(@NotNull final Item item, BiPredicate<Item, Item> equals) {
        return this.storage.stream().anyMatch(item1 -> equals.test(item, item1));
    }

    /**
     * returns the first Item that matches the given Predicate or else null
     *
     * @param p Predicate
     * @return first matching item or null
     */
    @Nullable
    public Item get(Predicate<Item> p) {
        return storage.stream().filter(p).findFirst().orElse(null);
    }

    /**
     * returns the Item at the Index index
     *
     * @param index int
     * @return Item
     */
    public Item get(int index) {
        return storage.get(index);
    }

    /**
     * returns any Items that match the given Predicate
     *
     * @param p Predicate
     * @return Item[]
     */
    public List<Item> filter(Predicate<Item> p) {
        //https://stackoverflow.com/questions/372250/generics-arrays-and-the-classcastexception
        //had to change the method because of the above. Before that, this method returned simply an Item[]

        return storage.stream().filter(p).collect(Collectors.toList());
    }

    /**
     * applies the given function to all items in-place
     *
     * @param f Function to be applied
     */
    public void applyToAll(Consumer<Item> f) {
        this.storage.forEach(f::accept);
    }

    /**
     * applies the given function to every item that fulfills the Predicate
     *
     * @param p Predicate
     * @param f Function
     */
    public void applyToItem(Predicate<Item> p, Consumer<Item> f) {
        this.storage.stream().filter(p).forEach(f::accept);
    }

    /**
     * returns a sorted List of this storage
     *
     * @param bP {@link BiPredicate}
     * @return {@link List}
     */
    public List<Item> getSorted(BiPredicate<Item, Item> bP) {
        ArrayList<Item> sortedList = ((ArrayList<Item>) storage.clone());

        sortedList.sort((o1, o2) -> bP.test(o1, o2) ? 1 : 0);

        return sortedList;
    }

    @Override
    public String toString() {
        //this could be the best toString()-Method in this country currently
        return (this.storage.stream().map(Item::toString).collect(Collectors.joining(", ")));
    }


    public String toString(@NotNull String delimiter) {
        return (this.storage.stream().map(Item::toString).collect(Collectors.joining(delimiter)));
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * sets the maximum item count
     *
     * @param maxItemCount
     * @throws IllegalArgumentException when
     *                                  item count < 1
     */
    public void setMaxItemCount(int maxItemCount) {
        if (maxItemCount < 1) throw new IllegalArgumentException(getMsg(""));
        if (maxItemCount > MAX_STORAGE_SIZE) {
            System.err.println(getMsg("MAX_LAGER_SIZE"));
            System.err.println("Storage size was set to default max value: " + MAX_STORAGE_SIZE);
            this.maxItemCount = MAX_STORAGE_SIZE;
        }
        this.maxItemCount = maxItemCount;
    }

    public String getStorageName() {
        return this.storageName;
    }

    /**
     * sets the non-empty storage name
     *
     * @param storageName
     * @throws IllegalArgumentException when
     *                                  storageName is empty
     */
    public void setStorageName(@NotNull String storageName) {
        if (IOTools.stringEmpty(storageName)) throw new IllegalArgumentException(getMsg("EMPTY_STRING"));
        this.storageName = storageName;
    }
}

/**
 * Exception class for Storage
 */
class StorageException extends Exception {

    public StorageException(String msg) {
        super(msg);
    }

}