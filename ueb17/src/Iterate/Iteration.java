package Iterate;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * An implementation of Iteration
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-13
 */
public class Iteration {

    private static final double x0 = 2.0;

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final int n = 5;
    private static final int a = 1;
    private static final BiFunction<Double, Integer, Double> taskTwoI = (x0, n) -> iterate(x0, n, f -> 2 * f);
    private static final BiFunction<Double, Integer, Double> taskTwoII = (x0, n) -> iterate(x0, n, f -> 0.5 * f);
    private static final TriFunction<Double, Integer, Integer, Double> taskTwoIII =
            (x0, n, a) -> (a < 1 && a > 0) ? iterate(x0, n, f -> a * x0 * (x0 - 1)) : null;

    /* ---------------------------------------- Main ---------------------------------------------------------------- */
    public static void main(String[] args) {
        double result1 = iterate(x0, n, f -> 2 * x0);           //2B : 1
        double result2 = iterate(x0, n, f -> (1 / 2) * x0);        //2B : 2
        double result3 = iterate(x0, n, f -> a * x0 * (x0 - 1));   //2B : 3

        //Alternative
        taskTwoI.apply(x0, n);                              //2B : 1
        taskTwoII.apply(x0, n);                             //2B : 2
        taskTwoIII.apply(x0, n, a);                          //2B : 3

        //Output
        System.out.println("Result 1:" + result1);
        System.out.println("Result 2:" + result2);
        System.out.println("Result 3:" + result3);
    }


    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * applies the given function f exactly n-times on the initial value x0
     *
     * @param x0 double
     * @param n  int
     * @param f  {@link Function}
     * @return double
     */
    private static double iterate(double x0, int n, Function<Double, Double> f) {
        return (n > 0) ? iterate(f.apply(x0), n - 1, f) : x0; //beautiful
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
