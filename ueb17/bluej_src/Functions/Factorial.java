package Functions;

/**
 * An implementation of Factorial
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-13
 */
public class Factorial {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public static int calcFactorial(int x){
        if(x < 0){
            throw new IllegalArgumentException();
        }

        return calculateEndRecursively(x, 1);

    }

    private static int calculateEndRecursively(int x, int current){
        if(x < 1){
            return current;
        }else{
            try {
                calculateEndRecursively(x - 1, x * current);
            }catch (RuntimeException e){
                throw new IllegalArgumentException("Argument too large, last calculated value: " + current);
            }
        }

        return 0;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */



}
