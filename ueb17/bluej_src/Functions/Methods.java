package Functions;

import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import static Tools.IOTools.*;
import static Tools.MessageTools.getMsg;

/**
 * An implementation of Methods
 * in ueb17
 *
 * @author ytatar
 * @version 1.0
 * @since 2018-Mai-11
 */
public class Methods {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */


    public static void main(String[] args) {
        try {
            /*
            Implementation of the functions goes here. First the 4 functions all used with lambda-expressions.
            After that every one of them is implemented with anonymous classes to compare the coding effort needed.
             */

            // ------ Lambda-Implementations: ------

            //i)
            applyAndPrint(I, N,     (x -> x*x));

            //ii)
            applyAndPrint(I, N,     (factorial));

            //iii)
            applyAndPrint(I, N,     (x -> (int) Math.pow(x,x+1)));

            //iv)
            applyAndPrint(I, N,     (fibonacci));


            // ------ Anonymous-Class-Implementations: ------

            //i)
            applyAndPrint(I, N, new MyFunction() {
                @Override
                public int apply(int i) {
                    return i*i;
                }
            });

            //ii)
            applyAndPrint(I, N, new MyFunction() {
                @Override
                public int apply(int i) {
                    return (i<1) ? 1 : i * apply(i-1);
                }
            });


            //iii)
            applyAndPrint(I, N, new MyFunction() {
                @Override
                public int apply(int i) {
                    return (int) Math.pow(i,i+1);
                }
            });


            //iv)
            applyAndPrint(I, N, new MyFunction() {
                @Override
                public int apply(int i) {
                    return (i<1) ? 0 : (apply(i-1) + apply(i-2));
                }
            });


            //1e)
            applyAndPrint(I, N, x->(even.test(x) ? x*x : 0));


            //1f) this is the prettiest version we could accomplish
            applyAndPrint(I, N,  x -> {
                if (odd().test(factorial.apply(x))) {
                    return factorial.apply(x);
                } else {
                    return 0;
                }
            });


        } catch (MethodsException e) {
            System.err.println(e);
        }
    }

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */
    private static final int I = 0;
    private static final int N = 5;

    private static final MyFunction square      = x -> (x*x);
    private static final MyFunction factorial   = x-> (x<1) ? 1 : x * Methods.factorial.apply(--x);
    private static final MyFunction fibonacci   =
            x-> (x<1) ? 0 : Methods.fibonacci.apply(--x) + Methods.fibonacci.apply(x-2);

    private static final Predicate<Integer> even = x-> (x % 2 == 0);

    

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * returns a Predicate that checks whether a given input is odd
     * @return
     */
    private static Predicate<Integer> odd(){

        return new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return (integer % 2) == 1;
            }
        };
    }

    /**
     *
     * @param i int > 0
     * @param j int > 0
     * @param f MyFunction - the function that is applied to every whole number between i and j
     */
    private static void applyAndPrint(int i, int j, MyFunction f) throws MethodsException {

        if (i < 0 || j < 0){ //check the input
            throw new MethodsException(getMsg("INPUT_MUST_BE_GREATER_THAN_ZERO"));
        }

        for (int index = i + 1; index < j; index++) {


            stdoutln(f.apply(index));


        }
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

/**
 * Exception class for Methods
 */
class MethodsException extends Exception {

    public MethodsException(String msg) {
        super(msg);
    }

}