package Functions;

import java.util.function.Predicate;

/**
 * An implementation of ConditionateMyFunction
 * in Prog2_Codebase
 *
 * @author takezo (jkrieger)
 * @version 1.0
 * @since 2018-May-13
 */
@FunctionalInterface
public interface ConditionateMyFunction extends MyFunction {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public default MyFunction conditionateInput(Predicate<Integer> predicate){
        return x -> (predicate.test(x) ? this.apply(x) : 0);
    }

    public default MyFunction conditionateOutput(Predicate<Integer> predicate){
        return x -> (predicate.test(this.apply(x)) ? this.apply(x) : 0);
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
