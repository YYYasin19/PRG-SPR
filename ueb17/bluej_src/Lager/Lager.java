package Lager;

import Tools.IOTools;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static Tools.CoreTools.check;
import static Tools.MessageTools.getMsg;


/**
 * Write a description of class Lager here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
@SuppressWarnings("JavaDoc")
public class Lager{
    /* ------------------------------ Konstanten ------------------------------ */
    private static int MAXIMAL_LAGER_GROESSE = 1000;

    /* -------------- Nachrichten -------------- */
    private static final String ARTIKEL_NICHT_GEFUNDEN =
            "Der Artikel konnte nicht gefunden werden.";

    private static final String ARTIKELNR_MUSS_GROESSER_SEIN =
            "Die Artikelnummer muss größer/gleich " + Artikel.ARTIKELNUMMER_MIN + " sein";
    private static final String LAGER_VOLL =
             "Das Lager ist leider voll.";
    private static final String KEIN_NAME_GEFUNDEN =
            "Der Name darf nicht leer sein.";


    /* ------------------------------ Attribute ------------------------------ */
    //Artikel[] artikelLager;
    ArrayList<Artikel> artikelLager;
    private int maxAnzahlArtikel;
    private String lagerName;

    /* ------------------------------ Konstruktoren ------------------------------ */
    /**
     * Legt ein neues Lager an und initialisiert ein Artikellager
     * mit der Maximalgroesse. (= Alle möglichen Artikelnummern)
     */
    public Lager(){
        maxAnzahlArtikel = MAXIMAL_LAGER_GROESSE;
        artikelLager = new ArrayList<Artikel>(MAXIMAL_LAGER_GROESSE);
    }

    /**
     * Legt ein neues Lager an und initialisiert ein Artikellager
     * mit der angegeben Groesse
     * @param lagerGroesse: Die maximale Anzahl an Artikeln die
     *                      im Lager gespeichert werden können
     * @throws IllegalArgumentException when
     *          lagerGroesse > {@link Lager#MAXIMAL_LAGER_GROESSE}
     */
    public Lager(int lagerGroesse){
        if (lagerGroesse <= 0) throw new IllegalArgumentException(getMsg("MAX_LAGER_SIZE"));

        maxAnzahlArtikel = lagerGroesse;
        artikelLager = new ArrayList<Artikel>(lagerGroesse);
    }

    /**
     * Legt ein neues Lager an und initialisiert ein Artikellager
     * mit dem angegeben Namen
     * @param lagerName: Der Name des Lagers. (Default: "HTW-Lager")
     * @throws IllegalArgumentException when
     *      lagerName is empty. (null-reference or 0 length)
     */
    public Lager(String lagerName) throws Exception {
        if (IOTools.stringEmpty(lagerName))
            throw new IllegalArgumentException(getMsg("EMPTY_STRING") + "@LagerConstructor.");

        new Lager(MAXIMAL_LAGER_GROESSE);
        this.setLagerName(lagerName);
    }

    /* ---------------------------------------- Methoden ---------------------------------------- */
    /* -------------------- Lager-Bearbeitung -------------------- */

    /**
     * Fuegt einen neuen Artikel dem Lager hinzu und überprüft vorher
     * ob der Artikel bereits vorhanden ist.
     * Es wird ein neues Artikel-Objekt angelegt!
     * @param artikelNummer int the number of the Artikel
     * @param artikelBezeichnung: Die Bezeichnung des Artikels.
     * @param artikelBestand: Bestand den der Artikel zu Beginn hat.
     * @param artikelPreis: Der Preis des Artikels
     */
    public void fuegeArtikelHinzu(
        int artikelNummer,
        String artikelBezeichnung,
        int artikelBestand,
        double artikelPreis
    ) throws Exception {
        fuegeArtikelHinzu(new Artikel(artikelNummer, artikelBezeichnung, artikelBestand, artikelPreis));
    }


    /**
     * adds a new Artikel to the Lager
     * @param neuerArtikel
     * @throws Exception when the Lager is already full
     */
    public void fuegeArtikelHinzu(Artikel neuerArtikel) throws Exception {
        check(!this.full(), new LagerException(getMsg("LAGER_FULL")));
        artikelLager.add(neuerArtikel);
    }

    /**
     * Entfernt einen Artikel mit der Artikelnummer aus dem Lager
     * @param artikelNummer int the number of the Artikel: Die Artikelnummer des zu entfernenden Artikels
     */
    public void entferneArtikel(int artikelNummer){
        artikelLager.removeIf((Artikel a) -> a.getArtikelNummer() == artikelNummer);
    }

    /**
     * changes the number of an Artikel object by taking a stream of the Artikel in this Lager and changing the
     * number of the first found object
     * if no object with the artikelNummer is found, the change will simply not be made
     * @param artikelNummer int the number of the Artikel
     * @param neueArtikelNummer
     * @return boolean true if the action was performed
     */
    public boolean nummerAendern(int artikelNummer, int neueArtikelNummer) throws Exception {
        //(Help h) -> System.err.println("Pleas help me too many lamdbas aaahh");
        Artikel temp = getArtikel(artikelNummer);

        if (temp == null){
            return false;
        } else {
            temp.setArtikelNummer(neueArtikelNummer);
            return true;
        }
    }

    /**
     * changes the bezeichnung of the first Artikel found with the given artikelNummer
     * @param artikelNummer int the number of the Artikel
     * @param neueBezeichnung
     * @throws Exception when neueBezeichnung is an empty String
     * @return boolean true if the action was performed
     */
    public boolean bezeichnungAendern(int artikelNummer, String neueBezeichnung) throws Exception {
        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        if (optionalArtikel.isPresent()){
            optionalArtikel.get().setArtikelBezeichnung(neueBezeichnung);
            return true;
        } else {
            return false;
        }
    }

    /**
     * increases the Bestand of the first Artikel found with artikelNummer
     * if no Artikel with artikelNummer is found, the change will not be made
     * @param artikelNummer int the number of the Artikel int
     * @param zugang int
     * @throws Exception when zugang < 0
     * @return boolean true if the action was performed
     */
    public boolean bestandErhoehen(int artikelNummer, int zugang) throws Exception {
        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        if (optionalArtikel.isPresent()){
            optionalArtikel.get().zugang(zugang);
            return true;
        } else {
            return false;
        }
    }

    /**
     * decreases the Bestand of the first Artikel found with artikelNummer
     * if no Artikel with artikelNummer is found, the change will not be made
     * @param artikelNummer int the number of the Artikel int
     * @param abgang int
     * @throws Exception when zugang < 0
     * @return boolean true if the action was performed
     */
    public boolean bestandReduzieren(int artikelNummer, int abgang) throws Exception {
        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        if (optionalArtikel.isPresent()){
            optionalArtikel.get().abgang(abgang);
            return true;
        } else {
            return false;
        }

    }

    /**
     * sets the price of an Artikel
     * @param artikelNummer int the number of the Artikel
     * @param neuerPreis
     * @return true if the action was performed
     * @throws Exception when neuerPreis < 0
     */
    public boolean preisAendern(int artikelNummer, double neuerPreis) throws Exception {

        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        if (optionalArtikel.isPresent()){
            optionalArtikel.get().setArtikelPreis(neuerPreis);
            return true;
        } else {
            return false;
        }
    }

    /**
     * increases the price of an Artikel by preisErhoehung
     * @param artikelNummer int the number of the Artikel
     * @param preisErhoehung
     * @throws Exception when preisErhoehung is < 0
     */
    public boolean preisErhoehen(int artikelNummer, double preisErhoehung) throws Exception {
        return performActionOnArtikel(
                artikel -> artikel.getArtikelNummer() == artikelNummer,
                artikel -> artikel.erhoehePreis(preisErhoehung));
    }

    /**
     * decreases the price of an Artikel by preisReduzierung
     * @param artikelNummer int the number of the Artikel
     * @param preisReduzierung
     * @throws Exception when preisReduzierung < 0
     */
    public boolean preisReduzieren(int artikelNummer, double preisReduzierung) throws Exception {
        return performActionOnArtikel(
                artikel -> artikel.getArtikelNummer() == artikelNummer,
                artikel -> artikel.reduzierePreis(preisReduzierung));
    }

    public boolean preisAendernProzent(int artikelNummer, double prozent) throws Exception {
        return performActionOnArtikel(
                artikel -> artikel.getArtikelNummer() == artikelNummer,
                artikel -> artikel.aenderePreisProzent(prozent));
    }

    /**
     * changes the price of all Artikel-Objects by a given percentage
     * @param prozent
     * @throws Exception when prozent < -100
     */
    public void preisAendernProzentAlle(double prozent) {
        if (prozent < - 100) throw new IllegalArgumentException(getMsg("PERCENT_TOO_LOW"));

        artikelLager.forEach(artikel -> {
            try {
                artikel.aenderePreisProzent(prozent);
            } catch (Exception e) {
                System.err.println(e.getLocalizedMessage());
            }
        });
    }

    /**
     * return the toString()-result of the Artikel with the given artikelNummer
     * @param artikelNummer int the number of the Artikel
     * @return String
     * @throws Exception when artikelNummer < {@link Artikel#ARTIKELNUMMER_MIN}
     * or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public String artikelStatusAusgeben(int artikelNummer) throws Exception {
        if (artikelNummer < Artikel.ARTIKELNUMMER_MIN || artikelNummer > Artikel.ARTIKELNUMMER_MAX)
            throw new IllegalArgumentException(
                    getMsg("ARTICLE_NUMBER_TOO_LOW_OR_HIGH") + "Your input: " + artikelNummer);

        return (getArtikel(artikelNummer).toString());
    }

    /* ---------------------------------------- Methods & Other ----------------------------------------------------- */

    /**
     * returns a stream of Artikel-Objects that fulfill the given Predicate
     * @param p
     * @return
     */
    private Stream<Artikel> getFilteredArtikelStream(Predicate<Artikel> p){
        return artikelLager.stream().filter(p);
    }

    /**
     * returns the first Artikel object that fulfills the given Predicate
     * only if there is one. Please see: {@link java.util.Optional}
     * @param p Predicate
     * @return Optional<Artikel>
     */
    private Optional<Artikel> getFirstArtikelThat(Predicate<Artikel> p) {
        return artikelLager.stream().filter(p).findFirst();
    }

    /**
     * returns the first Artikel object that matches the Predicate
     * if no Artikel was found, then null is returned
     * @param p {@link Predicate}
     * @return Artikel
     */
    private Artikel filter(Predicate<Artikel> p){
        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(p);

        //seems complicated but is not as complicated as it seems. i think.
        return optionalArtikel.orElse(null);
    }

    /**
     * performs a given action f on the first Artikel that fulfills p in {@link Lager#artikelLager}
     * @param p {@link Predicate}
     * @param f {@link java.util.function.Function}
     * @return boolean true if the action was performed
     */
    private boolean performActionOnArtikel(Predicate<Artikel> p, ArtikelFunction f) throws Exception {
        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(p);

        if (optionalArtikel.isPresent()){
            f.apply(optionalArtikel.get());
            return true;
        } else {
            return false;
        }
    }


    @SuppressWarnings("unchecked")
    public List<Artikel> getSorted(BiPredicate<Artikel,Artikel> bp){
        ArrayList<Artikel> sortedList = ((ArrayList<Artikel>) artikelLager.clone());

        sortedList.sort((o1, o2) -> bp.test(o1, o2) ? 1 : 0);

        return sortedList;

    }




    @Override
    public String toString() {
        return (this.artikelLager.stream().map(Artikel::toString).collect(Collectors.joining("\n")));
    }

    /**
     * checks whether the Lager is full or not
     * @return true when the Lager is full
     */
    private boolean full() {
        return artikelLager.size() == maxAnzahlArtikel;
    }

    /* ------------------------------ Setter & Getter ------------------------------ */

    /**
     * sets a new non-empty name for the Lager
     * @param neuerName String new name of the Lager
     * @throws IllegalArgumentException when
     *      artikelNummmer < {@link Artikel#ARTIKELNUMMER_MIN} or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public void setLagerName(String neuerName) {
        if (IOTools.stringEmpty(neuerName))
            throw new IllegalArgumentException(getMsg("EMPTY_STRING"));
        this.lagerName = neuerName;
    }

    /**
     * set the Artikel Bestand on the first Artikel that was found in the list
     * @param artikelNummer int the number of the Artikel
     * @param artikelBestand
     * @return boolean
     * @throws IllegalArgumentException when
     *         artikelNummmer < {@link Artikel#ARTIKELNUMMER_MIN} or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public boolean setArtikelBestand(int artikelNummer, int artikelBestand) throws Exception {
        if (artikelNummer < Artikel.ARTIKELNUMMER_MIN || artikelNummer > Artikel.ARTIKELNUMMER_MAX)
            throw new IllegalArgumentException(getMsg("ARTICLE_NUMBER_TOO_LOW_OR_HIGH"));

        return performActionOnArtikel(
                artikel -> artikel.getArtikelNummer() == artikelNummer,
                artikel -> artikel.setArtikelBestand(artikelBestand)
        );
    }

    /**
     * Gibt einem das Artikel-Objekt, also die Referenz auf das Objekt im Heap, zurück.
     * Damit kann man das sinnvoll bearbeiten. Vermutlich eine der sinnvollsten Methoden
     * in dieser Klasse
     * @param artikelNummer int the number of the Artikel: Die Nummer des Artikels im Lager
     * @throws IllegalArgumentException when
     *      artikelNummmer < {@link Artikel#ARTIKELNUMMER_MIN} or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public Artikel getArtikel(int artikelNummer) {
        if (artikelNummer < Artikel.ARTIKELNUMMER_MIN || artikelNummer > Artikel.ARTIKELNUMMER_MAX)
            throw new IllegalArgumentException(getMsg("ARTICLE_NUMBER_TOO_LOW_OR_HIGH"));

        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        return optionalArtikel.orElse(null);
    }

    /**
     * returns the Artikel object at index artikelIndex
     * @param artikelIndex > -1
     * @return Artikel
     * @throws IllegalArgumentException when
     *      artikelIndex < -1
     */
    public Artikel getArtikelAtIndex(int artikelIndex) {
        if (artikelIndex < -1) throw new IllegalArgumentException(getMsg("INDEX_LESS_THAN_ZERO"));
        return artikelLager.get(artikelIndex);
    }

    /**
     * returns the Bestand of the first Artikel with artikelNummer
     * @param artikelNummer int the number of the Artikel
     * @return int Bestand if Artikel was found - otherwise -1
     * @throws IllegalArgumentException when
     *      artikelNummmer < {@link Artikel#ARTIKELNUMMER_MIN} or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public int getArtikelBestand(int artikelNummer){
        if (artikelNummer < Artikel.ARTIKELNUMMER_MIN || artikelNummer > Artikel.ARTIKELNUMMER_MAX)
            throw new IllegalArgumentException(getMsg("ARTICLE_NUMBER_TOO_LOW_OR_HIGH"));

        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        if (optionalArtikel.isPresent()){
            return optionalArtikel.get().getArtikelBestand();
        } else {
            return -1;
        }
    }

    /**
     * returns the Bezeichnung of the first Artikel with artikelNummer
     * @param artikelNummer int the number of the Artikel
     * @return String bezeichnung if Artikel was found - otherwise ""
     * @throws IllegalArgumentException when
     *      artikelNummmer < {@link Artikel#ARTIKELNUMMER_MIN} or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public String getArtikelBezeichnung(int artikelNummer){
        if (artikelNummer < Artikel.ARTIKELNUMMER_MIN || artikelNummer > Artikel.ARTIKELNUMMER_MAX)
            throw new IllegalArgumentException(getMsg("ARTICLE_NUMBER_TOO_LOW_OR_HIGH"));

        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        if (optionalArtikel.isPresent()){
            return optionalArtikel.get().getArtikelBezeichnung();
        } else {
            return "";
        }
    }

    /**
     * returns the price of the first Artikel with artikelNummer
     * @param artikelNummer int the number of the Artikel
     * @return double price if Artikel was found - otherwise -1.0
     * @throws IllegalArgumentException when
     *      artikelNummmer < {@link Artikel#ARTIKELNUMMER_MIN} or artikelNummer > {@link Artikel#ARTIKELNUMMER_MAX}
     */
    public double getArtikelPreis(int artikelNummer){
        if (artikelNummer < Artikel.ARTIKELNUMMER_MIN || artikelNummer > Artikel.ARTIKELNUMMER_MAX)
            throw new IllegalArgumentException(getMsg("ARTICLE_NUMBER_TOO_LOW_OR_HIGH"));

        Optional<Artikel> optionalArtikel =
                getFirstArtikelThat(artikel -> artikel.getArtikelNummer() == artikelNummer);

        if (optionalArtikel.isPresent()){
            return optionalArtikel.get().getArtikelPreis();
        } else {
            return -1.0;
        }
    }

}

class LagerException extends Exception {

    public LagerException(String msg){
        super(msg);
    }

}
