package Lager;

/**
 * Exception class for Storage
 */
public class StorageException extends Exception {

    public StorageException(String msg) {
        super(msg);
    }

}