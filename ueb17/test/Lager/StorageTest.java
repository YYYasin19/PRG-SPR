package Lager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static org.junit.Assert.*;

public class StorageTest {

    private Storage<Artikel> artikelStorage;
    private Artikel[] testArtikel;

    /**
     * checks if a given list is sorted for a given comparator
     *
     * @param list
     * @param comparator
     * @return
     */
    public static boolean isSorted(List<?> list, Comparator comparator) {
        Object prev = null;
        for (Object elem : list) {
            if (prev != null && comparator.compare(prev, elem) > 0) {
                return false;
            }
            prev = elem;
        }
        return true;
    }

    /* ---------------------------------------- Test-Methods -------------------------------------------------------- */

    /* ---------------------------------------- Before -------------------------------------------------------------- */
    @Before
    public void setUp() {
        artikelStorage = new Storage<Artikel>(10);
        testArtikel = new Artikel[]{
                new Artikel(1000, "Test-Artikel-1000"),
                new Artikel(1001, "Test-Artikel 1001"),
                new Artikel(1002, "Test-Artikel 1002"),
        };

        //batch adds all the Artikel Objects to the Storage
        try {

            artikelStorage.add(testArtikel,
                    (artikel, artikel2) -> artikel.getArtikelNummer() == artikel2.getArtikelNummer());

        } catch (StorageException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    @Test
    public void testAdd() {
        artikelStorage.add(testArtikel[0]);
        assertTrue(artikelStorage.contains(testArtikel[0],
                (artikel, artikel2) -> artikel.getArtikelNummer() == artikel2.getArtikelNummer()));

    }

    @Test
    public void testAddNull() {
        Storage<Object> testStorage = new Storage<>(1);
        testStorage.add(null); //TODO: Why this? Maybe (excpected = NullPointerException.class) ?
        assertNotNull(testStorage.get(0));
    }

    @Test(expected = StorageException.class)
    public void testAddException() throws StorageException {

        artikelStorage.add(testArtikel[0], Artikel::equals); //adds the Item again and checks with Articles equals method
    }

    /**
     * tests if the given Artikel is in the storage. The Artikel are compared with Artikels equals()-Method
     */
    @Test
    public void testContains() {
        assertTrue(artikelStorage.contains(testArtikel[0], Artikel::equals));
    }

    /**
     * tests if the retrieved Artikel is right
     */
    @Test
    public void testGet() {
        assertEquals(testArtikel[0], artikelStorage.get(artikel -> artikel.getArtikelNummer() == 1000));
    }

    /**
     * filters the storage for a specification that can only be had once and then checks whether the number of results
     * are also only one.
     */
    @Test
    public void testFilter() {
        ArrayList<Artikel> filterResults =
                (ArrayList<Artikel>) artikelStorage.filter(artikel -> artikel.getArtikelNummer() == 1000);
        if (filterResults.size() == 1) { //there is only one Artikel that matchese
            assertEquals(testArtikel[0], filterResults.get(0)); //the result should be exactly this one
        } else {
            fail();
        }

    }

    /**
     * appends a specific-String to every Artikels Bezeichnung and then checks if every Artikel contains that String
     */
    @Test
    public void testApplyToAll() {
        String appendString = "testApplyToAll";

        artikelStorage.applyToAll(
                artikel -> artikel.setArtikelBezeichnung(artikel.getArtikelBezeichnung() + appendString)
        );

        //returns just every Artikel
        ArrayList<Artikel> allArtikel = (ArrayList<Artikel>) artikelStorage.filter(artikel -> (true));
        for (Artikel a : allArtikel) {
            if (!(a.getArtikelBezeichnung().contains(appendString))) {
                fail(); //fails this test
            }
        }

        //test finished!
    }

    /**
     * changes the Bezeichnung of an Artikel Object and then retrives the object and sees if the Bezeichnung was
     * changed
     */
    @Test
    public void testApplyToItem() {
        String neueBezeichnung = "Neue Bezeichnung 1000";
        Predicate<Artikel> p = artikel -> artikel.getArtikelNummer() == 1000;
        //apply the function
        artikelStorage.applyToItem(
                p,
                artikel -> artikel.setArtikelBezeichnung("Neue Bezeichnung 1000"));

        assertEquals(neueBezeichnung, artikelStorage.get(p).getArtikelBezeichnung());
    }

    /**
     * tests every Artikel object in the returned list with the same tester that was given as a parameter to sort
     * in the first place.
     */
    @Test
    public void testGetSorted() {
        //the tester for determining when an Artikel is greater than another
        BiPredicate<Artikel, Artikel> tester =
                (artikel, artikel2) -> artikel.getArtikelNummer() <= artikel2.getArtikelNummer();

        //sorted list according to the aforementioned tester
        ArrayList<Artikel> list = (ArrayList<Artikel>) artikelStorage.getSorted(
                tester);

        //objects for comparison
        Artikel prev = list.get(0);
        Artikel current;

        //loop for traversing the list and checking for sorted-ness:
        for (int index = 1; index < list.size(); index++) {
            current = list.get(index);

            if (tester.test(prev, current)) {
                //up to now everything seems sorted
                prev = current;

            } else {
                fail(); //it is not sorted according to the given tester
            }
        }
    }

    @Test
    public void testToString() {
        System.out.println(artikelStorage.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetMaxItemCount() {
        artikelStorage.setMaxItemCount(-10);
    }

    @Test
    public void testSetStorageName() {
        String newName = "New Name of the Storage";
        artikelStorage.setStorageName(newName);
        assertEquals(newName, artikelStorage.getStorageName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetStorageNameException() {
        artikelStorage.setStorageName("");
    }


    /* ---------------------------------------- Other Methods ------------------------------------------------------- */

    /* ---------------------------------------- After --------------------------------------------------------------- */
    @After
    public void tearDown() {

    }

}