package Lager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Consumer;

public class LagerTest {

    private Storage<Artikel> lager;

    /* ---------------------------------------- Before -------------------------------------------------------------- */
    @Before
    public void setUp() throws Exception {
        this.lager = new Storage<Artikel>(10);

        Artikel artikel1 = new Artikel(1111, "TestArtikel1", 10, 50);
        Artikel artikel2 = new Artikel(1111, "TestArtikel2", 20, 100);
        Artikel artikel3 = new Artikel(1111, "TestArtikel3", 30, 150);

        lager.add(artikel1);
        lager.add(artikel2);
        lager.add(artikel3);

    }

    /* ---------------------------------------- Test-Methods -------------------------------------------------------- */

    //3i)
    @Test
    public void testSortLexicographically(){
        this.lager.getSorted(((artikel, artikel2) -> artikel.getArtikelBezeichnung().compareTo(artikel2.getArtikelBezeichnung()) == 1));
    }

    @Test
    public void testSortByBestand(){
        this.lager.getSorted((artikel, artikel2) -> artikel.getArtikelBestand() > artikel2.getArtikelBestand());
    }

    @Test
    public void testSortedByPrice(){
        this.lager.getSorted((artikel, artikel2) -> artikel.getArtikelPreis() > artikel2.getArtikelPreis());
    }

    //3ii)
    @Test
    public void testReducePriceByTenPercent(){
        this.lager.applyToAll(artikel -> artikel.aenderePreisProzent(90));
    }

    //3iii)
    @Test
    public void testAddSuffix(){
        this.lager.applyToAll(artikel -> artikel.setArtikelBezeichnung(artikel.getArtikelBezeichnung() + "(Sonderangebot"));
    }

    //3iv)
    @Test
    public void testConcatenate(){
        Consumer<Artikel> artikelConsumer = artikel -> artikel.aenderePreisProzent(90);
        artikelConsumer.andThen(artikel -> artikel.setArtikelBezeichnung(artikel.getArtikelBezeichnung() + "Sonderangebot"));
        this.lager.applyToAll(artikelConsumer);
    }

    /* ---------------------------------------- Other Methods ------------------------------------------------------- */

}